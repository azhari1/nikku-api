package id.code.nikku.test;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.api.handler.UsersLoginHandler;
import id.code.nikku.model.UsersModel;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

public class DummyData extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet(authorizeAccess = false)
    public void handleGET(ServerExchange<AccessTokenPayload> exchange) throws Exception {
        for(int i=18;i<100;i++) {
            UsersModel um = new UsersModel();
            um.setId(i);
            UsersLoginHandler a = new UsersLoginHandler();
            a.autoAddPrivacySetting(um);
        }
    }
}
