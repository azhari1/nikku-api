package id.code.nikku.test;

import id.code.nikku.helper.SendMailSSL;
import id.code.nikku.model.UsersModel;

import java.util.UUID;

public class SendMail {
    public static void main(String[] arg){
        System.out.println("SENDING MAIL");
        UsersModel um = new UsersModel();
        um.setFullName("Azhari");
        um.setEmail("azhari@code.id");
        um.setUid(UUID.randomUUID().toString());
        SendMailSSL.sendMailVerification(um);
        System.out.println("END SEND OF MAIL");
    }
}

