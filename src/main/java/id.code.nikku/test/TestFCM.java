package id.code.nikku.test;

import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.helper.FcmHelper;
import org.json.JSONException;
import org.json.JSONObject;

public class TestFCM {
    public static void sendPushNotification(String deviceID){
        JSONObject json = new JSONObject();
        try {
            json.put("to",deviceID.trim());
            JSONObject jsonData = new JSONObject();
            jsonData.put(ConstantHelper.NOTIFICATION_TYPE,ConstantHelper.NOTIF_CHAT);
            jsonData.put(ConstantHelper.NOTIFICATION_MESSAGE,"Jl. Anggrek Utara");
            json.put("data",jsonData);
            try {
                FcmHelper.pushFCMNotification(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public static void main(String args[]){
        TestFCM.sendPushNotification("cV_AtJpnQa4:APA91bFwm8cTan28yDFiWP-ub-7c7McWMmzddnBAjUfiIUcbPgdV8vKYtj01HOZ37L_Q4nwWI6zsNehP3jVzrfwNaBPrdG8Rfeesl0H18pM3f5u2tq5QuJMGY8ew38FVbo_kPQdszyNM");
    }
}
