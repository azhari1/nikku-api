package id.code.nikku.helper;

public class ConstantHelper {
    public static final int REVIEW_AS_PUBLIC = 0;
    public static final int REVIEW_AS_COSTUMER = 1;
    public static final int REVIEW_AS_PIC = 7;

    public static final int INBOX_TYPE_REVIEW_AS_PUBLIC = 0;
    public static final int INBOX_TYPE_REVIEW_AS_CUSTOMER = 1;
    public static final int INBOX_TYPE_GOT_REVIEW_IN_COMPANY = 2;
    public static final int INBOX_TYPE_CHANGED_STATUS_IN_COMPANY = 3;
    public static final int INBOX_TYPE_REQUEST_REVIEW_WAITING = 4;
    public static final int INBOX_TYPE_REQUEST_REVIEW_APPROVAL = 5;
    public static final int INBOX_TYPE_REQUEST_REVIEW_REJECTED = 6;
    public static final int INBOX_TYPE_REVIEW_AS_PIC = 7;

    public static final int STATUS_ON_PROGRESS = 0;
    public static final int STATUS_APPROVED = 1;
    public static final int STATUS_REJECTED = 2;
    public static final int STATUS_RESIGNED = 3;
    public static final int STATUS_MARK_AS_PIC = 4;
    public static final int STATUS_HIDE_FROM_PUBLIC = 5;

    public static final int STATUS_VERYFIED=1;
    public static final int STATUS_ACTIVE=0;
    public static final int STATUS_DEACTIVE=1;
    public static final long IS_READED = 1;

    public static final String NOTIFICATION_TYPE = "NotificationType";
    public static final String NOTIFICATION_MESSAGE = "NotificationMessage";

    public static final int NOTIF_CHAT = 0;
    public static final int NOTIF_REVIEW_AS_PUBLIC = 1;
    public static final int NOTIF_REVIEW_AS_CUSTOMER = 2;
    public static final int NOTIF_COMPANY_PERSON_REVIEWS = 3;
    public static final int NOTIF_REQUEST_JOIN = 4;
    public static final int NOTIF_CHANGE_STATUS = 5;
    public static final int NOTIF_REQUEST_REVIEW = 6;

    public static final int RFR_WAITING = 0;
    public static final int RFR_APPROVAL = 1;
    public static final int RFR_REJECTED = 2;
    public static final int RFR_CLOSING = 3;
}
