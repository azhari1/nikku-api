package id.code.nikku.helper;

import org.json.JSONObject;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class FcmHelper {

    private final static String AUTH_KEY_FCM = "AAAAwHQ5lJU:APA91bG3ERu2GunQDx_3sgctz2WZzbmnrJ0LG4OWoZOhRbzL5OrM6ne90E-N4EyIS1GHN3JvAkYXL4l-FCmC0NqcsIK1l3R4gg2zNmN5rACj1LK7irUf3ZTVpeix9DTuSMY31X6v_W90";
    private final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    public static void pushFCMNotification(JSONObject json) throws Exception{
        URL url = new URL(API_URL_FCM);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization","key="+AUTH_KEY_FCM);
        conn.setRequestProperty("Content-Type","application/json");
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(json.toString());
        wr.flush();
        conn.getInputStream();
    }
}
