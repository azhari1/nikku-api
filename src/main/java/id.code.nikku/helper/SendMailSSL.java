package id.code.nikku.helper;

import id.code.nikku.model.UsersModel;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

public class SendMailSSL {

    public static String APP_NAME = "NIKKU";
    public static void sendMailVerification(UsersModel usersModel){
        Session session = Configure.getMailSession();
        try {
            System.out.println("Try to send mail");
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Configure.USER_MAIL,APP_NAME));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(usersModel.getEmail()));
            message.setSubject("Email Account Verification");
            message.setText("Dear "+usersModel.getFullName()+"," +
                    "\n\nPlease click link bellow to verify your account" +
                    "\n\n" +
                    Configure.HOST+"activation/"+usersModel.getUid());
            Transport.send(message);
            System.out.println("Try to send mail Successfully");
        } catch (MessagingException e) {
            System.out.println("Failed Try to send mail "+e.getMessage());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            System.out.println("Failed Try to send mail "+e.getMessage());
        }
    }

    public static void sendMailForgotPassword(UsersModel usersModel){
        Session session = Configure.getMailSession();
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Configure.USER_MAIL,APP_NAME));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(usersModel.getEmail()));
            message.setSubject("Forgot Password");
            message.setText("Dear "+usersModel.getFullName()+"," +
                    "\n\nPlease click link bellow to change your new password" +
                    "\n\n" +
                    Configure.HOST+"update-password/"+usersModel.getUid());
            Transport.send(message);
            System.out.println("Done");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}