package id.code.nikku.helper;

import java.util.Calendar;
import java.util.TimeZone;

public class DateHelper {
    public static long getCurrentTime(){
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Jakarta");
        Calendar calendar = Calendar.getInstance(timeZone);
        return calendar.getTime().getTime();
    }
}
