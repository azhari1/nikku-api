package id.code.nikku.helper;

import sun.misc.BASE64Decoder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class ImageHelper {

    private static final String users_profile = "data/upload/user_profiles/";
    private static final String company_profile = "data/upload/company/";
    private static final String review_files = "data/upload/reviews/";
    private static final String ktp_files = "data/upload/user_ktp/";

    public static String saveUserFile(String base64){
        String filename = Calendar.getInstance().getTime().getTime()+".png";
        try {
            byte[] btDataFile = new BASE64Decoder().decodeBuffer(base64);
            File file = new File(users_profile+filename);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(btDataFile);
            fos.flush();
            return filename;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String saveUserKtp(String base64){
        String filename = Calendar.getInstance().getTime().getTime()+".png";
        try {
            byte[] btDataFile = new BASE64Decoder().decodeBuffer(base64);
            File file = new File(ktp_files+filename);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(btDataFile);
            fos.flush();
            return filename;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String saveCompanyFile(String base64){
        String filename = Calendar.getInstance().getTime().getTime()+".png";
        try {
            byte[] btDataFile = new BASE64Decoder().decodeBuffer(base64);
            File file = new File(company_profile+filename);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(btDataFile);
            fos.flush();
            return filename;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String saveReviewFile(String base64){
        String filename = Calendar.getInstance().getTime().getTime()+".png";
        try {
            byte[] btDataFile = new BASE64Decoder().decodeBuffer(base64);
            File file = new File(review_files+filename);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(btDataFile);
            fos.flush();
            return filename;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String updateUserFile(String oldFile,String base64){
        if(oldFile!=null){
            deleteUserFile(oldFile);
        }
        String filename = Calendar.getInstance().getTime().getTime()+".png";
        try {
            byte[] btDataFile = new BASE64Decoder().decodeBuffer(base64);
            File file = new File(users_profile+filename);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(btDataFile);
            fos.flush();
            return filename;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String updateCompanyFile(String oldFile,String base64){
        if(oldFile!=null){
            deleteCompanyFile(oldFile);
        }
        String filename = Calendar.getInstance().getTime().getTime()+".png";
        try {
            byte[] btDataFile = new BASE64Decoder().decodeBuffer(base64);
            File file = new File(company_profile+filename);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(btDataFile);
            fos.flush();
            return filename;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean deleteUserFile(String filename){
        try{
            File file = new File(users_profile+filename);
            return file.delete();
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static boolean deleteCompanyFile(String filename){
        try{
            File file = new File(company_profile+filename);
            return file.delete();
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public static boolean deleteKtpFile(String filename){
        try{
            File file = new File(ktp_files+filename);
            return file.delete();
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
