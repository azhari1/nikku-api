package id.code.nikku.helper.sendgrid;

import com.sendgrid.Email;
import id.code.nikku.model.UsersModel;

public class SendMail {
    public static void sendMailVerification(UsersModel usersModel){
        String message = "Dear "+usersModel.getFullName()+"," +
                "\n\nPlease click link bellow to verify your account" +
                "\n\n" +
                Configure.HOST+"activation/"+usersModel.getUid();
        String subject = "Email account verification";
        Email to = new Email(usersModel.getEmail());
        Configure.sendMail(subject,to,message);
    }

    public static void sendMailForgotPassword(UsersModel usersModel){
        String message = "Dear "+usersModel.getFullName()+"," +
                "\n\nPlease click link bellow to change your new password" +
                "\n\n" +
                Configure.HOST+"update-password/"+usersModel.getUid();
        String subject = "Forgot Password";
        Email to = new Email(usersModel.getEmail());
        Configure.sendMail(subject,to,message);
    }
}
