package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.*;
import id.code.nikku.helper.ImageHelper;
import id.code.nikku.model.*;
import id.code.nikku.validation.UsersNikUpdateValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class UsersUpdateNikHandler extends RouteApiHandler<AccessTokenPayload> {

    private final UsersFacade usersFacade = new UsersFacade();
    public static final ReviewsFacade reviewsFacade = new ReviewsFacade();
    public static final InboxesFacade inboxesFacade = new InboxesFacade();
    public static final MessagesFacade messagesFacade = new MessagesFacade();
    private final PrivacysettingsFacade privacysettingsFacade = new PrivacysettingsFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody UsersNikUpdateValidation users) throws QueryBuilderException, SQLException, IOException {
        int isMerge = 1;
        if(users.getIsMerge()== isMerge){
            if(doMerge(users.getId(), exchange.getAccessTokenPayload().getId())){
                UsersModel umOld = this.usersFacade.findById(users.getId());
                UsersModel umNew = this.usersFacade.findById(exchange.getAccessTokenPayload().getId());
                umNew.setNik(umOld.getNik());
                umNew.setKtpPhoto(umOld.getKtpPhoto());
                if(this.privacysettingsFacade.delete(umOld.getId())){
                    this.usersFacade.delete(umOld.getId());
                    this.usersFacade.update(umNew);
                    ApiResponse apiResponse = new ApiResponse(200, "Your Nik has been updated and merge from old profile");
                    apiResponse.setData(umNew);
                    super.sendResponse(exchange, apiResponse);
                }else{
                    super.sendResponse(exchange, new ApiResponse(422, "Your data failed to merge!!!"));
                }
            }else{
                super.sendResponse(exchange, new ApiResponse(422, "Your data failed to merge!!!"));
            }
        }else{
            boolean available = this.usersFacade.isNIKAvailable(users.getNik(), exchange.getAccessTokenPayload());
            if (available) {
                UsersModel um = this.usersFacade.findById(users.getId());
                if (um != null) {
                    String imageName = ImageHelper.saveUserKtp(users.getKtpPhoto());
                    um.setNik(users.getNik());
                    um.setKtpPhoto(imageName);
                    try {
                        this.usersFacade.update(um);
                        super.sendResponse(exchange, new ApiResponse(200, "Your Nik has been updated", um));
                    } catch (Exception e) {
                        ImageHelper.deleteKtpFile(imageName);
                        e.printStackTrace();
                        super.sendResponse(exchange, new ApiResponse(422, "Your Nik failed to update!!!"));
                    }

                } else {
                    super.sendResponse(exchange, new ApiResponse(422, "Your Nik failed to update!!!"));
                }
            } else {
                //cek for marge
                UsersModel usersMerge = usersFacade.isNIkAvailableForMerge(users.getNik());
                if (usersMerge != null) {
                    ApiResponse apiResponse = new ApiResponse(423, "User asking for merge");
                    apiResponse.setData(usersMerge);
                    super.sendResponse(exchange, apiResponse);
                } else {
                    super.sendResponse(exchange, new ApiResponse(422, "Your nik is already exists!!!"));
                }
            }
        }
    }

    public static boolean doMerge(long oldUser,long newUser) throws QueryBuilderException, SQLException {

        List<ReviewsModel> revModelOlds = reviewsFacade.findByUserId(oldUser);
        List<InboxesModel> inbModelOlds = inboxesFacade.findByUserId(oldUser);
        List<MessagesModel> msgModelOlds = messagesFacade.findBySenderReceiverId(oldUser);

        try{
            for (ReviewsModel reviewsModel : revModelOlds) {
                reviewsModel.setUserId(newUser);
                reviewsFacade.update(reviewsModel);
            }

            for(InboxesModel inboxesModel : inbModelOlds){
                inboxesModel.setUserId(newUser);
                inboxesFacade.update(inboxesModel);
            }

            for(MessagesModel messagesModel : msgModelOlds){
                if(messagesModel.getSenderId()==oldUser){
                    messagesModel.setSenderId(newUser);
                }else{
                    messagesModel.setReceiverId(newUser);
                }
                messagesFacade.update(messagesModel);
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
