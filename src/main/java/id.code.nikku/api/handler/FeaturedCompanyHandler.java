package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.model.modify.FeaturedCompany;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class FeaturedCompanyHandler extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet(authorizeAccess = false)
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<FeaturedCompany> filter) throws QueryBuilderException, SQLException, IOException {
        List<FeaturedCompany> featuredCompanies = new CompaniesoutletsFacade().getFeaturedCompany(filter);
        ApiResponse response = new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK, featuredCompanies);
        response.setFilter(filter);
        response.setData(featuredCompanies);
        super.sendResponse(exchange, response);
    }
}
