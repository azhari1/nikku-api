package id.code.nikku.api.handler;

import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.*;
import id.code.nikku.filter.UserworkatcompaniesFilter;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.helper.DateHelper;
import id.code.nikku.helper.FcmHelper;
import id.code.nikku.model.*;
import id.code.nikku.validation.UserworkatcompaniesCreateValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class UserworkatcompaniesHandler extends RouteApiHandler<AccessTokenPayload> {

    private final UserworkatcompaniesFacade userworkatcompaniesFacade = new UserworkatcompaniesFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<UserworkatcompaniesFilter> filter) throws Exception {
        final List<UserworkatcompaniesModel> items = this.userworkatcompaniesFacade.getAllWithJoin(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final UserworkatcompaniesModel data = this.userworkatcompaniesFacade.findById(id);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody UserworkatcompaniesCreateValidation createData) throws Exception {
        UserworkatcompaniesModel newData = new UserworkatcompaniesModel();
        newData.setCompanyId(createData.getCompanyId());
        newData.setUserId(createData.getUserId());
        newData.setJobPosition(createData.getJobPosition());
        newData.setStartDate(createData.getStartDate());
        newData.setEndDate(createData.getEndDate());
        newData.setStatus(createData.getStatus());
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            CompaniesoutletsModel com = new CompaniesoutletsFacade().findById(newData.getCompanyId());
            UsersModel um = new UsersFacade().findById(com.getOwner());
            String comName = com.getOutletName();
            if(StringUtility.isNullOrWhiteSpace(comName)){
                comName = com.getCompanyName();
            }
            String deviceId = um.getFcmToken();
            String message = "You have got new request person to join in your company at ";
            String pushMessage = message + comName;
            if(!StringUtility.isNullOrWhiteSpace(deviceId)){
                notifRequestJoin(deviceId, pushMessage, com);
            }
            String inboxMessage = message+"<b>"+comName+"</b>";
            InboxesFacade inboxesFacade = new InboxesFacade();
            InboxesModel inboxesModel = new InboxesModel();
            inboxesModel.setUserId(um.getId());
            inboxesModel.setInboxType(ConstantHelper.INBOX_TYPE_CHANGED_STATUS_IN_COMPANY);
            inboxesModel.setMessage(inboxMessage);
            inboxesModel.setReferenceId(com.getId());
            inboxesModel.setDate(DateHelper.getCurrentTime());
            inboxesFacade.insert(inboxesModel);
            this.userworkatcompaniesFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final boolean deleted = this.userworkatcompaniesFacade.delete(id);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange,@RequestBody UserworkatcompaniesModel newData, Long id) throws Exception {
        newData.setId(id);
        UsersModel um = new UsersFacade().findById(newData.getUserId());
        CompaniesoutletsModel com = new CompaniesoutletsFacade().findById(newData.getCompanyId());
        String comName = com.getOutletName();
        if(StringUtility.isNullOrWhiteSpace(comName)){
            comName = com.getCompanyName();
        }
        String deviceId = um.getFcmToken();
        String message = "";
        if(newData.getStatus()== ConstantHelper.STATUS_APPROVED){
            message = "You have approved from ";
        }else if(newData.getStatus()== ConstantHelper.STATUS_REJECTED){
            message = "You have rejected from ";
        }else if(newData.getStatus()== ConstantHelper.STATUS_RESIGNED){
            message = "You have resigned from ";
            newData.setEndDate(Calendar.getInstance(TimeZone.getTimeZone("Asia/Jakarta")).getTime().getTime());
            CompanyoutletpicsFacade clf = new CompanyoutletpicsFacade();
            CompanyoutletpicsModel cpics = clf.findByCU(com.getId(),um.getId());
            if(cpics!=null){
                cpics.setStatus(ConstantHelper.STATUS_DEACTIVE);
                clf.update(cpics);
            }
        }else if(newData.getStatus()== ConstantHelper.STATUS_MARK_AS_PIC){
            message = "You have mark as pic from ";
            CompanyoutletpicsFacade clf = new CompanyoutletpicsFacade();
            CompanyoutletpicsModel cpics = new CompanyoutletpicsModel();
            cpics.setCompanyId(com.getId());
            cpics.setUserId(um.getId());
            cpics.setCreatedDate(DateHelper.getCurrentTime());
            cpics.setStatus(ConstantHelper.STATUS_ACTIVE);
            if(clf.findByCU(com.getId(),um.getId())==null){
                clf.insert(cpics);
            }

        }else if(newData.getStatus()== ConstantHelper.STATUS_HIDE_FROM_PUBLIC){
            message = "You have hide from public at ";
        }
        String pushMessage = message + comName;
        if (this.userworkatcompaniesFacade.update(newData)) {
            if(!StringUtility.isNullOrWhiteSpace(deviceId)){
                notifChangeStatus(deviceId, pushMessage,com);
            }
            String inboxMessage = message+"<b>"+comName+"</b>";
            InboxesFacade inboxesFacade = new InboxesFacade();
            InboxesModel inboxesModel = new InboxesModel();
            inboxesModel.setUserId(um.getId());
            inboxesModel.setInboxType(ConstantHelper.INBOX_TYPE_CHANGED_STATUS_IN_COMPANY);
            inboxesModel.setMessage(inboxMessage);
            inboxesModel.setReferenceId(com.getId());
            inboxesModel.setDate(DateHelper.getCurrentTime());
            inboxesFacade.insert(inboxesModel);
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }

    private void notifChangeStatus(String deviceID,String message,CompaniesoutletsModel companiesoutletsModel){
        JSONObject json = new JSONObject();
        try {
            json.put("to",deviceID.trim());
            JSONObject jsonData = new JSONObject();
            jsonData.put(ConstantHelper.NOTIFICATION_TYPE, ConstantHelper.NOTIF_CHANGE_STATUS);
            jsonData.put(ConstantHelper.NOTIFICATION_MESSAGE, message);
            jsonData.put("companiesoutletsmodel", companiesoutletsModel.toJsonObject());
            json.put("data",jsonData);
            try {
                FcmHelper.pushFCMNotification(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void notifRequestJoin(String deviceID,String message,CompaniesoutletsModel companiesoutletsModel){
        JSONObject json = new JSONObject();
        try {
            json.put("to",deviceID.trim());
            JSONObject jsonData = new JSONObject();
            jsonData.put(ConstantHelper.NOTIFICATION_TYPE, ConstantHelper.NOTIF_REQUEST_JOIN);
            jsonData.put(ConstantHelper.NOTIFICATION_MESSAGE, message);
            JSONObject jsonCompanies = companiesoutletsModel.toJsonObject();
            jsonData.put("companiesoutletsmodel", jsonCompanies);
            json.put("data",jsonData);
            try {
                FcmHelper.pushFCMNotification(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}