package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.modify.TopRatedPerson;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;
import org.json.JSONException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class TopRatedPersonHandler extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet(authorizeAccess = false)
    public void handleGET(ServerExchange<AccessTokenPayload> exchange,Filter<TopRatedPerson> filter) throws IOException, SQLException, QueryBuilderException, JSONException {
        List<TopRatedPerson> topRatedPerson = new UsersFacade().getTopRatedPerson(filter);
        ApiResponse response = new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK, topRatedPerson);
        response.setFilter(filter);
        response.setData(topRatedPerson);
        super.sendResponse(exchange, response);
    }
}
