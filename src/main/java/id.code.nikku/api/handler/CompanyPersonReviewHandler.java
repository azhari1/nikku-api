package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.ReviewsFacade;
import id.code.nikku.model.modify.CompanyPersonReviewModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class CompanyPersonReviewHandler extends RouteApiHandler<AccessTokenPayload> {
    private final ReviewsFacade reviewsFacade = new ReviewsFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<CompanyPersonReviewModel> filter) throws Exception {
        final List<CompanyPersonReviewModel> items = this.reviewsFacade.getMyCompanyPersonReviews(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }
}
