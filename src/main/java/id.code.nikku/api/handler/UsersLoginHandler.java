package id.code.nikku.api.handler;

import id.code.component.utility.StringUtility;
import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.PrivacysettingsFacade;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.model.PrivacysettingsModel;
import id.code.nikku.model.UsersModel;
import id.code.nikku.validation.LoginValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class UsersLoginHandler extends RouteApiHandler<AccessTokenPayload> {

    private static final ApiResponse RESPONSE_INVALID_USER_LOGIN = new ApiResponse(STATUS_INVALID_LOGIN, "Invalid email or password");
    private static final ApiResponse RESPONSE_INACTIVE_USER_LOGIN = new ApiResponse(STATUS_INVALID_LOGIN, "Your account is not activate. Please check your email to activate your account");
    private UsersFacade usersFacade = new UsersFacade();

    @HandlerPost(authorizeAccess = false, bypassAllMiddleware = true)
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody LoginValidation login) throws Exception {
        final UsersModel model = this.usersFacade.login(login.getEmail(), login.getPassword());
        if (model == null) {
            super.sendResponse(exchange, RESPONSE_INVALID_USER_LOGIN);
        } else {
            if (model.getIsActive() == ConstantHelper.STATUS_VERYFIED) {
                //update fcm
                String fcmToken = login.getFcmToken();
                if (!StringUtility.isNullOrWhiteSpace(fcmToken)) {
                    model.setFcmToken(fcmToken);
                }
                model.setSessionId(login.getSessionId());
                this.usersFacade.update(model);
                //end fcm
                AccessTokenPayload payload = new AccessTokenPayload(model);
                autoAddPrivacySetting(model);
                model.setAccessToken(super.getAccessTokenValidator().generateAccessToken(payload));
                super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, model));
            } else {
                super.sendResponse(exchange, RESPONSE_INACTIVE_USER_LOGIN);
            }
        }
    }

    public void autoAddPrivacySetting(UsersModel usersModel) throws QueryBuilderException, SQLException, IOException {
        PrivacysettingsFacade psf = new PrivacysettingsFacade();
        PrivacysettingsModel psm = psf.findByUserId(usersModel.getId());
        if (psm == null) {
            psm = new PrivacysettingsModel();
            psm.setUserId(usersModel.getId());
            psm.setBirthDateVisible(1);
            psm.setEmailVisible(1);
            psm.setCompanyVisible(1);
            psm.setPhoneVisible(1);
            psf.insert(psm);
        }
    }
}