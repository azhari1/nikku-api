package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.modify.MyProfilePopularReviews;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class PopularReviewsHandler extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange,Filter<MyProfilePopularReviews> filter, long id) throws IOException, SQLException, QueryBuilderException {
        UsersFacade uf = new UsersFacade();
        List<MyProfilePopularReviews> popularReviews = uf.getMyPopularReviews(exchange.getAccessTokenPayload().getId(), id, filter);
        ApiResponse response = new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK, null);
        response.setData(popularReviews);
        super.sendResponse(exchange, response);
    }
}
