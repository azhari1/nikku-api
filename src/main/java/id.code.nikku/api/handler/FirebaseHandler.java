package id.code.nikku.api.handler;

import id.code.component.utility.StringUtility;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.UsersModel;
import id.code.nikku.validation.UsersFirebaseValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class FirebaseHandler extends RouteApiHandler<AccessTokenPayload> {

    private final UsersFacade usersFacade = new UsersFacade();

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody UsersFirebaseValidation firebaseData, Long id) throws Exception {
        UsersModel newData = usersFacade.findById(id);
        if (newData != null) {
            if(!StringUtility.isNullOrWhiteSpace(firebaseData.getFcmToken())){
                newData.setFcmToken(firebaseData.getFcmToken());
            }
            if (this.usersFacade.update(newData)) {
                super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
            } else {
                super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
            }
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }

    }
}
