package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.model.modify.DetailCompany;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class DetailCompanyHandler extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet(pathTemplate = "{companyId}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, long companyId) throws Exception {
        DetailCompany detailCompany = new CompaniesoutletsFacade().getDetailCompany(companyId);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, detailCompany));
    }
}
