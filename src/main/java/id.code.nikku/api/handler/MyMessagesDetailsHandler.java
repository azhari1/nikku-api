package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.MessagesFacade;
import id.code.nikku.model.MessagesModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class MyMessagesDetailsHandler extends RouteApiHandler<AccessTokenPayload> {

    private final MessagesFacade messagesFacade = new MessagesFacade();

    @HandlerGet(pathTemplate = "{userId}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, long userId) throws QueryBuilderException, SQLException, IOException {
        long loginId = exchange.getAccessTokenPayload().getId();
        final List<MessagesModel> messages = messagesFacade.getDetailMyMessage(loginId,userId);
        if(messages!=null){
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, messages));
        }else{
            super.sendResponse(exchange, new ApiResponse(STATUS_DATA_NOT_FOUND,"No conversation found"));
        }
    }
}
