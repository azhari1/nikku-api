package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.UsersModel;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;

public class LogoutHandler extends RouteApiHandler<AccessTokenPayload> {

    final UsersFacade usersFacade = new UsersFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange) throws QueryBuilderException, SQLException, IOException {
        UsersModel usersModel = this.usersFacade.findById(exchange.getAccessTokenPayload().getId());
        if(usersModel!=null){
            usersModel.setFcmToken(null);
            this.usersFacade.update(usersModel);
            sendResponse(exchange, new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK,"Success logout"));
        }else{
            sendResponse(exchange, new ApiResponse(422,"Failed logout"));
        }
    }
}
