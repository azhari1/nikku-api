package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.facade.UserworkatcompaniesFacade;
import id.code.nikku.model.CompaniesoutletsModel;
import id.code.nikku.model.HomeCompanyDetails;
import id.code.nikku.model.UsersModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class HomeCompanyDetailsHandler extends RouteApiHandler<AccessTokenPayload> {

    final CompaniesoutletsFacade facade = new CompaniesoutletsFacade();
    final UserworkatcompaniesFacade uaf = new UserworkatcompaniesFacade();

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange,long id) throws Exception {
        CompaniesoutletsModel com = this.facade.findById(id);
        if(com!=null){
            long currentUsers = exchange.getAccessTokenPayload().getId();
            List<UsersModel> users = this.uaf.getUsersActive(com.getId());
            HomeCompanyDetails homeCompanyDetails = new HomeCompanyDetails();
            homeCompanyDetails.setJoined(uaf.isUserActive(currentUsers,com.getId()));
            homeCompanyDetails.setCompaniesoutletsModel(com);
            homeCompanyDetails.setUsersModels(users);
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, homeCompanyDetails));
        }else{
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}
