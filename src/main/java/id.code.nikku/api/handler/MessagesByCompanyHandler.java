package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.facade.MessagesFacade;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.helper.FcmHelper;
import id.code.nikku.model.MessagesModel;
import id.code.nikku.model.UsersModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class MessagesByCompanyHandler extends RouteApiHandler<AccessTokenPayload> {
    private final MessagesFacade messagesFacade = new MessagesFacade();
    private final CompaniesoutletsFacade companyFacade = new CompaniesoutletsFacade();

    @HandlerPost(pathTemplate = "{companyId}")
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody MessagesModel newData,long companyId) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());
        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            List<UsersModel> receiverPIC = companyFacade.findOwnerAndPIC(companyId);
            newData.setSenderId(exchange.getAccessTokenPayload().getId());
            newData.setDate(Calendar.getInstance().getTimeInMillis());
            newData.setIsRead(0);
            for (UsersModel receiver : receiverPIC) {
                MessagesModel msg;
                msg = newData;
                msg.setReceiverId(receiver.getId());
                this.messagesFacade.insert(msg);
                sendPushNotification(receiver.getFcmToken(), receiver);
            }
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    private void sendPushNotification(String fcmToken,UsersModel users){
        JSONObject json = new JSONObject();
        try {
            json.put("to",fcmToken);
            JSONObject jsonData = new JSONObject();
            jsonData.put(ConstantHelper.NOTIFICATION_TYPE, ConstantHelper.NOTIF_CHAT);
            jsonData.put(ConstantHelper.NOTIFICATION_MESSAGE, "You have 1 message");
            jsonData.put("users",users.toJsonObject());
            json.put("data",jsonData);
            try {
                FcmHelper.pushFCMNotification(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
