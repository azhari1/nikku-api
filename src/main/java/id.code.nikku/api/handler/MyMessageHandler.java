package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.MessagesFacade;
import id.code.nikku.filter.MessagesFilter;
import id.code.nikku.model.modify.MyMessages;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class MyMessageHandler extends RouteApiHandler<AccessTokenPayload> {
    private final MessagesFacade messagesFacade = new MessagesFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<MessagesFilter> filter) throws Exception {
        long loginId = exchange.getAccessTokenPayload().getId();
        final List<MyMessages> messages = messagesFacade.getAllMyMessage(loginId);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, messages, filter));
    }

}
