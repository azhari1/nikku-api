package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UserworkatcompaniesFacade;
import id.code.nikku.model.UsersModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class UsersOnCompanyHandler extends RouteApiHandler<AccessTokenPayload> {

    private final UserworkatcompaniesFacade userworkatcompaniesFacade = new UserworkatcompaniesFacade();


    @HandlerGet(authorizeAccess = false,pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final List<UsersModel> data = this.userworkatcompaniesFacade.findUsersById(id);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }
}
