package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.InboxesFacade;
import id.code.nikku.facade.ReviewsFacade;
import id.code.nikku.facade.ViewsFacade;
import id.code.nikku.helper.DateHelper;
import id.code.nikku.model.InboxesModel;
import id.code.nikku.model.ReviewsModel;
import id.code.nikku.model.ViewsModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class ReviewsNotificationHandler extends RouteApiHandler<AccessTokenPayload> {

    private final ReviewsFacade reviewsFacade = new ReviewsFacade();
    private final InboxesFacade inboxesFacade = new InboxesFacade();

    @HandlerGet(pathTemplate = "{inboxId}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, long inboxId) throws Exception {
        long id = exchange.getAccessTokenPayload().getId();
        ReviewsModel data = null;
        InboxesModel inboxesModel = inboxesFacade.findById(inboxId);
        if(inboxesModel!=null){
            data = this.reviewsFacade.getAllWithJoin(inboxesModel, id);
            if(data!=null){
                ViewsFacade vf = new ViewsFacade();
                ViewsModel vm = vf.findById(data.getId(),data.getUserId());
                if(vm==null){
                    long totalViews = vf.getTotalViews(data.getUserId());
                    vm = new ViewsModel();
                    vm.setReviewId(data.getId());
                    vm.setUserId(data.getUserId());
                    vm.setDate(DateHelper.getCurrentTime());
                    vf.insert(vm);
                    long newTotalViews = totalViews+1;
                    data.setTotalViews(newTotalViews);
                    this.reviewsFacade.update(data);
                }
                inboxesModel.setIsRead(1);
                inboxesFacade.update(inboxesModel);
            }
        }

        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }
}
