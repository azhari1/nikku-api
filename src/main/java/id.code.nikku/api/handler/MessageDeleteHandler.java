package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.MessagesFacade;
import id.code.nikku.validation.MessagesDeleteValidation;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

public class MessageDeleteHandler extends RouteApiHandler<AccessTokenPayload> {
    private final MessagesFacade messagesFacade = new MessagesFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody MessagesDeleteValidation newData) throws Exception {
        boolean allDeleted = true;
        for(int i=0;i<newData.getDeleted().size();i++){
            long senderId = newData.getDeleted().get(i).getSenderId();
            long receiverId = newData.getDeleted().get(i).getReceiverId();
            if(!messagesFacade.findAndDelete(senderId,receiverId)){
                allDeleted = false;
            }
        }
        if(allDeleted){
            sendResponse(exchange, new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK, newData));
        }else{
            sendResponse(exchange, new ApiResponse(422, "Cannot remove your message selected. Please try again"));
        }

    }
}
