package id.code.nikku.api.handler;

import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.filter.CompaniesoutletsFilter;
import id.code.nikku.helper.ImageHelper;
import id.code.nikku.model.CompaniesoutletsModel;
import id.code.nikku.validation.CompanyoutletCreateValidation;
import id.code.nikku.validation.CompanyoutletEditValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.Calendar;
import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CompaniesoutletsHandler extends RouteApiHandler<AccessTokenPayload> {

    private final static int STATUS_UPLOAD_FAILED = 1990;
    private static final ApiResponse RESPONSE_UPLOAD_FAILED = new ApiResponse(STATUS_UPLOAD_FAILED, "Failed upload your picture");
    private final CompaniesoutletsFacade companiesoutletsFacade = new CompaniesoutletsFacade();

    @HandlerGet(authorizeAccess = false)
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<CompaniesoutletsFilter> filter) throws Exception {
        final List<CompaniesoutletsModel> items = this.companiesoutletsFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(authorizeAccess = false,pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final CompaniesoutletsModel data = this.companiesoutletsFacade.findById(id);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody CompanyoutletCreateValidation createData) throws Exception {
        CompaniesoutletsModel newData = new CompaniesoutletsModel();
        newData.setCompanyName(createData.getCompanyName());
        newData.setOutletName(createData.getOutletName());
        newData.setTypeOfBusinessId(createData.getTypeOfBusinessId());
        newData.setAddress(createData.getAddress());
        newData.setSiupTdpnumber(createData.getSiupTdpnumber());
        newData.setPhone(createData.getPhone());
        newData.setEmail(createData.getEmail());
        newData.setWebsite(createData.getWebsite());
        newData.setOwner(exchange.getAccessTokenPayload().getId());
        newData.setCreatedBy(exchange.getAccessTokenPayload().getClaimName());
        newData.setDateCreated(Calendar.getInstance().getTimeInMillis());
        newData.setIsFeatured(0);
        newData.setIsDisabled(0);
        newData.setOrderList(0);
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            if(this.companiesoutletsFacade.siupIsExists(newData.getSiupTdpnumber())){
                super.sendResponse(exchange, new ApiResponse(422,"Your siup/tdp Is Already Exists"));
            }else{
                String logoName = ImageHelper.saveCompanyFile(createData.getLogo());
                if(!StringUtility.isNullOrWhiteSpace(logoName)){
                    newData.setLogo(logoName);
                    this.companiesoutletsFacade.insert(newData);
                    super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
                }else{
                    ImageHelper.deleteCompanyFile(logoName);
                    super.sendResponse(exchange, RESPONSE_UPLOAD_FAILED);
                }
            }
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final boolean deleted = this.companiesoutletsFacade.delete(id);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange,@RequestBody CompanyoutletEditValidation dataEdit, Long id) throws Exception {
        CompaniesoutletsModel newData = companiesoutletsFacade.findById(id);
        if(newData!=null){
            newData.setCompanyName(dataEdit.getCompanyName());
            newData.setOutletName(dataEdit.getOutletName());
            newData.setTypeOfBusinessId(dataEdit.getTypeOfBusinessId());
            newData.setPhone(dataEdit.getPhone());
            newData.setAddress(dataEdit.getAddress());
            newData.setEmail(dataEdit.getEmail());
            newData.setWebsite(dataEdit.getWebsite());
            if(dataEdit.getLogo()!=null){
                newData.setLogo(ImageHelper.updateCompanyFile(newData.getLogo(),dataEdit.getLogo()));
            }
            if (this.companiesoutletsFacade.update(newData)) {
                super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
            } else {
                super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
            }
        }else{
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }

    }
}