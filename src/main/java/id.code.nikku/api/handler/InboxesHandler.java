package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.facade.InboxesFacade;
import id.code.nikku.facade.ReviewsFacade;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.filter.InboxesFilter;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.model.CompaniesoutletsModel;
import id.code.nikku.model.InboxesModel;
import id.code.nikku.model.ReviewsModel;
import id.code.nikku.model.UsersModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class InboxesHandler extends RouteApiHandler<AccessTokenPayload> {
    private final InboxesFacade inboxesFacade = new InboxesFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<InboxesFilter> filter) throws Exception {
        final List<InboxesModel> items = this.inboxesFacade.getAll(filter);
        ReviewsFacade reviewsFacade = new ReviewsFacade();
        for (InboxesModel item : items) {
            long referenceId = item.getReferenceId();
            if (item.getInboxType() == ConstantHelper.INBOX_TYPE_GOT_REVIEW_IN_COMPANY ||
                    item.getInboxType() == ConstantHelper.INBOX_TYPE_CHANGED_STATUS_IN_COMPANY) {
                CompaniesoutletsModel cp = new CompaniesoutletsFacade().findById(referenceId);
                item.setCompaniesoutletsModel(cp);
            } else if(item.getInboxType() == ConstantHelper.INBOX_TYPE_REVIEW_AS_PIC){
                ReviewsModel rv = reviewsFacade.findById(item.getReferenceId());
                CompaniesoutletsModel cp = new CompaniesoutletsFacade().findById(rv.getAsCompanyId());
                UsersModel um = new UsersFacade().findById(rv.getReviewerId());
                item.setUsersModel(um);
                item.setCompaniesoutletsModel(cp);
            }else {
                UsersModel um = new UsersFacade().findById(referenceId);
                item.setUsersModel(um);
            }
        }
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final InboxesModel data = this.inboxesFacade.findById(id);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody InboxesModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.inboxesFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final boolean deleted = this.inboxesFacade.delete(id);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange,@RequestBody InboxesModel newData, Long id) throws Exception {
        newData.setId(id);

        if (this.inboxesFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}