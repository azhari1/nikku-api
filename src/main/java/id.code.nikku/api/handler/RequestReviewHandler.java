package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.InboxesFacade;
import id.code.nikku.facade.RequestReviewFacade;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.filter.RequestReviewFilter;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.helper.DateHelper;
import id.code.nikku.helper.FcmHelper;
import id.code.nikku.model.InboxesModel;
import id.code.nikku.model.RequestReviewModel;
import id.code.nikku.model.UsersModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class RequestReviewHandler extends RouteApiHandler<AccessTokenPayload> {
    
    private final RequestReviewFacade requestReviewFacade = new RequestReviewFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<RequestReviewFilter> filter) throws Exception {
        final List<RequestReviewModel> items = this.requestReviewFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final RequestReviewModel data = this.requestReviewFacade.findById(id);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody RequestReviewModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            RequestReviewModel oldData = this.requestReviewFacade.findAlreadyExists(newData.getRequester(), newData.getApprover());
            if(oldData==null){
                newData.setRequestDate(DateHelper.getCurrentTime());
                this.requestReviewFacade.insert(newData);
            }else{
                newData.setRequestDate(DateHelper.getCurrentTime());
                oldData.setStatus(ConstantHelper.RFR_WAITING);
                this.requestReviewFacade.update(oldData);
            }
            String message = "request for review";
            InboxesModel inboxesModel = new InboxesModel();
            inboxesModel.setUserId(newData.getApprover());
            inboxesModel.setInboxType(ConstantHelper.INBOX_TYPE_REQUEST_REVIEW_WAITING);
            inboxesModel.setReferenceId(newData.getRequester());
            inboxesModel.setMessage(message);
            inboxesModel.setDate(DateHelper.getCurrentTime());
            //to person review
            new InboxesFacade().insert(inboxesModel);
            //send Push Notif
            UsersModel receiver = new UsersFacade().findById(newData.getApprover());
            if(receiver!=null && receiver.getFcmToken()!=null){
                sendPushNotification(receiver.getFcmToken(), inboxesModel, "You have got 1 request for review");
            }

            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final boolean deleted = this.requestReviewFacade.delete(id);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody RequestReviewModel newData, Long id) throws Exception {
        newData.setId(id);
        if(newData.getStatus()==ConstantHelper.RFR_APPROVAL){
            newData.setRequestDate(DateHelper.getCurrentTime());
            String message = "has been approved your request for review";
            InboxesModel inboxesModel = new InboxesModel();
            inboxesModel.setUserId(newData.getRequester());
            inboxesModel.setInboxType(ConstantHelper.INBOX_TYPE_REQUEST_REVIEW_APPROVAL);
            inboxesModel.setReferenceId(newData.getApprover());
            inboxesModel.setMessage(message);
            inboxesModel.setDate(DateHelper.getCurrentTime());
            //to person review
            new InboxesFacade().insert(inboxesModel);
            //send Push Notif
            UsersModel receiver = new UsersFacade().findById(newData.getRequester());
            if(receiver!=null && receiver.getFcmToken()!=null){
                sendPushNotification(receiver.getFcmToken(), inboxesModel, message);
            }
        }else if(newData.getStatus()==ConstantHelper.RFR_REJECTED){
            String message = "has been rejected your request for review";
            InboxesModel inboxesModel = new InboxesModel();
            inboxesModel.setUserId(newData.getRequester());
            inboxesModel.setInboxType(ConstantHelper.INBOX_TYPE_REQUEST_REVIEW_REJECTED);
            inboxesModel.setReferenceId(newData.getApprover());
            inboxesModel.setMessage(message);
            inboxesModel.setDate(DateHelper.getCurrentTime());
            //to person review
            new InboxesFacade().insert(inboxesModel);
            //send Push Notif
            UsersModel receiver = new UsersFacade().findById(newData.getRequester());
            if(receiver!=null && receiver.getFcmToken()!=null){
                sendPushNotification(receiver.getFcmToken(), inboxesModel, message);
            }
        }
        if (this.requestReviewFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }

    private void sendPushNotification(String fcmToken,InboxesModel inboxesModel,String message){
        JSONObject json = new JSONObject();
        try {
            json.put("to",fcmToken);
            JSONObject jsonData = new JSONObject();
            jsonData.put(ConstantHelper.NOTIFICATION_TYPE, ConstantHelper.NOTIF_REQUEST_REVIEW);
            jsonData.put(ConstantHelper.NOTIFICATION_MESSAGE, message);
            jsonData.put("inboxesModel",inboxesModel.toJsonObject());
            json.put("data",jsonData);
            try {
                FcmHelper.pushFCMNotification(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
