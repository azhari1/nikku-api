package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CountItemNotificationFacade;
import id.code.nikku.validation.CountNotificationValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class CountItemNotificationHandler extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange,Filter<CountNotificationValidation> filter) throws Exception {
        long userId = exchange.getAccessTokenPayload().getId();
        CountNotificationValidation counter = new CountItemNotificationFacade().getCountItemNotification(userId);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, counter, filter));
    }
}
