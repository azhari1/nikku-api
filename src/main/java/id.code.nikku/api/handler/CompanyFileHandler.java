package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.server.ApiSimpleFileHandler;
import id.code.server.ServerExchange;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CompanyFileHandler extends ApiSimpleFileHandler<AccessTokenPayload> {

    private static final Path output = Paths.get("data/upload/company");

    public CompanyFileHandler() throws IOException {
        super(output, false);
    }

    @Override
    protected boolean authorizeAccess(ServerExchange<AccessTokenPayload> exchange) {
        return false;
    }
}
