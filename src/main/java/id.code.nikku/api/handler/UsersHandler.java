package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.PrivacysettingsFacade;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.filter.UsersFilter;
import id.code.nikku.helper.ImageHelper;
import id.code.nikku.model.UsersModel;
import id.code.nikku.validation.UserEditValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class UsersHandler extends RouteApiHandler<AccessTokenPayload> {

    private final UsersFacade usersFacade = new UsersFacade();
    private final PrivacysettingsFacade privacysettingsFacade = new PrivacysettingsFacade();

    @HandlerGet(authorizeAccess = false)
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<UsersFilter> filter) throws Exception {
        final List<UsersModel> items = this.usersFacade.getAllWithJoin(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final UsersModel data = this.usersFacade.findById(id);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody UsersModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.usersFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final boolean deleted = this.usersFacade.delete(id);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange,@RequestBody UserEditValidation dataEdit, Long id) throws Exception {
        int isMerge = 1;
        if(dataEdit.getIsMerge()== isMerge){
            if(UsersUpdateNikHandler.doMerge(dataEdit.getId(), exchange.getAccessTokenPayload().getId())){
                UsersModel umOld = this.usersFacade.findById(dataEdit.getId());
                UsersModel umNew = this.usersFacade.findById(exchange.getAccessTokenPayload().getId());
                umNew.setNik(umOld.getNik());
                umNew.setKtpPhoto(umOld.getKtpPhoto());
                //
                if(dataEdit.getProfilePicture()!=null){
                    umNew.setProfilePicture(ImageHelper.updateUserFile(umNew.getProfilePicture(), dataEdit.getProfilePicture()));
                }
                umNew.setFullName(dataEdit.getFullName());
                umNew.setJobTitle(dataEdit.getJobTitle());
                umNew.setNik(dataEdit.getNik());
                umNew.setBio(dataEdit.getBio());
                umNew.setSex(dataEdit.getSex());
                umNew.setDateOfBirth(dataEdit.getDateOfBirth());
                umNew.setCityId(dataEdit.getCityId());
                umNew.setPhone(dataEdit.getPhone());
                //
                if(this.privacysettingsFacade.delete(umOld.getId())){
                    this.usersFacade.delete(umOld.getId());
                    this.usersFacade.update(umNew);
                    ApiResponse apiResponse = new ApiResponse(200, "Your data has been updated and merge from old profile");
                    apiResponse.setData(umNew);
                    super.sendResponse(exchange, apiResponse);
                }else{
                    super.sendResponse(exchange, new ApiResponse(422, "Your data failed to update and merge!!!"));
                }
            }
        }else{
            UsersModel newData = usersFacade.findById(id);
            if(newData!=null){
                boolean isNikAvailable = usersFacade.isNIKAvailable(dataEdit.getNik(), exchange.getAccessTokenPayload());
                if(isNikAvailable){
                    if(dataEdit.getProfilePicture()!=null){
                        newData.setProfilePicture(ImageHelper.updateUserFile(newData.getProfilePicture(), dataEdit.getProfilePicture()));
                    }
                    newData.setFullName(dataEdit.getFullName());
                    newData.setJobTitle(dataEdit.getJobTitle());
                    newData.setNik(dataEdit.getNik());
                    newData.setBio(dataEdit.getBio());
                    newData.setSex(dataEdit.getSex());
                    newData.setDateOfBirth(dataEdit.getDateOfBirth());
                    newData.setCityId(dataEdit.getCityId());
                    newData.setEmail(dataEdit.getEmail());
                    newData.setPhone(dataEdit.getPhone());
                    if (this.usersFacade.update(newData)) {
                        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
                    } else {
                        super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
                    }
                }else{
                    UsersModel usersMerge = usersFacade.isNIkAvailableForMerge(dataEdit.getNik());
                    if (usersMerge != null) {
                        ApiResponse apiResponse = new ApiResponse(423, "Failed to update your profile. Need Merge? !!!");
                        apiResponse.setData(usersMerge);
                        super.sendResponse(exchange, apiResponse);
                    } else {
                        super.sendResponse(exchange, new ApiResponse(422, "Failed to update your profile. Nik is already exists !!!"));
                    }
                }
            }else{
                super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
            }
        }
    }
}