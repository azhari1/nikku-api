package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.filter.UsersSearchFilter;
import id.code.nikku.model.UsersModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class UsersSearchHandler extends RouteApiHandler<AccessTokenPayload> {

    private final UsersFacade usersFacade = new UsersFacade();

    @HandlerGet(authorizeAccess = false)
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<UsersSearchFilter> filter) throws Exception {
        final List<UsersModel> items = this.usersFacade.getFromSearch(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }
}
