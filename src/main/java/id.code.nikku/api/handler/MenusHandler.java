package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.MenusFacade;
import id.code.nikku.filter.MenusFilter;
import id.code.nikku.model.MenusModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class MenusHandler extends RouteApiHandler<AccessTokenPayload> {
    private final MenusFacade menusFacade = new MenusFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<MenusFilter> filter) throws Exception {
        final List<MenusModel> items = this.menusFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final MenusModel data = this.menusFacade.findById(id);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody MenusModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.menusFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final boolean deleted = this.menusFacade.delete(id);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange,@RequestBody MenusModel newData, Long id) throws Exception {
        newData.setId(id);

        if (this.menusFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}