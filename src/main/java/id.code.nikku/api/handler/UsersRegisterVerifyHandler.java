package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.UsersModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;

public class UsersRegisterVerifyHandler extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet(authorizeAccess = false, bypassAllMiddleware = true,pathTemplate = "{code}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange,String code) throws IOException, SQLException, QueryBuilderException {
        UsersFacade uf = new UsersFacade();
        UsersModel um = uf.verify(code);
        if(um!=null){
            if(um.getIsVerified()==1){
                super.sendResponse(exchange,new ApiResponse(200, "Your account is already verified"));
            }else{
                um.setIsVerified(1);
                if(uf.update(um)){
                    super.sendResponse(exchange,new ApiResponse(200, "Your account is verified"));
                }else{
                    super.sendResponse(exchange,new ApiResponse(200, "Your account failed to verify"));
                }
            }
        }else{
            super.sendResponse(exchange,new ApiResponse(499, "Your account cannot verify"));
        }
    }
}
