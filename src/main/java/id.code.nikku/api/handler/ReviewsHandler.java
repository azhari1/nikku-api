package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.*;
import id.code.nikku.filter.ReviewsFilter;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.helper.DateHelper;
import id.code.nikku.helper.FcmHelper;
import id.code.nikku.helper.ImageHelper;
import id.code.nikku.model.*;
import id.code.nikku.validation.ReviewValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class ReviewsHandler extends RouteApiHandler<AccessTokenPayload> {

    private final ReviewsFacade reviewsFacade = new ReviewsFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<ReviewsFilter> filter) throws Exception {
        long id = exchange.getAccessTokenPayload().getId();
        final List<ReviewsModel> items = this.reviewsFacade.getAllWithJoin(filter, id);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, long id) throws Exception {
        ReviewsModel data = this.reviewsFacade.findById(id);
        if(data!=null){
            ViewsFacade vf = new ViewsFacade();
            ViewsModel vm = vf.findById(id, exchange.getAccessTokenPayload().getId());
            if(vm==null){
                long totalViews = vf.getTotalViews(data.getId());
                vm = new ViewsModel();
                vm.setReviewId(data.getId());
                vm.setUserId(exchange.getAccessTokenPayload().getId());
                vm.setDate(DateHelper.getCurrentTime());
                vf.insert(vm);
                data.setTotalViews(totalViews+1);
                this.reviewsFacade.update(data);
            }
        }
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody ReviewValidation dataReview) throws Exception {
        ReviewsModel newData = new ReviewsModel();
        newData.setUserId(dataReview.getUserId());
        newData.setReviewerId(exchange.getAccessTokenPayload().getId());
        newData.setReviewType(dataReview.getReviewType());
        newData.setAsCompanyId(dataReview.getAsCompanyId());
        newData.setDate(Calendar.getInstance(TimeZone.getTimeZone("Asia/Jakarta")).getTimeInMillis());
        if (dataReview.getReviewType() == ConstantHelper.REVIEW_AS_PUBLIC) {
            newData.setQuestionnaire1(dataReview.getQuestionnaire1());
            newData.setQuestionnaire2(dataReview.getQuestionnaire2());
            newData.setQuestionnaire3(dataReview.getQuestionnaire3());
            newData.setQuestionnaire4(dataReview.getQuestionnaire4());
            newData.setQuestionnaire5(dataReview.getQuestionnaire5());
        } else {
            newData.setQuestionnaire1(dataReview.getQuestionnaire1());
            newData.setQuestionnaire2(5);
            newData.setQuestionnaire3(5);
            newData.setQuestionnaire4(5);
            newData.setQuestionnaire5(5);
        }
        newData.setRatingScore(calculateRatingScore(newData));
        newData.setComment(dataReview.getComment());

        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            if (dataReview.getPhoto() != null) {
                String imageName = ImageHelper.saveReviewFile(dataReview.getPhoto());
                if (imageName != null) {
                    newData.setPhoto(imageName);
                }
            }
            long lastInsertedId = this.reviewsFacade.insertLastId(newData);
            if(lastInsertedId > 0){
//                RequestReviewFacade rrf = new RequestReviewFacade();
//                RequestReviewModel rrm = rrf.findById(dataReview.getRequestReviewId());
//                rrm.setStatus(ConstantHelper.RFR_CLOSING);
//                rrf.update(rrm);
                this.autoUpdateInboxes(newData);
                super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
            }else{
                super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, "Ops something wrong, failed save your review"));
            }
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final boolean deleted = this.reviewsFacade.delete(id);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange,@RequestBody ReviewsModel newData, Long id) throws Exception {
        newData.setId(id);

        if (this.reviewsFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }


    private double calculateRatingScore(ReviewsModel newData) {
        int max_question = 5;
        double totalScore = 0;
        totalScore += newData.getQuestionnaire1();
        totalScore += newData.getQuestionnaire2();
        totalScore += newData.getQuestionnaire3();
        totalScore += newData.getQuestionnaire4();
        totalScore += newData.getQuestionnaire5();
        return totalScore / max_question;
    }

    private void autoUpdateInboxes(ReviewsModel reviewsModel) throws IOException, SQLException, QueryBuilderException {
        int inboxType = 0;
        String message = "";
        InboxesModel inboxesModel = new InboxesModel();
        switch (reviewsModel.getReviewType()) {
            case ConstantHelper.REVIEW_AS_PUBLIC:
                inboxType = ConstantHelper.INBOX_TYPE_REVIEW_AS_PUBLIC;
                inboxesModel.setReferenceId(reviewsModel.getReviewerId());
                message = "reviewing you as public";
                break;
            case ConstantHelper.REVIEW_AS_COSTUMER:
                inboxType = ConstantHelper.INBOX_TYPE_REVIEW_AS_CUSTOMER;
                inboxesModel.setReferenceId(reviewsModel.getReviewerId());
                message = "reviewing you as customer";
                break;
            case ConstantHelper.REVIEW_AS_PIC:
                inboxType = ConstantHelper.INBOX_TYPE_REVIEW_AS_PIC;
                inboxesModel.setReferenceId(reviewsModel.getId());
                message = "reviewing you as pic";
                break;
        }
        inboxesModel.setUserId(reviewsModel.getUserId());
        inboxesModel.setInboxType(inboxType);
        inboxesModel.setMessage(message);
        inboxesModel.setDate(reviewsModel.getDate());
        //to person review
        new InboxesFacade().insert(inboxesModel);
        //send Push Notif
        UsersModel receiver = new UsersFacade().findById(reviewsModel.getUserId());
        if(receiver!=null && receiver.getFcmToken()!=null){
            sendPushNotification(receiver.getFcmToken(), inboxesModel);
        }
        autoUpdateInboxesWithCompany(reviewsModel);
    }

    private void autoUpdateInboxesWithCompany(ReviewsModel reviewsModel) throws QueryBuilderException, SQLException, IOException {
        UsersFacade uf = new UsersFacade();
        UserworkatcompaniesFacade uwc = new UserworkatcompaniesFacade();
        CompaniesoutletsFacade cof = new CompaniesoutletsFacade();
        CompaniesoutletsModel com = cof.findById(reviewsModel.getAsCompanyId());
        if (com != null) {
            UsersModel um = uf.findById(com.getOwner());
            if (um != null) {
                String message = "One of your employee got a review in <b>" + com.getCompanyName() + "</b>";
                InboxesModel inboxesModel = new InboxesModel();
                inboxesModel.setUserId(um.getId());
                inboxesModel.setInboxType(ConstantHelper.INBOX_TYPE_GOT_REVIEW_IN_COMPANY);
                inboxesModel.setReferenceId(com.getId());
                inboxesModel.setMessage(message);
                inboxesModel.setDate(reviewsModel.getDate());
                new InboxesFacade().insert(inboxesModel);
                sendPushNotification(um.getFcmToken(), inboxesModel,"One of your employee got a review");
            }
        } else {
            //cari company yang aktif
            List<UserworkatcompaniesModel> uats = uwc.findByUserId(reviewsModel.getUserId());
            for (UserworkatcompaniesModel uat : uats) {
                Date lastJoined = new Date(uat.getEndDate());
                if (uat.getStatus() == ConstantHelper.STATUS_APPROVED
                        && lastJoined.compareTo(Calendar.getInstance().getTime()) > 0) {
                    String message = "One of your employee got a review in <b>" + uat.getCompaniesoutletsModel().getCompanyName() + "</b>";
                    InboxesModel inboxesModel = new InboxesModel();
                    inboxesModel.setUserId(uat.getCompaniesoutletsModel().getOwner());
                    inboxesModel.setInboxType(ConstantHelper.INBOX_TYPE_GOT_REVIEW_IN_COMPANY);
                    inboxesModel.setReferenceId(uat.getCompanyId());
                    inboxesModel.setMessage(message);
                    inboxesModel.setDate(reviewsModel.getDate());
                    new InboxesFacade().insert(inboxesModel);
                    sendPushNotification(uat.getUsersModel().getFcmToken(),inboxesModel, "One of your employee got a review");
                }
            }
        }
    }

    private void sendPushNotification(String fcmToken,InboxesModel inboxesModel,String message){
        JSONObject json = new JSONObject();
        try {
            json.put("to",fcmToken);
            JSONObject jsonData = new JSONObject();
            jsonData.put(ConstantHelper.NOTIFICATION_TYPE, ConstantHelper.NOTIF_COMPANY_PERSON_REVIEWS);
            jsonData.put(ConstantHelper.NOTIFICATION_MESSAGE, message);
            jsonData.put("inboxesModel",inboxesModel.toJsonObject());
            json.put("data",jsonData);
            try {
                FcmHelper.pushFCMNotification(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendPushNotification(String fcmToken,InboxesModel inboxesModel){
        JSONObject json = new JSONObject();
        try {
            json.put("to",fcmToken);
            JSONObject jsonData = new JSONObject();
            if(inboxesModel.getInboxType()==ConstantHelper.INBOX_TYPE_REVIEW_AS_PUBLIC){
                jsonData.put(ConstantHelper.NOTIFICATION_TYPE, ConstantHelper.NOTIF_REVIEW_AS_PUBLIC);
            }else if(inboxesModel.getInboxType()==ConstantHelper.INBOX_TYPE_REVIEW_AS_CUSTOMER){
                jsonData.put(ConstantHelper.NOTIFICATION_TYPE, ConstantHelper.NOTIF_REVIEW_AS_CUSTOMER);
            }
            jsonData.put(ConstantHelper.NOTIFICATION_MESSAGE, "You have got 1 review");
            jsonData.put("inboxesModel",inboxesModel.toJsonObject());
            json.put("data",jsonData);
            try {
                FcmHelper.pushFCMNotification(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}