package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.model.modify.MyCompany;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class MyCompanyHandler extends RouteApiHandler<AccessTokenPayload> {

    private final CompaniesoutletsFacade companiesoutletsFacade = new CompaniesoutletsFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange) throws IOException, SQLException, QueryBuilderException {
        long userLoginId = exchange.getAccessTokenPayload().getId();
        final List<MyCompany> items = this.companiesoutletsFacade.getMyCompany(userLoginId);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items));
    }
}
