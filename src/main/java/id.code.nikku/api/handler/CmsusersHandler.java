package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CmsusersFacade;
import id.code.nikku.filter.CmsusersFilter;
import id.code.nikku.model.CmsusersModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CmsusersHandler extends RouteApiHandler<AccessTokenPayload> {
    private final CmsusersFacade cmsusersFacade = new CmsusersFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<CmsusersFilter> filter) throws Exception {
        final List<CmsusersModel> items = this.cmsusersFacade.getAll(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final CmsusersModel data = this.cmsusersFacade.findById(id);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody CmsusersModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            this.cmsusersFacade.insert(newData);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final boolean deleted = this.cmsusersFacade.delete(id);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange,@RequestBody CmsusersModel newData, Long id) throws Exception {
        newData.setId(id);
        if (this.cmsusersFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }
}