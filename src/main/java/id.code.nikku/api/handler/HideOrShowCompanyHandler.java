package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UserworkatcompaniesFacade;
import id.code.nikku.model.UserworkatcompaniesModel;
import id.code.nikku.validation.HideOrShowCompanyValidation;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPut;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;

public class HideOrShowCompanyHandler extends RouteApiHandler<AccessTokenPayload> {
    private final UserworkatcompaniesFacade userworkatcompaniesFacade = new UserworkatcompaniesFacade();

    @HandlerPut
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody HideOrShowCompanyValidation validation) throws QueryBuilderException, SQLException, IOException {
        UserworkatcompaniesModel uat = this.userworkatcompaniesFacade.findById(validation.getId());
        uat.setIsHide(validation.getIsHide());
        this.userworkatcompaniesFacade.update(uat);
        super.sendResponse(exchange, new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK, validation));
    }
}
