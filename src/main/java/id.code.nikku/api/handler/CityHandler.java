package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CityFacade;
import id.code.nikku.filter.CityFilter;
import id.code.nikku.model.CityModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CityHandler extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet(authorizeAccess = false, bypassAllMiddleware = true)
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<CityFilter> filter) throws Exception {
        final CityFacade cityFacade = new CityFacade();
        final List<CityModel> items = cityFacade.getWithProvince(filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

}