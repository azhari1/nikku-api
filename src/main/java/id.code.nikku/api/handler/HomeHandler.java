package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.modify.FeaturedCompany;
import id.code.nikku.model.modify.Home;
import id.code.nikku.model.modify.TopRatedPerson;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;
import org.json.JSONException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class HomeHandler extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange) throws IOException, SQLException, QueryBuilderException, JSONException {
        List<TopRatedPerson> topRatedPerson = new UsersFacade().getTopRatedPerson();
        List<FeaturedCompany> featuredCompanies = new CompaniesoutletsFacade().getFeaturedCompany();
        Home home = new Home();
        home.setTopRatedPersons(topRatedPerson);
        home.setFeaturedCompanies(featuredCompanies);
        ApiResponse response = new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK, null);
        response.setData(home);
        super.sendResponse(exchange, response);
    }
}
