package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.modify.MyProfile;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class MyProfileHandler extends RouteApiHandler<AccessTokenPayload> {

    private final UsersFacade usersFacade = new UsersFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<MyProfile> filter) throws IOException, SQLException, QueryBuilderException {
        MyProfile myProfile = this.usersFacade.getMyProfile(exchange.getAccessTokenPayload().getId(),filter);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, myProfile, filter));
    }
}
