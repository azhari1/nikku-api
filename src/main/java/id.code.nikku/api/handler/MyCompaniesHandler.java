package id.code.nikku.api.handler;

import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.filter.CompaniesoutletsFilter;
import id.code.nikku.model.CompaniesoutletsModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class MyCompaniesHandler extends RouteApiHandler<AccessTokenPayload> {

    private final CompaniesoutletsFacade companiesoutletsFacade = new CompaniesoutletsFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<CompaniesoutletsFilter> filter) throws Exception {
        long userId = exchange.getAccessTokenPayload().getId();
        final List<CompaniesoutletsModel> items = this.companiesoutletsFacade.getAllWithType(filter,userId);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }
}
