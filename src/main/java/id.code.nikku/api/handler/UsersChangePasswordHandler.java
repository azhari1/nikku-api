package id.code.nikku.api.handler;

import id.code.component.PasswordHasher;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.UsersModel;
import id.code.nikku.validation.ChangePasswordValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class UsersChangePasswordHandler extends RouteApiHandler<AccessTokenPayload> {
    private UsersFacade usersFacade = new UsersFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody ChangePasswordValidation value) throws Exception {
        final UsersModel model = this.usersFacade.findById(exchange.getAccessTokenPayload().getId());
        if (model == null || !PasswordHasher.verify(value.getOldPassword(), model.getPassword())) {
            super.sendResponse(exchange, new ApiResponse(422,"Your current password is wrong"));
        } else {
            this.usersFacade.changePassword(model, value.getNewPassword());
            super.sendResponse(exchange, new ApiResponse(200, "Your password has been changed"));
        }
    }
}