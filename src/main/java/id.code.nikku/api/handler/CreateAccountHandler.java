package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.PrivacysettingsFacade;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.helper.DateHelper;
import id.code.nikku.helper.ImageHelper;
import id.code.nikku.model.PrivacysettingsModel;
import id.code.nikku.model.UsersModel;
import id.code.nikku.model.modify.CreateAccount;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.UUID;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class CreateAccountHandler extends RouteApiHandler<AccessTokenPayload> {

    private final UsersFacade usersFacade = new UsersFacade();

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody CreateAccount newData) throws Exception {
        if(this.usersFacade.isNIKAvailable(newData.getNik())){
            newData.setEmail(newData.getNik()+"@gmail.com");
            newData.setDateOfBirth(DateHelper.getCurrentTime());
            newData.setSex(0);
            newData.setCityId(1);
            newData.setProfilePicture("");
            newData.setUid(UUID.randomUUID().toString());
            newData.setIsActive(1);
            newData.setSessionId("");
            newData.setDateCreated(DateHelper.getCurrentTime());
            String imageName = ImageHelper.saveUserKtp(newData.getKtpPhoto());
            newData.setKtpPhoto(imageName);
            if(this.usersFacade.insertAccount(newData)){
                UsersModel um = this.usersFacade.findByEmail(newData.getEmail());
                autoAddPrivacySetting(um);
                super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
            }else{
                super.sendResponse(exchange, new ApiResponse(422, "Failed create your account"));
            }
        }else{
            super.sendResponse(exchange, new ApiResponse(422, "Your nik is already exists"));
        }
    }

    private void autoAddPrivacySetting(UsersModel usersModel) throws QueryBuilderException, SQLException, IOException {
        PrivacysettingsFacade psf = new PrivacysettingsFacade();
        PrivacysettingsModel psm = psf.findByUserId(usersModel.getId());
        if (psm == null) {
            psm = new PrivacysettingsModel();
            psm.setUserId(usersModel.getId());
            psm.setBirthDateVisible(1);
            psm.setEmailVisible(1);
            psm.setCompanyVisible(1);
            psm.setPhoneVisible(1);
            psf.insert(psm);
        }
    }
}
