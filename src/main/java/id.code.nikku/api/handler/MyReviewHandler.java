package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.ReviewsFacade;
import id.code.nikku.model.modify.MyReview;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;
import org.json.JSONException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class MyReviewHandler extends RouteApiHandler<AccessTokenPayload> {

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<MyReview> filter) throws IOException, SQLException, QueryBuilderException, JSONException {
        long userLoginId = exchange.getAccessTokenPayload().getId();
        List<MyReview> myReviews = new ReviewsFacade().getMyReviews(userLoginId);
        ApiResponse response = new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK, myReviews);
        response.setFilter(filter);
        response.setData(myReviews);
        super.sendResponse(exchange, response);
    }
}
