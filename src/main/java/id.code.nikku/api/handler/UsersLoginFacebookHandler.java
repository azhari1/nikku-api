package id.code.nikku.api.handler;

import id.code.component.utility.StringUtility;
import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.model.UsersModel;
import id.code.nikku.validation.UsersFacebookValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class UsersLoginFacebookHandler extends RouteApiHandler<AccessTokenPayload> {

    private final UsersFacade usersFacade = new UsersFacade();
    private static final int COMPLETE_REGISTRATION = 201;
    private static final ApiResponse RESPONSE_UNVERYFIED_USER_LOGIN = new ApiResponse(STATUS_INVALID_LOGIN, "Your account is not activate. Please check your email to activate your account");

    @HandlerPost(authorizeAccess = false)
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody UsersFacebookValidation login) throws SQLException, GeneralSecurityException, QueryBuilderException, IOException {
        final UsersModel model = this.usersFacade.loginFacebook(login.getEmail(), login.getGoogleId());
        if (model == null) {
            UsersModel usersFacebook = new UsersModel();
            usersFacebook.setGoogleId(login.getGoogleId());
            usersFacebook.setProfilePicture(login.getProfilePicture());
            usersFacebook.setFcmToken(login.getFullName());
            usersFacebook.setEmail(login.getEmail());
            usersFacebook.setSex(login.getSex());
            usersFacebook.setDateOfBirth(login.getDateOfBirth());
            usersFacebook.setIsActive(1);//hilangkan apabila email ok
            super.sendResponse(exchange, new ApiResponse(COMPLETE_REGISTRATION, usersFacebook));
        } else {
            if (model.getIsActive() == ConstantHelper.STATUS_VERYFIED) {
                //update fcm
                String fcmToken = login.getFcmToken();
                if (!StringUtility.isNullOrWhiteSpace(fcmToken)) {
                    model.setFcmToken(fcmToken);
                    this.usersFacade.update(model);
                }
                //end fcm
                final AccessTokenPayload payload = new AccessTokenPayload(model);
                model.setAccessToken(super.getAccessTokenValidator().generateAccessToken(payload));
                super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, model));
            }else{
                super.sendResponse(exchange, RESPONSE_UNVERYFIED_USER_LOGIN);
            }

        }
    }
}
