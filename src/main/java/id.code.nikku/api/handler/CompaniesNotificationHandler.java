package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.CompaniesoutletsFacade;
import id.code.nikku.facade.InboxesFacade;
import id.code.nikku.model.CompaniesoutletsModel;
import id.code.nikku.model.InboxesModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class CompaniesNotificationHandler extends RouteApiHandler<AccessTokenPayload> {
    private final CompaniesoutletsFacade companiesoutletsFacade = new CompaniesoutletsFacade();
    private final InboxesFacade inboxesFacade = new InboxesFacade();

    @HandlerGet(pathTemplate = "{companyId}/{inboxId}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, long companyId, long inboxId) throws Exception {
        final CompaniesoutletsModel data = this.companiesoutletsFacade.findById(companyId);
        InboxesModel inboxesModel = this.inboxesFacade.findById(inboxId);
        if (inboxesModel != null) {
            inboxesModel.setIsRead(1);
            this.inboxesFacade.update(inboxesModel);
        }
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }
}
