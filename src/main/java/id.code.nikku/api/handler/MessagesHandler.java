package id.code.nikku.api.handler;

import id.code.component.utility.StringUtility;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.MessagesFacade;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.filter.MessagesFilter;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.helper.DateHelper;
import id.code.nikku.helper.FcmHelper;
import id.code.nikku.model.MessagesModel;
import id.code.nikku.model.UsersModel;
import id.code.nikku.model.modify.MyMessages;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.*;
import id.code.server.route.RouteApiHandler;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class MessagesHandler extends RouteApiHandler<AccessTokenPayload> {
    private final MessagesFacade messagesFacade = new MessagesFacade();

    @HandlerGet
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Filter<MessagesFilter> filter) throws Exception {
        long loginId = exchange.getAccessTokenPayload().getId();
        final List<MessagesModel> items = this.messagesFacade.getAllWithJoin(filter,loginId);
        super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, items, filter));
    }

    @HandlerGet(pathTemplate = "{id}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final MessagesModel data = this.messagesFacade.findById(id);
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }

    @HandlerPost
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody MessagesModel newData) throws Exception {
        final ApiResponse responseCache = super.getRequestIdReferenceCache(exchange.getAccessTokenPayload(), newData.getRequestId());

        if (responseCache != null) {
            super.sendResponse(exchange, responseCache);
        } else {
            newData.setDate(DateHelper.getCurrentTime());
            this.messagesFacade.insert(newData);
            UsersFacade uf = new UsersFacade();
            UsersModel sender = uf.findById(newData.getSenderId());
            UsersModel receiver = uf.findById(newData.getReceiverId());
            MyMessages myMessages = new MyMessages();
            myMessages.setUserId(sender.getId());
            myMessages.setProfilePicture(sender.getProfilePicture());
            myMessages.setFullName(sender.getFullName());
            myMessages.setMessage(newData.getMessage());
            myMessages.setDate(newData.getDate());
            sendPushNotification(receiver.getFcmToken(),myMessages);
            super.sendResponse(exchange, exchange.getAccessTokenPayload(), newData.getRequestId(), new ApiResponse(HTTP_STATUS_OK, newData));
        }
    }

    @HandlerDelete(pathTemplate = "{id}")
    public void handleDELETE(ServerExchange<AccessTokenPayload> exchange, Long id) throws Exception {
        final boolean deleted = this.messagesFacade.delete(id);
        super.sendResponse(exchange, deleted ? RESPONSE_OK : RESPONSE_DATA_NOT_FOUND);
    }

    @HandlerPut(pathTemplate = "{id}")
    public void handlePUT(ServerExchange<AccessTokenPayload> exchange, @RequestBody MessagesModel newData, Long id) throws Exception {
        newData.setId(id);

        if (this.messagesFacade.update(newData)) {
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
        } else {
            super.sendResponse(exchange, RESPONSE_DATA_NOT_FOUND);
        }
    }

    private void sendPushNotification(String fcmToken,MyMessages myMessages){
        JSONObject json = new JSONObject();
        if(!StringUtility.isNullOrWhiteSpace(fcmToken)){
            try {
                json.put("to",fcmToken);
                JSONObject jsonData = new JSONObject();
                jsonData.put(ConstantHelper.NOTIFICATION_TYPE, ConstantHelper.NOTIF_CHAT);
                jsonData.put(ConstantHelper.NOTIFICATION_MESSAGE, "You have 1 message");
                jsonData.put("myMessages",myMessages.toJsonObject());
                json.put("data",jsonData);
                try {
                    FcmHelper.pushFCMNotification(json);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}