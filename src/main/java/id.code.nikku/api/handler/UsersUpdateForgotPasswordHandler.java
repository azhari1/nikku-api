package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.UsersModel;
import id.code.nikku.validation.UpdateForgotPasswordValidation;
import id.code.server.ApiHttpStatus;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

public class UsersUpdateForgotPasswordHandler extends RouteApiHandler<AccessTokenPayload> {
    private final UsersFacade usersFacade = new UsersFacade();

    @HandlerPost(authorizeAccess = false)
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody UpdateForgotPasswordValidation validation) throws Exception {
        final UsersModel model = this.usersFacade.findByEmail(validation.getEmail());
        if (model == null) {
            ApiResponse response = new ApiResponse(422,null);
            String message = "Your email is not found!!!";
            response.getMetadata().setUserMessage(message);
            response.getMetadata().setMessage(message);
            super.sendResponse(exchange, response);
        } else {
            if(validation.getPassword().equals(validation.getPasswordConfirmation())){
                this.usersFacade.changePassword(model, validation.getPassword());
                ApiResponse response = new ApiResponse(ApiHttpStatus.HTTP_STATUS_OK,null);
                String message = "Your password has been successfully changed";
                response.getMetadata().setUserMessage(message);
                response.getMetadata().setMessage(message);
                super.sendResponse(exchange, response);
            }else{
                ApiResponse response = new ApiResponse(422,null);
                String message = "Your password doest not match";
                response.getMetadata().setUserMessage(message);
                response.getMetadata().setMessage(message);
                super.sendResponse(exchange, new ApiResponse(422, "Your password doest not match"));
            }
        }
    }
}
