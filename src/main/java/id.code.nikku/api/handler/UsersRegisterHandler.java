package id.code.nikku.api.handler;

import id.code.component.utility.StringUtility;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.helper.ImageHelper;
import id.code.nikku.helper.sendgrid.SendMail;
import id.code.nikku.model.UsersModel;
import id.code.nikku.validation.UserRegisterValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.util.UUID;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;
import static id.code.server.ApiHttpStatus.STATUS_CONFLICT;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class UsersRegisterHandler extends RouteApiHandler<AccessTokenPayload> {
    private final static int STATUS_UPLOAD_FAILED = 1990;
    private static final ApiResponse RESPONSE_UPLOAD_FAILED = new ApiResponse(STATUS_UPLOAD_FAILED, "Failed upload your picture");
    private static final ApiResponse RESPONSE_DUPLICATE = new ApiResponse(STATUS_CONFLICT,  "Your email has been registered. Please choose another email address.");
    private UsersFacade usersFacade = new UsersFacade();

    @HandlerPost(authorizeAccess = false, bypassAllMiddleware = true)
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange,@RequestBody UserRegisterValidation dataRegistration) throws Exception {
        UsersModel newData = new UsersModel();
        newData.setUid(UUID.randomUUID().toString());
        newData.setGoogleId(dataRegistration.getGoogleId());
        newData.setFullName(dataRegistration.getFullName());
        newData.setEmail(dataRegistration.getEmail());
        newData.setPassword(dataRegistration.getPassword());
        newData.setSex(dataRegistration.getSex());
        newData.setDateOfBirth(dataRegistration.getDateOfBirth());
        newData.setCityId(dataRegistration.getCityId());
        newData.setSessionId(UUID.randomUUID().toString());
        if (!this.usersFacade.isEmailAvailable(newData.getEmail())) {
            super.sendResponse(exchange, RESPONSE_DUPLICATE);
        } else {
            String imageName = ImageHelper.saveUserFile(dataRegistration.getProfilePicture());
            if(!StringUtility.isNullOrWhiteSpace(imageName)){
                newData.setProfilePicture(imageName);
                //newData.setIsActive(1);//hilangkan apabila email ok
                this.usersFacade.insert(newData);
                SendMail.sendMailVerification(newData);
//                try{
//                    SendMailSSL.sendMailVerification(newData);
//                }catch (Exception e){
//                    e.printStackTrace();
//                }finally {
//                    super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
//                }
                super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, newData));
            }else{
                ImageHelper.deleteUserFile(imageName);
                super.sendResponse(exchange, RESPONSE_UPLOAD_FAILED);
            }
        }
    }

}