package id.code.nikku.api.handler;

import id.code.database.builder.QueryBuilderException;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.helper.sendgrid.SendMail;
import id.code.nikku.model.UsersModel;
import id.code.nikku.validation.ForgotPasswordValidation;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerPost;
import id.code.server.annotation.RequestBody;
import id.code.server.route.RouteApiHandler;

import java.io.IOException;
import java.sql.SQLException;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class UsersForgotPasswordHandler extends RouteApiHandler<AccessTokenPayload> {
    private final UsersFacade uf = new UsersFacade();

    @HandlerPost(authorizeAccess = false)
    public void handlePOST(ServerExchange<AccessTokenPayload> exchange, @RequestBody ForgotPasswordValidation forgotPasswordValidation) throws IOException, SQLException, QueryBuilderException {
        UsersModel validated = uf.isEmailValidated(forgotPasswordValidation.getEmail());
        if (validated!=null) {
            //SendMailSSL.sendMailForgotPassword(validated);
            SendMail.sendMailForgotPassword(validated);
            super.sendResponse(exchange, new ApiResponse(HTTP_STATUS_OK, forgotPasswordValidation));
        } else {
            super.sendResponse(exchange, new ApiResponse(422, "Your email address is not found. Please try again!!!."));
        }
    }
}
