package id.code.nikku.api.handler;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.ReviewsFacade;
import id.code.nikku.facade.ViewsFacade;
import id.code.nikku.helper.DateHelper;
import id.code.nikku.model.ReviewsModel;
import id.code.nikku.model.ViewsModel;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;
import id.code.server.annotation.HandlerGet;
import id.code.server.route.RouteApiHandler;

import static id.code.server.ApiHttpStatus.HTTP_STATUS_OK;

public class ReviewsShareHandler extends RouteApiHandler<AccessTokenPayload> {

    private final ReviewsFacade reviewsFacade = new ReviewsFacade();

    @HandlerGet(authorizeAccess = false,pathTemplate = "{id}/{userId}")
    public void handleGET(ServerExchange<AccessTokenPayload> exchange, long id, long userId) throws Exception {
        ReviewsModel data = this.reviewsFacade.findById(id);
        if(data!=null){
            ViewsFacade vf = new ViewsFacade();
            ViewsModel vm = vf.findById(id, userId);
            if(vm==null){
                long totalViews = vf.getTotalViews(data.getId());
                vm = new ViewsModel();
                vm.setReviewId(data.getId());
                vm.setUserId(exchange.getAccessTokenPayload().getId());
                vm.setDate(DateHelper.getCurrentTime());
                vf.insert(vm);
                data.setTotalViews(totalViews+1);
                this.reviewsFacade.update(data);
            }
        }
        super.sendResponse(exchange, data == null ? RESPONSE_DATA_NOT_FOUND : new ApiResponse(HTTP_STATUS_OK, data));
    }
}
