package id.code.nikku.api;

import id.code.nikku.api.handler.*;
import id.code.server.ApiHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Web Api Generator 17/12/2017.
 */
public class Route {
    public static Map<String, ApiHandler> createRoutes(Properties properties) throws Exception {
        final Map<String, ApiHandler> routes = new HashMap<>();

        routes.put("login", new UsersLoginHandler());
        routes.put("login_fb", new UsersLoginFacebookHandler());
        routes.put("register", new UsersRegisterHandler());
        routes.put("register/verify", new UsersRegisterVerifyHandler());
        routes.put("forgot_password", new UsersForgotPasswordHandler());
        routes.put("change_password", new UsersChangePasswordHandler());
        routes.put("get_item_notification", new CountItemNotificationHandler());
        routes.put("get_item_notification", new CountItemNotificationHandler());

        routes.put("users", new UsersHandler());
        routes.put("users/search", new UsersSearchHandler());
        routes.put("users/create", new CreateAccountHandler());
        routes.put("users/{id}", new UsersHandler());
        routes.put("users/firebase/{id}", new FirebaseHandler());
        routes.put("users/{id}/picture/{file}", new UserFileHandler());
        routes.put("users/{id}/ktp/{file}", new UsersKtpFileHandler());
        routes.put("users/nik", new UsersUpdateNikHandler());

//		routes.put("province", new ProvinceHandler());
//		routes.put("province/{id}", new ProvinceHandler());

        routes.put("companyoutletpics", new CompanyoutletpicsHandler());
        routes.put("companyoutletpics/{id}", new CompanyoutletpicsHandler());
        routes.put("homecompanydetails", new HomeCompanyDetailsHandler());

        routes.put("skills", new SkillsHandler());
        routes.put("skills/{id}", new SkillsHandler());

//		routes.put("cmsusers", new CmsusersHandler());
//		routes.put("cmsusers/{id}", new CmsusersHandler());

        routes.put("privacysettings", new PrivacysettingsHandler());
        routes.put("privacysettings/{id}", new PrivacysettingsHandler());

        routes.put("likes", new LikesHandler());
        routes.put("likes/{id}", new LikesHandler());

        routes.put("roles", new RolesHandler());
        routes.put("roles/{id}", new RolesHandler());

        routes.put("views", new ViewsHandler());
        routes.put("views/{id}", new ViewsHandler());

        routes.put("typeofbusiness", new TypeofbusinessHandler());
        routes.put("typeofbusiness/{id}", new TypeofbusinessHandler());

        routes.put("userworkatcompanies", new UserworkatcompaniesHandler());
        routes.put("userworkatcompanies/{id}", new UserworkatcompaniesHandler());
        routes.put("usersoncompany", new UsersOnCompanyHandler());

        routes.put("inboxes", new InboxesHandler());
        routes.put("inboxes/{id}", new InboxesHandler());

        routes.put("city", new CityHandler());
//		routes.put("city/{id}", new CityHandler());

        routes.put("companiesoutlets", new CompaniesoutletsHandler());
        routes.put("companiesoutlets/my_companies", new MyCompaniesHandler());
        routes.put("companiesoutlets/notification", new CompaniesNotificationHandler());
        routes.put("companiesoutlets/{id}", new CompaniesoutletsHandler());
        routes.put("companiesoutlets/{id}/logo/{file}", new CompanyFileHandler());

        routes.put("menus", new MenusHandler());
        routes.put("menus/{id}", new MenusHandler());

        routes.put("reviews", new ReviewsHandler());
        routes.put("reviews/share", new ReviewsShareHandler());
        routes.put("reviews/company_person", new CompanyPersonReviewHandler());
        routes.put("reviews_notification", new ReviewsNotificationHandler());
        routes.put("reviews_notification/company", new ReviewsNotificationCompanyHandler());
        routes.put("reviews/{id}", new ReviewsHandler());
        routes.put("reviews/{id}/photo/{file}", new ReviewFileHandler());

        routes.put("messages", new MessagesHandler());
        routes.put("messages/{id}", new MessagesHandler());
        routes.put("messages/deleted", new MessageDeleteHandler());

        //authentication
        routes.put("update_password", new UsersUpdateForgotPasswordHandler());
        //home page
        routes.put("home", new HomeHandler());
        routes.put("top_rated_person", new TopRatedPersonHandler());
        routes.put("featured_company", new FeaturedCompanyHandler());
        routes.put("detail_company", new DetailCompanyHandler());
        //my review page
        routes.put("my_review", new MyReviewHandler());
        //my_company
        routes.put("my_company", new MyCompanyHandler());
        //my_profile
        routes.put("my_profile", new MyProfileHandler());
        routes.put("popular_reviews", new PopularReviewsHandler());
        routes.put("hide_or_show_companies", new HideOrShowCompanyHandler());

        //message_to_company
        routes.put("message_by_company", new MessagesByCompanyHandler());

        //my message
        routes.put("my_message", new MyMessageHandler());
        routes.put("my_message/details", new MyMessagesDetailsHandler());

        //faq
        routes.put("faq", new FaqHandler());

        //requestReview
        routes.put("request_review", new RequestReviewHandler());

        //logout
        routes.put("logout", new LogoutHandler());
        return routes;
    }
}