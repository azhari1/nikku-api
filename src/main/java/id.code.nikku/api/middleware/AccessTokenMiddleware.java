package id.code.nikku.api.middleware;

import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.facade.UsersFacade;
import id.code.nikku.model.UsersModel;
import id.code.server.AccessTokenValidator;
import id.code.server.ApiHandler;
import id.code.server.ApiResponse;
import id.code.server.ServerExchange;

import java.util.Map;
import java.util.Properties;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class AccessTokenMiddleware extends AccessTokenValidator<AccessTokenPayload> {
    private final UsersFacade facade = new UsersFacade();

    public AccessTokenMiddleware(Properties properties) {
        super(properties);
    }

    @Override
    public AccessTokenPayload getRequestedClaim(Map payloadFromClient) {
        return new AccessTokenPayload(payloadFromClient);
    }

    @Override
    public AccessTokenPayload getValidatedClaim(AccessTokenPayload payload) throws Exception {
        final UsersModel payloadFromDatabase = this.facade.findById(payload.getId());
        return payloadFromDatabase == null ? null : new AccessTokenPayload(payloadFromDatabase);
    }

    @Override
    public boolean isMatch(AccessTokenPayload fromClient, AccessTokenPayload fromServer, ApiHandler<AccessTokenPayload> handler, ServerExchange<AccessTokenPayload> exchange, boolean sendResponse) throws Exception {
        UsersModel payloadFromDatabase = this.facade.findById(fromClient.getId());
        //by pas from server
        fromServer = new AccessTokenPayload(payloadFromDatabase);
        //end by pas
        if(fromClient.getSessionId()!=null && !fromClient.getSessionId().equals(fromServer.getSessionId())){
            handler.sendResponse(exchange, new ApiResponse(6666, "You has logged in another device"));
            return false;
        }
        return super.isMatch(fromClient, fromServer, handler, exchange, sendResponse);
    }
}