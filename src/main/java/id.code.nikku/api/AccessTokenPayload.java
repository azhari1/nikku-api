package id.code.nikku.api;

import id.code.component.utility.StringUtility;
import id.code.server.ApiClaim;
import id.code.nikku.model.UsersModel;

import java.util.Map;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class AccessTokenPayload extends ApiClaim {
    private String sessionId;
    private long id;

    public AccessTokenPayload(UsersModel user) {
        super(String.valueOf(user.getId()), user.getFullName());
        this.id = user.getId();
        this.sessionId = user.getSessionId();
    }

    public AccessTokenPayload(Map payload) {
        super(payload);
        this.id = StringUtility.getLong(super.getClaimId());
        this.sessionId = StringUtility.getString(payload.get(UsersModel._SESSION_ID));
    }

    @Override
    public boolean isEmpty() { return this.id == 0; }
    public long getId() { return this.id; }
    public String getSessionId() {
        return sessionId;
    }

    @Override
    public Map toMap() {
        Map map = super.toMap();
        map.put(UsersModel._ID, this.id);
        map.put(UsersModel._SESSION_ID, this.sessionId);
        return map;
    }
}