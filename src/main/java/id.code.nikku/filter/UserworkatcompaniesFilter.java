package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.UserworkatcompaniesModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class UserworkatcompaniesFilter {
	@FilterColumn(UserworkatcompaniesModel._ID)
	private Long id;

	@FilterColumn(UserworkatcompaniesModel._USER_ID)
	private Long userId;

	@FilterColumn(UserworkatcompaniesModel._COMPANY_ID)
	private Long companyId;

	@FilterColumn(UserworkatcompaniesModel._JOB_POSITION)
	private String jobPosition;

	@FilterColumn(UserworkatcompaniesModel._START_DATE)
	private Long startDate;

	@FilterColumn(UserworkatcompaniesModel._END_DATE)
	private Long endDate;

	@FilterColumn(UserworkatcompaniesModel._STATUS)
	private Integer status;

	@FilterColumn(UserworkatcompaniesModel._IS_HIDE)
	private Integer isHide;

	@FilterColumn(UserworkatcompaniesModel._HIDE_FROM_RATE)
	private Integer hideFromRate;


	public Long getId() { return this.id; }
	public Long getUserId() { return this.userId; }
	public Long getCompanyId() { return this.companyId; }
	public String getJobPosition() { return this.jobPosition; }
	public Long getStartDate() { return this.startDate; }
	public Long getEndDate() { return this.endDate; }
	public Integer getStatus() { return this.status; }
	public Integer getIsHide() { return this.isHide; }
	public Integer getHideFromRate() { return this.hideFromRate; }

	public void setId(Long id) { this.id = id; }
	public void setUserId(Long userId) { this.userId = userId; }
	public void setCompanyId(Long companyId) { this.companyId = companyId; }
	public void setJobPosition(String jobPosition) { this.jobPosition = jobPosition; }
	public void setStartDate(Long startDate) { this.startDate = startDate; }
	public void setEndDate(Long endDate) { this.endDate = endDate; }
	public void setStatus(Integer status) { this.status = status; }
	public void setIsHide(Integer isHide) { this.isHide = isHide; }
	public void setHideFromRate(Integer hideFromRate) { this.hideFromRate = hideFromRate; }

}