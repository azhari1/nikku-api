package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.SkillsModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class SkillsFilter {
	@FilterColumn(SkillsModel._ID)
	private Long id;

	@FilterColumn(SkillsModel._USER_ID)
	private Long userId;

	@FilterColumn(SkillsModel._SKILL_NAME)
	private String skillName;


	public Long getId() { return this.id; }
	public Long getUserId() { return this.userId; }
	public String getSkillName() { return this.skillName; }

	public void setId(Long id) { this.id = id; }
	public void setUserId(Long userId) { this.userId = userId; }
	public void setSkillName(String skillName) { this.skillName = skillName; }

}