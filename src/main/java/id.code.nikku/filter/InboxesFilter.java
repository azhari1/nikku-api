package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.InboxesModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class InboxesFilter {
	@FilterColumn(InboxesModel._ID)
	private Long id;

	@FilterColumn(InboxesModel._USER_ID)
	private Long userId;

	@FilterColumn(InboxesModel._INBOX_TYPE)
	private Integer inboxType;

	@FilterColumn(InboxesModel._REFERENCE_ID)
	private Long referenceId;

	@FilterColumn(InboxesModel._MESSAGE)
	private String message;

	@FilterColumn(InboxesModel._DATE)
	private Long date;


	public Long getId() { return this.id; }
	public Long getUserId() { return this.userId; }
	public Integer getInboxType() { return this.inboxType; }
	public Long getReferenceId() { return this.referenceId; }
	public String getMessage() { return this.message; }
	public Long getDate() { return this.date; }

	public void setId(Long id) { this.id = id; }
	public void setUserId(Long userId) { this.userId = userId; }
	public void setInboxType(Integer inboxType) { this.inboxType = inboxType; }
	public void setReferenceId(Long referenceId) { this.referenceId = referenceId; }
	public void setMessage(String message) { this.message = message; }
	public void setDate(Long date) { this.date = date; }

}