package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.CompaniesoutletsModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CompaniesoutletsFilter {
	@FilterColumn(CompaniesoutletsModel._ID)
	private Long id;

	@FilterColumn(CompaniesoutletsModel._COMPANY_NAME)
	private String companyName;

	@FilterColumn(CompaniesoutletsModel._OUTLET_NAME)
	private String outletName;

	@FilterColumn(CompaniesoutletsModel._SIUP_TDPNUMBER)
	private String siupTdpnumber;

	@FilterColumn(CompaniesoutletsModel._TYPE_OF_BUSINESS_ID)
	private Long typeOfBusinessId;

	@FilterColumn(CompaniesoutletsModel._ADDRESS)
	private String address;

	@FilterColumn(CompaniesoutletsModel._PHONE)
	private String phone;

	@FilterColumn(CompaniesoutletsModel._EMAIL)
	private String email;

	@FilterColumn(CompaniesoutletsModel._WEBSITE)
	private String website;

	@FilterColumn(CompaniesoutletsModel._OWNER)
	private Long owner;

	@FilterColumn(CompaniesoutletsModel._LOGO)
	private String logo;

	@FilterColumn(CompaniesoutletsModel._DATE_CREATED)
	private Long dateCreated;

	@FilterColumn(CompaniesoutletsModel._CREATED_BY)
	private Long createdBy;

	@FilterColumn(CompaniesoutletsModel._IS_FEATURED)
	private Integer isFeatured;

	@FilterColumn(CompaniesoutletsModel._ORDER_LIST)
	private Integer orderList;

	@FilterColumn(CompaniesoutletsModel._IS_DISABLED)
	private Integer isDisabled;


	public Long getId() { return this.id; }
	public String getCompanyName() { return this.companyName; }
	public String getOutletName() { return this.outletName; }
	public String getSiupTdpnumber() { return this.siupTdpnumber; }
	public Long getTypeOfBusinessId() { return this.typeOfBusinessId; }
	public String getAddress() { return this.address; }
	public String getPhone() { return this.phone; }
	public String getEmail() { return this.email; }
	public String getWebsite() { return this.website; }
	public Long getOwner() { return this.owner; }
	public String getLogo() { return this.logo; }
	public Long getDateCreated() { return this.dateCreated; }
	public Long getCreatedBy() { return this.createdBy; }
	public Integer getIsFeatured() { return this.isFeatured; }
	public Integer getOrderList() { return this.orderList; }
	public Integer getIsDisabled() { return this.isDisabled; }

	public void setId(Long id) { this.id = id; }
	public void setCompanyName(String companyName) { this.companyName = companyName; }
	public void setOutletName(String outletName) { this.outletName = outletName; }
	public void setSiupTdpnumber(String siupTdpnumber) { this.siupTdpnumber = siupTdpnumber; }
	public void setTypeOfBusinessId(Long typeOfBusinessId) { this.typeOfBusinessId = typeOfBusinessId; }
	public void setAddress(String address) { this.address = address; }
	public void setPhone(String phone) { this.phone = phone; }
	public void setEmail(String email) { this.email = email; }
	public void setWebsite(String website) { this.website = website; }
	public void setOwner(Long owner) { this.owner = owner; }
	public void setLogo(String logo) { this.logo = logo; }
	public void setDateCreated(Long dateCreated) { this.dateCreated = dateCreated; }
	public void setCreatedBy(Long createdBy) { this.createdBy = createdBy; }
	public void setIsFeatured(Integer isFeatured) { this.isFeatured = isFeatured; }
	public void setOrderList(Integer orderList) { this.orderList = orderList; }
	public void setIsDisabled(Integer isDisabled) { this.isDisabled = isDisabled; }
}