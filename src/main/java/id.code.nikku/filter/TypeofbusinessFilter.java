package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.TypeofbusinessModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class TypeofbusinessFilter {
	@FilterColumn(TypeofbusinessModel._ID)
	private Long id;

	@FilterColumn(TypeofbusinessModel._TYPE_OF_BUSINESS_NAME)
	private String typeOfBusinessName;


	public Long getId() { return this.id; }
	public String getTypeOfBusinessName() { return this.typeOfBusinessName; }

	public void setId(Long id) { this.id = id; }
	public void setTypeOfBusinessName(String typeOfBusinessName) { this.typeOfBusinessName = typeOfBusinessName; }

}