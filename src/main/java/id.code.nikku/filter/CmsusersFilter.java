package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.CmsusersModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CmsusersFilter {
	@FilterColumn(CmsusersModel._ID)
	private Long id;

	@FilterColumn(CmsusersModel._ROLE_ID)
	private Long roleId;

	@FilterColumn(CmsusersModel._USER_NAME)
	private String userName;

	@FilterColumn(CmsusersModel._PASSWORD)
	private String password;

	@FilterColumn(CmsusersModel._PASSWORD_SALT)
	private String passwordSalt;

	@FilterColumn(CmsusersModel._NAME)
	private String name;

	@FilterColumn(CmsusersModel._IS_ACTIVE)
	private Integer isActive;

	@FilterColumn(CmsusersModel._CREATED_BY)
	private String createdBy;

	@FilterColumn(CmsusersModel._CREATED_DATE)
	private Long createdDate;

	@FilterColumn(CmsusersModel._MODIFIED_BY)
	private String modifiedBy;

	@FilterColumn(CmsusersModel._MODIFIED_DATE)
	private Long modifiedDate;

	@FilterColumn(CmsusersModel._LAS_LOGIN)
	private Long lasLogin;


	public Long getId() { return this.id; }
	public Long getRoleId() { return this.roleId; }
	public String getUserName() { return this.userName; }
	public String getPassword() { return this.password; }
	public String getPasswordSalt() { return this.passwordSalt; }
	public String getName() { return this.name; }
	public Integer getIsActive() { return this.isActive; }
	public String getCreatedBy() { return this.createdBy; }
	public Long getCreatedDate() { return this.createdDate; }
	public String getModifiedBy() { return this.modifiedBy; }
	public Long getModifiedDate() { return this.modifiedDate; }
	public Long getLasLogin() { return this.lasLogin; }

	public void setId(Long id) { this.id = id; }
	public void setRoleId(Long roleId) { this.roleId = roleId; }
	public void setUserName(String userName) { this.userName = userName; }
	public void setPassword(String password) { this.password = password; }
	public void setPasswordSalt(String passwordSalt) { this.passwordSalt = passwordSalt; }
	public void setName(String name) { this.name = name; }
	public void setIsActive(Integer isActive) { this.isActive = isActive; }
	public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
	public void setCreatedDate(Long createdDate) { this.createdDate = createdDate; }
	public void setModifiedBy(String modifiedBy) { this.modifiedBy = modifiedBy; }
	public void setModifiedDate(Long modifiedDate) { this.modifiedDate = modifiedDate; }
	public void setLasLogin(Long lasLogin) { this.lasLogin = lasLogin; }

}