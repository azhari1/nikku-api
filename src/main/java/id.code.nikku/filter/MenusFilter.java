package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.MenusModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class MenusFilter {
	@FilterColumn(MenusModel._ID)
	private Long id;

	@FilterColumn(MenusModel._MENU_PARENT)
	private Long menuParent;

	@FilterColumn(MenusModel._MENU_NAME)
	private String menuName;

	@FilterColumn(MenusModel._URL)
	private String url;

	@FilterColumn(MenusModel._POSITION)
	private Integer position;

	@FilterColumn(MenusModel._IS_ACTIVE)
	private Integer isActive;

	@FilterColumn(MenusModel._ICON_PATH)
	private String iconPath;

	@FilterColumn(MenusModel._CREATED_BY)
	private String createdBy;

	@FilterColumn(MenusModel._CREATED_DATE)
	private Long createdDate;

	@FilterColumn(MenusModel._MODIFIED_BY)
	private String modifiedBy;

	@FilterColumn(MenusModel._MODIFIED_DATE)
	private Long modifiedDate;


	public Long getId() { return this.id; }
	public Long getMenuParent() { return this.menuParent; }
	public String getMenuName() { return this.menuName; }
	public String getUrl() { return this.url; }
	public Integer getPosition() { return this.position; }
	public Integer getIsActive() { return this.isActive; }
	public String getIconPath() { return this.iconPath; }
	public String getCreatedBy() { return this.createdBy; }
	public Long getCreatedDate() { return this.createdDate; }
	public String getModifiedBy() { return this.modifiedBy; }
	public Long getModifiedDate() { return this.modifiedDate; }

	public void setId(Long id) { this.id = id; }
	public void setMenuParent(Long menuParent) { this.menuParent = menuParent; }
	public void setMenuName(String menuName) { this.menuName = menuName; }
	public void setUrl(String url) { this.url = url; }
	public void setPosition(Integer position) { this.position = position; }
	public void setIsActive(Integer isActive) { this.isActive = isActive; }
	public void setIconPath(String iconPath) { this.iconPath = iconPath; }
	public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
	public void setCreatedDate(Long createdDate) { this.createdDate = createdDate; }
	public void setModifiedBy(String modifiedBy) { this.modifiedBy = modifiedBy; }
	public void setModifiedDate(Long modifiedDate) { this.modifiedDate = modifiedDate; }

}