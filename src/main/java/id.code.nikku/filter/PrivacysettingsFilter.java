package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.PrivacysettingsModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class PrivacysettingsFilter {
	@FilterColumn(PrivacysettingsModel._USER_ID)
	private Long userId;

	@FilterColumn(PrivacysettingsModel._EMAIL_VISIBLE)
	private Integer emailVisible;

	@FilterColumn(PrivacysettingsModel._PHONE_VISIBLE)
	private Integer phoneVisible;

	@FilterColumn(PrivacysettingsModel._BIRTH_DATE_VISIBLE)
	private Integer birthDateVisible;

	@FilterColumn(PrivacysettingsModel._COMPANY_VISIBLE)
	private Integer companyVisible;


	public Long getUserId() { return this.userId; }
	public Integer getEmailVisible() { return this.emailVisible; }
	public Integer getPhoneVisible() { return this.phoneVisible; }
	public Integer getBirthDateVisible() { return this.birthDateVisible; }
	public Integer getCompanyVisible() { return this.companyVisible; }

	public void setUserId(Long userId) { this.userId = userId; }
	public void setEmailVisible(Integer emailVisible) { this.emailVisible = emailVisible; }
	public void setPhoneVisible(Integer phoneVisible) { this.phoneVisible = phoneVisible; }
	public void setBirthDateVisible(Integer birthDateVisible) { this.birthDateVisible = birthDateVisible; }
	public void setCompanyVisible(Integer companyVisible) { this.companyVisible = companyVisible; }

}