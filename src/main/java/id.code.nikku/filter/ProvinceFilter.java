package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.ProvinceModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class ProvinceFilter {
	@FilterColumn(ProvinceModel._ID)
	private Long id;

	@FilterColumn(ProvinceModel._PROVINCE_NAME)
	private String provinceName;


	public Long getId() { return this.id; }
	public String getProvinceName() { return this.provinceName; }

	public void setId(Long id) { this.id = id; }
	public void setProvinceName(String provinceName) { this.provinceName = provinceName; }

}