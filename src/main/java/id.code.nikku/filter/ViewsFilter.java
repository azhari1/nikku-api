package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.ViewsModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class ViewsFilter {
	@FilterColumn(ViewsModel._ID)
	private Long id;

	@FilterColumn(ViewsModel._REVIEW_ID)
	private Long reviewId;

	@FilterColumn(ViewsModel._USER_ID)
	private Long userId;

	@FilterColumn(ViewsModel._DATE)
	private Long date;


	public Long getId() { return this.id; }
	public Long getReviewId() { return this.reviewId; }
	public Long getUserId() { return this.userId; }
	public Long getDate() { return this.date; }

	public void setId(Long id) { this.id = id; }
	public void setReviewId(Long reviewId) { this.reviewId = reviewId; }
	public void setUserId(Long userId) { this.userId = userId; }
	public void setDate(Long date) { this.date = date; }

}