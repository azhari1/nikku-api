package id.code.nikku.filter;

import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.SkillsModel;
import id.code.nikku.model.UsersModel;

public class UsersSearchFilter {
    @FilterColumn(UsersModel._ID)
    private Long id;

    @FilterColumn(value = UsersModel._NIK,comparator = WhereComparator.EQUALS)
    private String nik;

    @FilterColumn(value = UsersModel._FULL_NAME,comparator = WhereComparator.CONTAINS)
    private String fullName;

    @FilterColumn(UsersModel._EMAIL)
    private String email;

    @FilterColumn(UsersModel._SEX)
    private Integer sex;

    @FilterColumn(UsersModel._DATE_OF_BIRTH)
    private Long dateOfBirth;

    @FilterColumn(UsersModel._PHONE)
    private String phone;

    @FilterColumn(UsersModel._CITY_ID)
    private Long cityId;

    @FilterColumn(UsersModel._PROFILE_PICTURE)
    private String profilePicture;

    @FilterColumn(UsersModel._KTP_PHOTO)
    private String ktpPhoto;

    @FilterColumn(UsersModel._JOB_TITLE)
    private String jobTitle;

    @FilterColumn(UsersModel._BIO)
    private String bio;

    @FilterColumn(UsersModel._AVG_RATING)
    private Double avgRating;

    @FilterColumn(UsersModel._TOTAL_REVIEWS)
    private Integer totalReviews;

    @FilterColumn(UsersModel._PASSWORD)
    private String password;

    @FilterColumn(UsersModel._GOOGLE_ID)
    private String googleId;

    @FilterColumn(UsersModel._FCM_TOKEN)
    private String fcmToken;

    @FilterColumn(UsersModel._IS_ACTIVE)
    private Integer isActive;

    @FilterColumn(UsersModel._DATE_CREATED)
    private Long dateCreated;

    @FilterColumn(UsersModel._IS_VERIFIED)
    private Integer isVerified;

    @FilterColumn(value = SkillsModel._SKILL_NAME,comparator = WhereComparator.CONTAINS, includeInQuery = false)
    private String skillName;

    public Long getId() { return this.id; }
    public String getNik() { return this.nik; }
    public String getFullName() { return this.fullName; }
    public String getEmail() { return this.email; }
    public Integer getSex() { return this.sex; }
    public Long getDateOfBirth() { return this.dateOfBirth; }
    public String getPhone() { return this.phone; }
    public Long getCityId() { return this.cityId; }
    public String getProfilePicture() { return this.profilePicture; }
    public String getKtpPhoto() { return this.ktpPhoto; }
    public String getJobTitle() { return this.jobTitle; }
    public String getBio() { return this.bio; }
    public Double getAvgRating() { return this.avgRating; }
    public Integer getTotalReviews() { return this.totalReviews; }
    public String getPassword() { return this.password; }
    public String getGoogleId() { return this.googleId; }
    public String getFcmToken() { return this.fcmToken; }
    public Integer getIsActive() { return this.isActive; }
    public Long getDateCreated() { return this.dateCreated; }
    public Integer getIsVerified() { return this.isVerified; }

    public void setId(Long id) { this.id = id; }
    public void setNik(String nik) { this.nik = nik; }
    public void setFullName(String fullName) { this.fullName = fullName; }
    public void setEmail(String email) { this.email = email; }
    public void setSex(Integer sex) { this.sex = sex; }
    public void setDateOfBirth(Long dateOfBirth) { this.dateOfBirth = dateOfBirth; }
    public void setPhone(String phone) { this.phone = phone; }
    public void setCityId(Long cityId) { this.cityId = cityId; }
    public void setProfilePicture(String profilePicture) { this.profilePicture = profilePicture; }
    public void setKtpPhoto(String ktpPhoto) { this.ktpPhoto = ktpPhoto; }
    public void setJobTitle(String jobTitle) { this.jobTitle = jobTitle; }
    public void setBio(String bio) { this.bio = bio; }
    public void setAvgRating(Double avgRating) { this.avgRating = avgRating; }
    public void setTotalReviews(Integer totalReviews) { this.totalReviews = totalReviews; }
    public void setPassword(String password) { this.password = password; }
    public void setGoogleId(String googleId) { this.googleId = googleId; }
    public void setFcmToken(String fcmToken) { this.fcmToken = fcmToken; }
    public void setIsActive(Integer isActive) { this.isActive = isActive; }
    public void setDateCreated(Long dateCreated) { this.dateCreated = dateCreated; }
    public void setIsVerified(Integer isVerified) { this.isVerified = isVerified; }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }
}
