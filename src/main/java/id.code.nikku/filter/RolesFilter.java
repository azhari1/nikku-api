package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.RolesModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class RolesFilter {
	@FilterColumn(RolesModel._ID)
	private Long id;

	@FilterColumn(RolesModel._ROLE_NAME)
	private String roleName;

	@FilterColumn(RolesModel._IS_SUPER_ADMIN)
	private Integer isSuperAdmin;

	@FilterColumn(RolesModel._CREATED_DATE)
	private Long createdDate;

	@FilterColumn(RolesModel._CREATED_BY)
	private String createdBy;

	@FilterColumn(RolesModel._MODIFIED_BY)
	private String modifiedBy;

	@FilterColumn(RolesModel._MODIFIED_DATE)
	private Long modifiedDate;


	public Long getId() { return this.id; }
	public String getRoleName() { return this.roleName; }
	public Integer getIsSuperAdmin() { return this.isSuperAdmin; }
	public Long getCreatedDate() { return this.createdDate; }
	public String getCreatedBy() { return this.createdBy; }
	public String getModifiedBy() { return this.modifiedBy; }
	public Long getModifiedDate() { return this.modifiedDate; }

	public void setId(Long id) { this.id = id; }
	public void setRoleName(String roleName) { this.roleName = roleName; }
	public void setIsSuperAdmin(Integer isSuperAdmin) { this.isSuperAdmin = isSuperAdmin; }
	public void setCreatedDate(Long createdDate) { this.createdDate = createdDate; }
	public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
	public void setModifiedBy(String modifiedBy) { this.modifiedBy = modifiedBy; }
	public void setModifiedDate(Long modifiedDate) { this.modifiedDate = modifiedDate; }

}