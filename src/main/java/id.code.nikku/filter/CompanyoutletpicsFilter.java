package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.CompanyoutletpicsModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CompanyoutletpicsFilter {
	@FilterColumn(CompanyoutletpicsModel._ID)
	private Long id;

	@FilterColumn(CompanyoutletpicsModel._COMPANY_ID)
	private Long companyId;

	@FilterColumn(CompanyoutletpicsModel._USER_ID)
	private Long userId;

	@FilterColumn(CompanyoutletpicsModel._CREATED_DATE)
	private Long createdDate;

	@FilterColumn(CompanyoutletpicsModel._STATUS)
	private Integer status;


	public Long getId() { return this.id; }
	public Long getCompanyId() { return this.companyId; }
	public Long getUserId() { return this.userId; }
	public Long getCreatedDate() { return this.createdDate; }
	public Integer getStatus() { return this.status; }

	public void setId(Long id) { this.id = id; }
	public void setCompanyId(Long companyId) { this.companyId = companyId; }
	public void setUserId(Long userId) { this.userId = userId; }
	public void setCreatedDate(Long createdDate) { this.createdDate = createdDate; }
	public void setStatus(Integer status) { this.status = status; }

}