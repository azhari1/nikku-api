package id.code.nikku.filter;

import id.code.database.builder.WhereComparator;
import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.CityModel;
import id.code.nikku.model.ProvinceModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CityFilter {
	@FilterColumn(CityModel._ID)
	private Long id;

	@FilterColumn(CityModel._PROVINCE_ID)
	private Long provinceId;

	@FilterColumn(value = CityModel._CITY_NAME, comparator = WhereComparator.CONTAINS, columnName = CityModel._CITY_NAME, orColumnName = ProvinceModel._PROVINCE_NAME)
	private String cityName;


	public Long getId() { return this.id; }
	public Long getProvinceId() { return this.provinceId; }
	public String getCityName() { return this.cityName; }

	public void setId(Long id) { this.id = id; }
	public void setProvinceId(Long provinceId) { this.provinceId = provinceId; }
	public void setCityName(String cityName) { this.cityName = cityName; }
}