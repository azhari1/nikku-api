package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.ReviewsModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class ReviewsFilter {
	@FilterColumn(ReviewsModel._ID)
	private Long id;

	@FilterColumn(ReviewsModel._USER_ID)
	private Long userId;

	@FilterColumn(ReviewsModel._REVIEWER_ID)
	private Long reviewerId;

	@FilterColumn(ReviewsModel._REVIEW_TYPE)
	private Integer reviewType;

	@FilterColumn(ReviewsModel._AS_COMPANY_ID)
	private Long asCompanyId;

	@FilterColumn(ReviewsModel._DATE)
	private Long date;

	@FilterColumn(ReviewsModel._TOTAL_LIKES)
	private Long totalLikes;

	@FilterColumn(ReviewsModel._TOTAL_VIEWS)
	private Long totalViews;

	@FilterColumn(ReviewsModel._RATING_SCORE)
	private Double ratingScore;

	@FilterColumn(ReviewsModel._QUESTIONNAIRE1)
	private Integer questionnaire1;

	@FilterColumn(ReviewsModel._QUESTIONNAIRE2)
	private Integer questionnaire2;

	@FilterColumn(ReviewsModel._QUESTIONNAIRE3)
	private Integer questionnaire3;

	@FilterColumn(ReviewsModel._QUESTIONNAIRE4)
	private Integer questionnaire4;

	@FilterColumn(ReviewsModel._QUESTIONNAIRE5)
	private Integer questionnaire5;

	@FilterColumn(ReviewsModel._COMMENT)
	private String comment;

	@FilterColumn(ReviewsModel._PHOTO)
	private String photo;

	@FilterColumn(ReviewsModel._URL)
	private String url;

	@FilterColumn(ReviewsModel._IS_HIDDEN)
	private Integer isHidden;


	public Long getId() { return this.id; }
	public Long getUserId() { return this.userId; }
	public Long getReviewerId() { return this.reviewerId; }
	public Integer getReviewType() { return this.reviewType; }
	public Long getAsCompanyId() { return this.asCompanyId; }
	public Long getDate() { return this.date; }
	public Long getTotalLikes() { return this.totalLikes; }
	public Long getTotalViews() { return this.totalViews; }
	public Double getRatingScore() { return this.ratingScore; }
	public Integer getQuestionnaire1() { return this.questionnaire1; }
	public Integer getQuestionnaire2() { return this.questionnaire2; }
	public Integer getQuestionnaire3() { return this.questionnaire3; }
	public Integer getQuestionnaire4() { return this.questionnaire4; }
	public Integer getQuestionnaire5() { return this.questionnaire5; }
	public String getComment() { return this.comment; }
	public String getPhoto() { return this.photo; }
	public String getUrl() { return this.url; }
	public Integer getIsHidden() { return this.isHidden; }

	public void setId(Long id) { this.id = id; }
	public void setUserId(Long userId) { this.userId = userId; }
	public void setReviewerId(Long reviewerId) { this.reviewerId = reviewerId; }
	public void setReviewType(Integer reviewType) { this.reviewType = reviewType; }
	public void setAsCompanyId(Long asCompanyId) { this.asCompanyId = asCompanyId; }
	public void setDate(Long date) { this.date = date; }
	public void setTotalLikes(Long totalLikes) { this.totalLikes = totalLikes; }
	public void setTotalViews(Long totalViews) { this.totalViews = totalViews; }
	public void setRatingScore(Double ratingScore) { this.ratingScore = ratingScore; }
	public void setQuestionnaire1(Integer questionnaire1) { this.questionnaire1 = questionnaire1; }
	public void setQuestionnaire2(Integer questionnaire2) { this.questionnaire2 = questionnaire2; }
	public void setQuestionnaire3(Integer questionnaire3) { this.questionnaire3 = questionnaire3; }
	public void setQuestionnaire4(Integer questionnaire4) { this.questionnaire4 = questionnaire4; }
	public void setQuestionnaire5(Integer questionnaire5) { this.questionnaire5 = questionnaire5; }
	public void setComment(String comment) { this.comment = comment; }
	public void setPhoto(String photo) { this.photo = photo; }
	public void setUrl(String url) { this.url = url; }
	public void setIsHidden(Integer isHidden) { this.isHidden = isHidden; }

}