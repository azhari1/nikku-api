package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.MessagesModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class MessagesFilter {
	@FilterColumn(MessagesModel._ID)
	private Long id;

	@FilterColumn(MessagesModel._SENDER_ID)
	private Long senderId;

	@FilterColumn(MessagesModel._RECEIVER_ID)
	private Long receiverId;

	@FilterColumn(MessagesModel._DATE)
	private Long date;

	@FilterColumn(MessagesModel._MESSAGE)
	private String message;


	public Long getId() { return this.id; }
	public Long getSenderId() { return this.senderId; }
	public Long getReceiverId() { return this.receiverId; }
	public Long getDate() { return this.date; }
	public String getMessage() { return this.message; }

	public void setId(Long id) { this.id = id; }
	public void setSenderId(Long senderId) { this.senderId = senderId; }
	public void setReceiverId(Long receiverId) { this.receiverId = receiverId; }
	public void setDate(Long date) { this.date = date; }
	public void setMessage(String message) { this.message = message; }

}