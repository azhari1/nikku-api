package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.RequestReviewModel;

public class RequestReviewFilter {

    @FilterColumn(RequestReviewModel._APPROVER)
    private Long approver;

    @FilterColumn(RequestReviewModel._REQUESTER)
    private Long requester;

    public Long getApprover() {
        return approver;
    }

    public void setApprover(Long approver) {
        this.approver = approver;
    }

    public Long getRequester() {
        return requester;
    }

    public void setRequester(Long requester) {
        this.requester = requester;
    }
}
