package id.code.nikku.filter;

import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.RolemenusModel;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class RolemenusFilter {
	@FilterColumn(RolemenusModel._ID)
	private Long id;

	@FilterColumn(RolemenusModel._ROLES_ID)
	private Long rolesId;

	@FilterColumn(RolemenusModel._MENUS_ID)
	private Long menusId;

	@FilterColumn(RolemenusModel._VIEW)
	private Integer view;

	@FilterColumn(RolemenusModel._ADD)
	private Integer add;

	@FilterColumn(RolemenusModel._UPDATE)
	private Integer update;

	@FilterColumn(RolemenusModel._DELETE)
	private Integer delete;


	public Long getId() { return this.id; }
	public Long getRolesId() { return this.rolesId; }
	public Long getMenusId() { return this.menusId; }
	public Integer getView() { return this.view; }
	public Integer getAdd() { return this.add; }
	public Integer getUpdate() { return this.update; }
	public Integer getDelete() { return this.delete; }

	public void setId(Long id) { this.id = id; }
	public void setRolesId(Long rolesId) { this.rolesId = rolesId; }
	public void setMenusId(Long menusId) { this.menusId = menusId; }
	public void setView(Integer view) { this.view = view; }
	public void setAdd(Integer add) { this.add = add; }
	public void setUpdate(Integer update) { this.update = update; }
	public void setDelete(Integer delete) { this.delete = delete; }

}