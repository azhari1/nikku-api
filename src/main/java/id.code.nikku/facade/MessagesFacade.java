package id.code.nikku.facade;

import id.code.component.DatabasePool;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.nikku.filter.MessagesFilter;
import id.code.nikku.model.MessagesModel;
import id.code.nikku.model.UsersModel;
import id.code.nikku.model.modify.MyMessages;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class MessagesFacade {
    public boolean insert(MessagesModel value) throws SQLException, QueryBuilderException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(MessagesModel value) throws SQLException, QueryBuilderException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException {
        final MessagesModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<MessagesModel> getAll(Filter<MessagesFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MessagesModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(MessagesModel.class);
    }

    public List<MessagesModel> getAllWithJoin(Filter<MessagesFilter> filter, long userLoginId) throws SQLException, QueryBuilderException {
        List<MessagesModel> messagesModels = new ArrayList<>();
        QueryBuilder.select("m", MessagesModel.class)
                .includeAllJoin()
                .join("s", UsersModel.class).on("s", "id").isEqual("m", "senderId")
                .join("r", UsersModel.class).on("r", "id").isEqual("m", "receiverId")
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(MessagesModel.class, (result, m) -> {
                    UsersModel s = result.getItem("s", UsersModel.class);
                    UsersModel r = result.getItem("r", UsersModel.class);
                    m.setSender(s);
                    m.setReceiver(r);
                    messagesModels.add(m);
                });
        return messagesModels;
    }

    public List<MyMessages> getAllMyMessage(long loginId) throws SQLException, QueryBuilderException {
        Connection con = DatabasePool.getConnection();
        String query = "SELECT " +
                "users.id as userId, " +
                "users.profilePicture, " +
                "users.fullName, " +
                "messages.message, " +
                "messages.date " +
                "FROM " +
                "(SELECT MAX(messages.id) as id FROM messages " +
                "WHERE " + loginId + " IN (senderId, receiverId) " +
                "GROUP BY IF (" + loginId + "=senderId,receiverId, senderId)) AS latest " +
                "LEFT JOIN messages USING(id) " +
                "LEFT JOIN users ON IF (senderId=" + loginId + ",users.id=receiverId,users.id=senderId) " +
                "ORDER BY messages.id DESC";
        ResultBuilder builder = QueryBuilder.selectRaw(con, query, null);
        List<MyMessages> messages = builder.getItems(MyMessages.class);
        builder.close();
        return messages;
    }

    public List<MessagesModel> getDetailMyMessage(long loginId, long userId) throws SQLException, QueryBuilderException {
        UsersModel users = new UsersFacade().findById(userId);
        if (users != null) {
            //setup messages
            Connection con = DatabasePool.getConnection();
            String query = "SELECT * FROM messages " +
                    "WHERE (senderId=" + loginId + " AND receiverId=" + userId + ") " +
                    "OR (senderId=" + userId + " AND receiverId=" + loginId + ") " +
                    "ORDER BY DATE ASC";
            List<MessagesModel> models = QueryBuilder.selectRaw(con, query, null).getItems(MessagesModel.class);
            for (MessagesModel model : models) {
                if(model.getIsRead()==0){
                    model.setIsRead(1);
                    this.update(model);
                }
            }
            con.close();
            return models;
        }
        return null;
    }

    public MessagesModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MessagesModel.class)
                .where(MessagesModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(MessagesModel.class);
    }

    public List<MessagesModel> findBySenderReceiverId(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MessagesModel.class)
                .where(MessagesModel._SENDER_ID)
                .isEqualOr(id, MessagesModel._RECEIVER_ID)
                .isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItems(MessagesModel.class);
    }

    public boolean findAndDelete(long sender, long receiver) throws SQLException, QueryBuilderException, IOException {
        List<MessagesModel> messagesModels = QueryBuilder.select(MessagesModel.class)
                .getResult(DatabasePool.getConnection())
                .executeItems(MessagesModel.class);
        boolean allDeleted = true;
        for (MessagesModel messagesModel : messagesModels) {
            if (messagesModel.getSenderId() == sender) {
                if (messagesModel.getReceiverId() == receiver) {
                    if (!delete(messagesModel.getId())) {
                        allDeleted = false;
                    }
                }
            } else {
                if (messagesModel.getSenderId() == receiver) {
                    if (messagesModel.getReceiverId() == sender) {
                        if (!delete(messagesModel.getId())) {
                            allDeleted = false;
                        }
                    }
                }
            }
        }
        return allDeleted;
    }
}