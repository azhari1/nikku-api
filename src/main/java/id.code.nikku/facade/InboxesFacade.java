package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.InboxesModel;
import id.code.nikku.filter.InboxesFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class InboxesFacade {
    public boolean insert(InboxesModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(InboxesModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final InboxesModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<InboxesModel> getAll(Filter<InboxesFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(InboxesModel.class)
                .includeAllJoin()
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(InboxesModel.class);
    }

    public InboxesModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(InboxesModel.class)
                .where(InboxesModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(InboxesModel.class);
    }

    public InboxesModel findByReferenceIdAndDate(long referencesId,long date) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(InboxesModel.class)
                .where(InboxesModel._REFERENCE_ID).isEqual(referencesId)
                .where(InboxesModel._DATE).isEqual(date)
                .getResult(DatabasePool.getConnection())
                .executeItem(InboxesModel.class);
    }

    public List<InboxesModel> findByUserId(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(InboxesModel.class)
                .where(InboxesModel._USER_ID)
                .isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItems(InboxesModel.class);
    }
}