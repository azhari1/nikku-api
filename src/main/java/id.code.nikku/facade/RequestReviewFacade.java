package id.code.nikku.facade;

import id.code.component.DatabasePool;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.nikku.filter.RequestReviewFilter;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.model.RequestReviewModel;
import id.code.nikku.model.UsersModel;

import java.sql.SQLException;
import java.util.List;

public class RequestReviewFacade {

    public boolean insert(RequestReviewModel value) throws SQLException, QueryBuilderException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(RequestReviewModel value) throws SQLException, QueryBuilderException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException {
        final RequestReviewModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<RequestReviewModel> getAll(Filter<RequestReviewFilter> filter) throws SQLException, QueryBuilderException {
        if (filter.getParam() != null) {
            if (filter.getParam().getApprover() != null && filter.getParam().getApprover() > 0) {
                return QueryBuilder.select("r", RequestReviewModel.class)
                        .includeAllJoin()
                        .join("u", UsersModel.class).on("u", UsersModel._ID).isEqual("r", RequestReviewModel._REQUESTER)
                        .where("r", RequestReviewModel._STATUS)
                        .isEqual(ConstantHelper.RFR_WAITING)
                        .limitOffset(filter)
                        .orderBy(filter)
                        .filter(filter)
                        .getResult(DatabasePool.getConnection())
                        .executeItems("r", RequestReviewModel.class, (resultBuilder, requestReviewModel) -> requestReviewModel.setUsers(resultBuilder.getItem(UsersModel.class)));
            } else if (filter.getParam().getRequester() != null && filter.getParam().getRequester() > 0) {
                filter.getParam().setApprover(filter.getParam().getRequester());
                filter.getParam().setRequester(null);
                return QueryBuilder.select("r", RequestReviewModel.class)
                        .includeAllJoin()
                        .join("u", UsersModel.class).on("u", UsersModel._ID).isEqual("r", RequestReviewModel._REQUESTER)
                        .where("r", RequestReviewModel._STATUS)
                        .isEqual(ConstantHelper.RFR_APPROVAL)
                        .limitOffset(filter)
                        .orderBy(filter)
                        .filter(filter)
                        .getResult(DatabasePool.getConnection())
                        .executeItems("r", RequestReviewModel.class, (resultBuilder, requestReviewModel) -> requestReviewModel.setUsers(resultBuilder.getItem(UsersModel.class)));
//                return QueryBuilder.select("r", RequestReviewModel.class)
//                        .includeAllJoin()
//                        .join("u", UsersModel.class).on("u", UsersModel._ID).isEqual("r", RequestReviewModel._APPROVER)
//                        .where("r", RequestReviewModel._STATUS)
//                        .isEqualOr(ConstantHelper.RFR_WAITING, "r", RequestReviewModel._STATUS)
//                        .isEqual(ConstantHelper.RFR_APPROVAL)
//                        .limitOffset(filter)
//                        .orderBy(filter)
//                        .filter(filter)
//                        .getResult(DatabasePool.getConnection())
//                        .executeItems("r", RequestReviewModel.class, (resultBuilder, requestReviewModel) -> requestReviewModel.setUsers(resultBuilder.getItem(UsersModel.class)));
            } else {
                return QueryBuilder.select(RequestReviewModel.class)
                        .limitOffset(filter)
                        .orderBy(filter)
                        .filter(filter)
                        .getResult(DatabasePool.getConnection())
                        .executeItems(RequestReviewModel.class);
            }
        } else {
            return QueryBuilder.select(RequestReviewModel.class)
                    .limitOffset(filter)
                    .orderBy(filter)
                    .filter(filter)
                    .getResult(DatabasePool.getConnection())
                    .executeItems(RequestReviewModel.class);
        }
    }

    public RequestReviewModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RequestReviewModel.class)
                .where(RequestReviewModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(RequestReviewModel.class);
    }

    public boolean isAlreadyReviewed(long loginId, long userId) throws SQLException, QueryExecutionException {
        if(loginId==userId){
            return false;
        }else{
            String sql = "select id from " + RequestReviewModel.TABLE_NAME + " where ";
            sql += "(" + RequestReviewModel._REQUESTER + "=" + loginId + " AND " + RequestReviewModel._APPROVER + "=" + userId + ") ";
            sql += "AND " + RequestReviewModel._STATUS + "!=" + ConstantHelper.RFR_REJECTED+" ";

            ResultBuilder result = QueryBuilder.selectRaw(DatabasePool.getConnection(), sql, null);
            boolean ok = result.moveNext();
            result.close();
            return ok;
        }
    }

    public RequestReviewModel findAlreadyExists(long loginId, long userId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RequestReviewModel.class)
                .where(RequestReviewModel._REQUESTER).isEqual(loginId)
                .where(RequestReviewModel._APPROVER).isEqual(userId)
                .getResult(DatabasePool.getConnection())
                .executeItem(RequestReviewModel.class);
    }

}
