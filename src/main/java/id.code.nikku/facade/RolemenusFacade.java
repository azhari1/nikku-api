package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.RolemenusModel;
import id.code.nikku.filter.RolemenusFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class RolemenusFacade {
    public boolean insert(RolemenusModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(RolemenusModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final RolemenusModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<RolemenusModel> getAll(Filter<RolemenusFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RolemenusModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(RolemenusModel.class);
    }

    public RolemenusModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RolemenusModel.class)
                .where(RolemenusModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(RolemenusModel.class);
    }
}