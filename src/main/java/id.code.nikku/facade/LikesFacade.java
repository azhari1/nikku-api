package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.LikesModel;
import id.code.nikku.filter.LikesFilter;
import id.code.nikku.model.ReviewsModel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class LikesFacade {
    public boolean insert(LikesModel value) throws SQLException, QueryBuilderException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean insertOrRemove(LikesModel value) throws SQLException, QueryBuilderException, IOException {
        LikesModel lm = findByReviewUser(value.getUserId(), value.getReviewId());
        if(lm==null){
            calculateTotalLikes(value.getReviewId(),true);
            final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
            return result.isModified();
        }else{
            calculateTotalLikes(value.getReviewId(),false);
            return delete(lm.getId());
        }
    }

    public boolean update(LikesModel value) throws SQLException, QueryBuilderException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException {
        final LikesModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<LikesModel> getAll(Filter<LikesFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(LikesModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(LikesModel.class);
    }

    public LikesModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(LikesModel.class)
                .where(LikesModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(LikesModel.class);
    }

    public LikesModel findByReviewUser(long userId,long reviewId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(LikesModel.class)
                .where(LikesModel._USER_ID).isEqual(userId)
                .where(LikesModel._REVIEW_ID).isEqual(reviewId)
                .getResult(DatabasePool.getConnection())
                .executeItem(LikesModel.class);
    }

    private void calculateTotalLikes(long reviewId, boolean added) throws QueryBuilderException {
        try {
            ReviewsModel reviewsModel = new ReviewsFacade().findById(reviewId);
            if(reviewsModel!=null){
                long oldLikes = reviewsModel.getTotalLikes()==null ? 0 : reviewsModel.getTotalLikes();
                long newLikes;
                if(added){
                    newLikes = oldLikes + 1;
                }else{
                    newLikes = oldLikes - 1;
                }
                reviewsModel.setTotalLikes(newLikes);
                new ReviewsFacade().update(reviewsModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}