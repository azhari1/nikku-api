package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.ViewsModel;
import id.code.nikku.filter.ViewsFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class ViewsFacade {
    public boolean insert(ViewsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(ViewsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final ViewsModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<ViewsModel> getAll(Filter<ViewsFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ViewsModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(ViewsModel.class);
    }

    public ViewsModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ViewsModel.class)
                .where(ViewsModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(ViewsModel.class);
    }

    public ViewsModel findById(long reviewId,long userId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ViewsModel.class)
                .where(ViewsModel._REVIEW_ID).isEqual(reviewId)
                .where(ViewsModel._USER_ID).isEqual(userId)
                .getResult(DatabasePool.getConnection())
                .executeItem(ViewsModel.class);
    }

    public long getTotalViews(long reviewId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ViewsModel.class)
                .where(ViewsModel._REVIEW_ID).isEqual(reviewId)
                .getResult(DatabasePool.getConnection())
                .executeItems(ViewsModel.class).size();
    }
}