package id.code.nikku.facade;

import id.code.component.DatabasePool;
import id.code.component.utility.StringUtility;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.ResultBuilder;
import id.code.database.builder.SelectBuilder;
import id.code.database.filter.Filter;
import id.code.nikku.filter.CityFilter;
import id.code.nikku.model.CityModel;
import id.code.nikku.model.ProvinceModel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CityFacade {

    public List<CityModel> getWithProvince(Filter<CityFilter> filter) throws SQLException, QueryBuilderException {
        SelectBuilder sqlSelect = QueryBuilder.select("c",CityModel.class)
                .includeAllJoin()
                .join("p", ProvinceModel.class).on("p","id").isEqual("c","provinceId")
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter);

        if(StringUtility.isNullOrWhiteSpace(filter.getParam().getCityName())){
           sqlSelect.where("c",CityModel._IS_PRIORITY).isEqual(1);
        }

        try (final ResultBuilder result = sqlSelect.execute(DatabasePool.getConnection())) {
            List<CityModel> cityModels = new ArrayList<>();
            while(result.moveNext()){
                CityModel cityModel = result.getItem("c",CityModel.class);
                ProvinceModel provinceModel = result.getItem("p",ProvinceModel.class);
                cityModel.setProvince(provinceModel);
                cityModels.add(cityModel);
            }
            result.close();
            return cityModels;
        }
    }

}