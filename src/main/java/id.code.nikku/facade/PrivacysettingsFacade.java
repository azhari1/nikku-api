package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.PrivacysettingsModel;
import id.code.nikku.filter.PrivacysettingsFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class PrivacysettingsFacade {
    public boolean insert(PrivacysettingsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(PrivacysettingsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long userId) throws SQLException, QueryBuilderException, IOException {
        final PrivacysettingsModel oldData = this.findByUserId(userId);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<PrivacysettingsModel> getAll(Filter<PrivacysettingsFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(PrivacysettingsModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(PrivacysettingsModel.class);
    }

    public PrivacysettingsModel findByUserId(long userId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(PrivacysettingsModel.class)
                .where(PrivacysettingsModel._USER_ID).isEqual(userId)
                .getResult(DatabasePool.getConnection())
                .executeItem(PrivacysettingsModel.class);
    }
}