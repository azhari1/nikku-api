package id.code.nikku.facade;

import id.code.component.DatabasePool;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.nikku.model.InboxesModel;
import id.code.nikku.model.MessagesModel;
import id.code.nikku.validation.CountNotificationValidation;

import java.sql.SQLException;

public class CountItemNotificationFacade {

    public CountNotificationValidation getCountItemNotification(long userId) throws SQLException, QueryBuilderException {
        int inbox = QueryBuilder.select(InboxesModel.class)
                .where(InboxesModel._USER_ID).isEqual(userId)
                .where(InboxesModel._IS_READ).isEqual(0)
                .getResult(DatabasePool.getConnection())
                .executeItems(InboxesModel.class).size();
        int message = QueryBuilder.select(MessagesModel.class)
                .where(MessagesModel._RECEIVER_ID).isEqual(userId)
                .where(MessagesModel._IS_READ).isEqual(0)
                .getResult(DatabasePool.getConnection())
                .executeItems(MessagesModel.class).size();
        CountNotificationValidation c = new CountNotificationValidation();
        int counter = inbox + message;
        c.setCounter(counter);
        return c;
    }
}
