package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.ProvinceModel;
import id.code.nikku.filter.ProvinceFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class ProvinceFacade {
    public boolean insert(ProvinceModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(ProvinceModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final ProvinceModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<ProvinceModel> getAll(Filter<ProvinceFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ProvinceModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(ProvinceModel.class);
    }

    public ProvinceModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ProvinceModel.class)
                .where(ProvinceModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(ProvinceModel.class);
    }
}