package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.model.*;
import id.code.nikku.filter.UserworkatcompaniesFilter;
import id.code.nikku.model.modify.MyProfileWorkingExperiences;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class UserworkatcompaniesFacade {
    public boolean insert(UserworkatcompaniesModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(UserworkatcompaniesModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final UserworkatcompaniesModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<UserworkatcompaniesModel> getAll(Filter<UserworkatcompaniesFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UserworkatcompaniesModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(UserworkatcompaniesModel.class);
    }

    public List<UserworkatcompaniesModel> getAllWithJoin(Filter<UserworkatcompaniesFilter> filter) throws SQLException, QueryBuilderException {
        final SelectBuilder builder = QueryBuilder.select("uw",UserworkatcompaniesModel.class)
                .includeAllJoin()
                .join("cp", CompaniesoutletsModel.class).on("uw","companyId").isEqual("cp","id")
                .join("us", UsersModel.class).on("uw", "userID").isEqual("us","id")
                .limitOffset(filter)
                .orderBy("uw",filter)
                .filter("uw",filter);

        try (final ResultBuilder result = builder.execute(DatabasePool.getConnection())) {
            List<UserworkatcompaniesModel> userworkatcompaniesModels = new ArrayList<>();
            CompanyoutletpicsFacade cps = new CompanyoutletpicsFacade();
            ReviewsFacade reviewsFacade = new ReviewsFacade();
            while(result.moveNext()){
                UserworkatcompaniesModel uw = result.getItem("uw",UserworkatcompaniesModel.class);
                CompaniesoutletsModel cp = result.getItem("cp", CompaniesoutletsModel.class);
                UsersModel us = result.getItem("us", UsersModel.class);

                //update rating score by company
                    double avgRatingCompany = reviewsFacade.getRatingScoreByUserAndCompany(uw.getCompanyId(), uw.getUserId());
                    us.setAvgRatingCompany(avgRatingCompany);
                //end update rating score by company
                CompanyoutletpicsModel pic = cps.findByCU(cp.getId(), us.getId());
                uw.setCompaniesoutletsModel(cp);
                uw.setUsersModel(us);
                uw.setCompanyoutletpicsModel(pic);
                userworkatcompaniesModels.add(uw);
            }
            result.close();
            return userworkatcompaniesModels;
        }
    }


    public UserworkatcompaniesModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UserworkatcompaniesModel.class)
                .where(UserworkatcompaniesModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(UserworkatcompaniesModel.class);
    }

    public List<UsersModel> findUsersById(long id) throws SQLException, QueryBuilderException {
        List<UsersModel> usersModels = new ArrayList<>();
        QueryBuilder.select("uat",UserworkatcompaniesModel.class)
                .includeAllJoin()
                .join("usr",UsersModel.class).on("uat",UserworkatcompaniesModel._USER_ID).isEqual("usr",UsersModel._ID)
                .join("c", CityModel.class).on("usr",UsersModel._CITY_ID).isEqual("c",CityModel._ID)
                .join("p",ProvinceModel.class).on("c",CityModel._PROVINCE_ID).isEqual("p",ProvinceModel._ID)
                .join("s",PrivacysettingsModel.class).on("s",PrivacysettingsModel._USER_ID).isEqual("usr",UsersModel._ID)
                .where("uat",UserworkatcompaniesModel._COMPANY_ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItems("usr",UsersModel.class,(result, usersModel) -> {
                    CityModel c = result.getItem("c",CityModel.class);
                    ProvinceModel p = result.getItem("p",ProvinceModel.class);
                    PrivacysettingsModel s = result.getItem("s",PrivacysettingsModel.class);
                    c.setProvince(p);
                    usersModel.setCityModel(c);
                    usersModel.setPrivacysettingsModel(s);
                    usersModels.add(usersModel);
                });
        return usersModels;
    }

    public boolean isUserActive(long userId,long companyId) throws SQLException, QueryBuilderException {
        UserworkatcompaniesModel uat = QueryBuilder.select(UserworkatcompaniesModel.class)
                .where(UserworkatcompaniesModel._USER_ID).isEqual(userId)
                .where(UserworkatcompaniesModel._ID).isEqual(companyId)
                .where(UserworkatcompaniesModel._STATUS)
                    .endWithOr(ConstantHelper.STATUS_APPROVED, UserworkatcompaniesModel._STATUS)
                    .endWith(ConstantHelper.STATUS_MARK_AS_PIC)
                .getResult(DatabasePool.getConnection())
                .executeItem(UserworkatcompaniesModel.class);
        return uat != null;
    }

    public List<UsersModel> getUsersActive(long companyId) throws SQLException, QueryBuilderException {
         return QueryBuilder.select("uat",UserworkatcompaniesModel.class)
                 .includeAllJoin()
                 .join("usr", UsersModel.class)
                    .on("usr",UsersModel._ID)
                    .isEqual("uat",UserworkatcompaniesModel._USER_ID)
                 .where("uat",UserworkatcompaniesModel._COMPANY_ID).isEqual(companyId)
                 .where("uat",UserworkatcompaniesModel._STATUS)
                    .isEqualOr(ConstantHelper.STATUS_APPROVED,"uat",UserworkatcompaniesModel._STATUS)
                 .isEqual(ConstantHelper.STATUS_MARK_AS_PIC)
                 .getResult(DatabasePool.getConnection())
                 .executeItems("usr",UsersModel.class);
    }

    public List<UserworkatcompaniesModel> findByUserId(long id) throws SQLException, QueryBuilderException {
        List<UserworkatcompaniesModel> uats = new ArrayList<>();
        QueryBuilder.select("uat",UserworkatcompaniesModel.class)
                .includeAllJoin()
                .join("com", CompaniesoutletsModel.class)
                    .on("com","id")
                    .isEqual("uat","companyId")
                .where("uat",UserworkatcompaniesModel._USER_ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItems(UserworkatcompaniesModel.class, (resultBuilder, com) -> {
                    CompaniesoutletsModel cp = resultBuilder.getItem(CompaniesoutletsModel.class);
                    com.setCompaniesoutletsModel(cp);
                    uats.add(com);
                });
        return uats;
    }


    public List<MyProfileWorkingExperiences> getMyProfileWorkingExperiences(long userId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MyProfileWorkingExperiences.class)
                .where(UserworkatcompaniesModel._USER_ID).isEqual(userId)
                .where(UserworkatcompaniesModel._IS_HIDE).isEqual(0)
                .where(UserworkatcompaniesModel._STATUS)
                    .isNotEqualAnd(ConstantHelper.STATUS_ON_PROGRESS,UserworkatcompaniesModel._STATUS)
                .isNotEqual(ConstantHelper.STATUS_REJECTED)
                .getResult(DatabasePool.getConnection())
                .executeItems(MyProfileWorkingExperiences.class);
    }

    public List<CompanyoutletpicsModel> getAllMyCompanyAsPIC(long loginId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(CompanyoutletpicsModel.class,CompanyoutletpicsModel._COMPANY_ID)
                .where(CompanyoutletpicsModel._USER_ID).isEqual(loginId)
                .getResult(DatabasePool.getConnection())
                .executeItems(CompanyoutletpicsModel.class);
    }

}