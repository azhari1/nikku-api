package id.code.nikku.facade;

import id.code.component.DatabasePool;
import id.code.component.PasswordHasher;
import id.code.component.utility.StringUtility;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.nikku.api.AccessTokenPayload;
import id.code.nikku.filter.UsersFilter;
import id.code.nikku.filter.UsersSearchFilter;
import id.code.nikku.model.*;
import id.code.nikku.model.modify.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class UsersFacade {

    private final UserworkatcompaniesFacade userworkatcompaniesFacade = new UserworkatcompaniesFacade();

    public boolean insert(UsersModel value) throws SQLException, QueryBuilderException, IOException, GeneralSecurityException {
        value.setPassword(PasswordHasher.computePasswordHash(value.getPassword()));
        value.setDateCreated(Calendar.getInstance().getTimeInMillis());
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean insertAccount(CreateAccount value) throws SQLException, QueryBuilderException {
        value.setDateCreated(Calendar.getInstance().getTimeInMillis());
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(UsersModel value) throws SQLException, QueryBuilderException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException {
        final UsersModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<UsersModel> getAll(Filter<UsersFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UsersModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(UsersModel.class);
    }

    public List<UsersModel> getFromSearch(Filter<UsersSearchFilter> filter) throws SQLException, QueryBuilderException {
        SelectBuilder sqlSelect = QueryBuilder.select("u", UsersModel.class)
                .includeAllJoin()
                .join("c", CityModel.class).on("c", "id").isEqual("u", "cityId")
                .join("p", ProvinceModel.class).on("p", "id").isEqual("c", "provinceId")
                .join("s", PrivacysettingsModel.class).on("s", "userId").isEqual("u", "id")
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter);

        if (!StringUtility.isNullOrWhiteSpace(filter.getParam().getSkillName())) {
            sqlSelect.where("u", "id").in(
                    QueryBuilder.select(SkillsModel.class, "userId")
                            .where("skillName").contains(filter.getParam().getSkillName()));
        }

        return sqlSelect.getResult(DatabasePool.getConnection())
                .executeItems(UsersModel.class, (result, u) -> {
                    CityModel c = result.getItem(CityModel.class);
                    ProvinceModel p = result.getItem(ProvinceModel.class);
                    PrivacysettingsModel s = result.getItem(PrivacysettingsModel.class);
                    c.setProvince(p);
                    u.setCityModel(c);
                    u.setPrivacysettingsModel(s);
                });
    }

    public UsersModel getByFilter(Filter<MyProfile> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UsersModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItem(UsersModel.class);
    }

    public List<UsersModel> getAllWithJoin(Filter<UsersFilter> filter) throws SQLException, QueryBuilderException {
        List<UsersModel> usersModels = new ArrayList<>();
        QueryBuilder.select("u", UsersModel.class)
                .includeAllJoin()
                .join("c", CityModel.class).on("c", "id").isEqual("u", "cityId")
                .join("p", ProvinceModel.class).on("p", "id").isEqual("c", "provinceId")
                .join("s", PrivacysettingsModel.class).on("s", "userId").isEqual("u", "id")
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(UsersModel.class, (result, u) -> {
                    CityModel c = result.getItem(CityModel.class);
                    ProvinceModel p = result.getItem(ProvinceModel.class);
                    PrivacysettingsModel s = result.getItem(PrivacysettingsModel.class);
                    c.setProvince(p);
                    u.setCityModel(c);
                    u.setPrivacysettingsModel(s);
                    usersModels.add(u);
                });
        return usersModels;
    }

    public List<TopRatedPerson> getTopRatedPerson(Filter<TopRatedPerson> filter) throws SQLException, QueryBuilderException {
        String query = "select * from "+UsersModel.TABLE_NAME+" where "+UsersModel._IS_ACTIVE+"=1 order by "+UsersModel._AVG_RATING+" desc limit "+filter.getPageSize()+ "  offset "+filter.getPageOffset();
        return QueryBuilder.selectRaw(DatabasePool.getConnection(), query,null).getItems(TopRatedPerson.class);
    }

    public List<TopRatedPerson> getTopRatedPerson() throws SQLException, QueryBuilderException {
        return QueryBuilder.select(TopRatedPerson.class)
                .where(UsersModel._IS_ACTIVE).isEqual(true)
                .orderBy(UsersModel._AVG_RATING).desc()
                .limit(6)
                .getResult(DatabasePool.getConnection())
                .executeItems(TopRatedPerson.class);
    }

    public UsersModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UsersModel.class)
                .where(UsersModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(UsersModel.class);
    }

    public MyProfile getMyProfile(long loginId, Filter<MyProfile> filter) throws QueryBuilderException, SQLException {
        UsersModel um = getByFilter(filter);
        if (um != null) {
            MyProfile myProfile = new MyProfile();
            //personal information
            myProfile.setId(um.getId());
            myProfile.setNik(um.getNik());
            myProfile.setFullName(um.getFullName());
            myProfile.setProfilePicture(um.getProfilePicture());
            myProfile.setJobTitle(um.getJobTitle());
            myProfile.setBio(um.getBio());
            myProfile.setPhone(um.getPhone());
            myProfile.setEmail(um.getEmail());
            myProfile.setSex(um.getSex());
            myProfile.setDateOfBirth(um.getDateOfBirth());
            List<MyProfileWorkingExperiences> myProfileWorkingExperiences = getMyWorkingExperience(myProfile.getId());
            boolean isPIC = cekIsAsPicLogin(loginId, myProfileWorkingExperiences);
            myProfile.setIsPIC(isPIC ? 1 : 0);
            //end personal information
            myProfile.setWorkingExperiences(myProfileWorkingExperiences);
            myProfile.setSkills(getMySkills(myProfile.getId()));
            myProfile.setPopularReviews(getMyPopularReviews(loginId, myProfile.getId(),null));
            myProfile.setPrivacysettings(getPrivacySettings(myProfile.getId()));
            //cek if already reviewed
            boolean already_reviewed = cekIsAlreadyReview(loginId, myProfile.getId());
            myProfile.setAlready_reviewed(already_reviewed ? 1 : 0);
            return myProfile;
        } else {
            return null;
        }
    }

    private boolean cekIsAsPicLogin(long loginId, List<MyProfileWorkingExperiences> comUSER) throws QueryBuilderException, SQLException {
        boolean isPIC = false;
        List<CompanyoutletpicsModel> comPIC = this.userworkatcompaniesFacade.getAllMyCompanyAsPIC(loginId);
        boolean foundSameCompany = false;
        for (MyProfileWorkingExperiences aComUSER : comUSER) {
            //cek is as pic
            for (CompanyoutletpicsModel aComPIC : comPIC) {
                if (aComPIC.getCompanyId() == aComUSER.getCompanyId()) {
                    foundSameCompany=true;
                    break;
                }
            }
            if(foundSameCompany){
                isPIC = true;
                break;
            }
        }
        return isPIC;
    }

    private boolean cekIsAlreadyReview(long loginId,long userId) throws SQLException, QueryExecutionException {
        RequestReviewFacade requestReviewFacade = new RequestReviewFacade();
        return requestReviewFacade.isAlreadyReviewed(loginId, userId);
    }
    private List<MyProfileWorkingExperiences> getMyWorkingExperience(long userId) throws QueryBuilderException, SQLException {
        return this.userworkatcompaniesFacade.getMyProfileWorkingExperiences(userId);
    }

    private List<MyProfileSkills> getMySkills(long userId) throws QueryBuilderException, SQLException {
        return new SkillsFacade().getMySkills(userId);
    }

    public List<MyProfilePopularReviews> getMyPopularReviews(long loginId, long userId,Filter<MyProfilePopularReviews> filter) throws QueryBuilderException, SQLException {
        return new ReviewsFacade().getMyPopularReviewes(loginId, userId, filter);
    }

    private PrivacysettingsModel getPrivacySettings(long userId) throws QueryBuilderException, SQLException {
        return new PrivacysettingsFacade().findByUserId(userId);
    }

    public UsersModel findByEmail(String email) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UsersModel.class)
                .where(UsersModel._EMAIL).isEqual(email)
                .getResult(DatabasePool.getConnection())
                .executeItem(UsersModel.class);
    }

    public UsersModel login(String email, String password) throws SQLException, GeneralSecurityException, UnsupportedEncodingException, QueryBuilderException {
        final UsersModel model = QueryBuilder.select(UsersModel.class)
                .where(UsersModel._EMAIL).isEqual(email).limit(1)
                .getResult(DatabasePool.getConnection())
                .executeItem(UsersModel.class);

        return model != null && PasswordHasher.verify(password, model.getPassword()) ? model : null;
    }

    public UsersModel loginFacebook(String email, String googleId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(UsersModel.class)
                .where(UsersModel._EMAIL).isEqual(email)
                .where(UsersModel._GOOGLE_ID).isEqual(googleId)
                .limit(1)
                .getResult(DatabasePool.getConnection())
                .executeItem(UsersModel.class);
    }

    public boolean isEmailAvailable(String email) throws SQLException, QueryBuilderException {
        try (ResultBuilder result = QueryBuilder.select(UsersModel.class, UsersModel._ID)
                .where(UsersModel._EMAIL).isEqual(email).limit(1)
                .execute(DatabasePool.getConnection())) {
            return !result.moveNext();
        }
    }

    public boolean changePassword(UsersModel model, String newPassword) throws SQLException, GeneralSecurityException, UnsupportedEncodingException, QueryBuilderException {
        model.setPassword(PasswordHasher.computePasswordHash(newPassword));
        UpdateResult result = QueryBuilder.update(model, UsersModel._PASSWORD)
                .execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public UsersModel verify(String code) {
        try (ResultBuilder result = QueryBuilder.select(UsersModel.class)
                .where(UsersModel._UID).isEqual(code)
                .execute(DatabasePool.getConnection())) {
            if (result.moveNext()) {
                return result.getItem(UsersModel.class);
            } else {
                return null;
            }
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    public UsersModel isEmailValidated(String email) {
        try (ResultBuilder result = QueryBuilder.select(UsersModel.class)
                .where(UsersModel._EMAIL).isEqual(email)
                .execute(DatabasePool.getConnection())) {
            if (result.moveNext()) {
                return result.getItem(UsersModel.class);
            } else {
                return null;
            }
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean isNIKAvailable(String nik, AccessTokenPayload payload) throws SQLException, QueryBuilderException {
        List<UsersModel> usersModels = QueryBuilder.select(UsersModel.class, UsersModel._ID)
                .where(UsersModel._NIK).isEqual(nik)
                .getResult(DatabasePool.getConnection())
                .executeItems(UsersModel.class);
        if (usersModels.size() > 0) {
            boolean isNikCurrentLogin = false;
            for (UsersModel usersModel : usersModels) {
                if (usersModel.getId() == payload.getId()) {
                    isNikCurrentLogin = true;
                }
            }
            return isNikCurrentLogin;
        } else {
            return true;
        }
    }

    public UsersModel isNIkAvailableForMerge(String nik) throws SQLException, QueryBuilderException {
        UsersModel usersModel = QueryBuilder.select(UsersModel.class)
                .where(UsersModel._NIK).isEqual(nik)
                .getResult(DatabasePool.getConnection())
                .executeItem(UsersModel.class);
        if (usersModel != null) {
            String[] emails = usersModel.getEmail().split("@");
            if (emails.length > 0) {
                if (emails.length == 2) {
                    String nIK = emails[0];
                    String eMAIL = emails[1];
                    if (nIK.equals(nik) && eMAIL.equals("gmail.com")) {
                        return usersModel;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean isNIKAvailable(String nik) throws SQLException, QueryBuilderException {
        UsersModel um = QueryBuilder.select(UsersModel.class)
                .where(UsersModel._NIK).isEqual(nik)
                .getResult(DatabasePool.getConnection())
                .executeItem(UsersModel.class);
        return um == null;
    }

}