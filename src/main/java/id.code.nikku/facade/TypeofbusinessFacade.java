package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.TypeofbusinessModel;
import id.code.nikku.filter.TypeofbusinessFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class TypeofbusinessFacade {
    public boolean insert(TypeofbusinessModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(TypeofbusinessModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final TypeofbusinessModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<TypeofbusinessModel> getAll(Filter<TypeofbusinessFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(TypeofbusinessModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(TypeofbusinessModel.class);
    }

    public TypeofbusinessModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(TypeofbusinessModel.class)
                .where(TypeofbusinessModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(TypeofbusinessModel.class);
    }
}