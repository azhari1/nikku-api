package id.code.nikku.facade;

import id.code.component.DatabasePool;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.filter.Filter;
import id.code.nikku.model.FaqModel;

import java.sql.SQLException;
import java.util.List;

public class FaqFacade {
    public List<FaqModel> getAll(Filter<FaqModel> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(FaqModel.class)
                .includeAllJoin()
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(FaqModel.class);
    }
}
