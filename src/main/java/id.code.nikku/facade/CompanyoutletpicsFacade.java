package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.CompanyoutletpicsModel;
import id.code.nikku.filter.CompanyoutletpicsFilter;
import id.code.nikku.model.UsersModel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CompanyoutletpicsFacade {
    public boolean insert(CompanyoutletpicsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(CompanyoutletpicsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final CompanyoutletpicsModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<CompanyoutletpicsModel> getAll(Filter<CompanyoutletpicsFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(CompanyoutletpicsModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(CompanyoutletpicsModel.class);
    }

    public CompanyoutletpicsModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(CompanyoutletpicsModel.class)
                .where(CompanyoutletpicsModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(CompanyoutletpicsModel.class);
    }

    public List<UsersModel> findByCompany(long companyId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select("c",CompanyoutletpicsModel.class)
                .includeAllJoin()
                .join("u",UsersModel.class).on("u",UsersModel._ID).isEqual("c",CompanyoutletpicsModel._USER_ID)
                .where("c",CompanyoutletpicsModel._COMPANY_ID).isEqual(companyId)
                .getResult(DatabasePool.getConnection())
                .executeItems("u",UsersModel.class);
    }

    public CompanyoutletpicsModel findByCU(long company,long user) throws SQLException, QueryBuilderException {
        CompanyoutletpicsModel cp;
        cp = QueryBuilder.select(CompanyoutletpicsModel.class)
                .where(CompanyoutletpicsModel._COMPANY_ID).isEqual(company)
                .where(CompanyoutletpicsModel._USER_ID).isEqual(user)
                .getResult(DatabasePool.getConnection())
                .executeItem(CompanyoutletpicsModel.class);
        return cp;
    }
}