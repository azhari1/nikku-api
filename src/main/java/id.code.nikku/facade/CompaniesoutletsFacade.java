package id.code.nikku.facade;

import id.code.component.DatabasePool;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.nikku.filter.CompaniesoutletsFilter;
import id.code.nikku.helper.ConstantHelper;
import id.code.nikku.model.*;
import id.code.nikku.model.modify.DetailCompany;
import id.code.nikku.model.modify.DetailCompanyUsers;
import id.code.nikku.model.modify.FeaturedCompany;
import id.code.nikku.model.modify.MyCompany;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CompaniesoutletsFacade {
    public boolean insert(CompaniesoutletsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(CompaniesoutletsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final CompaniesoutletsModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<CompaniesoutletsModel> getAll(Filter<CompaniesoutletsFilter> filter) throws SQLException, QueryBuilderException {
        filter.getParam().setIsDisabled(0);
        return QueryBuilder.select("c",CompaniesoutletsModel.class)
                .includeAllJoin()
                .join("t", TypeofbusinessModel.class).on("c","typeOfBusinessID").isEqual("t","id")
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(CompaniesoutletsModel.class, (result, value) -> {
                    TypeofbusinessModel tp = result.getItem("t",TypeofbusinessModel.class);
                    value.setTypeofbusinessModel(tp);
                });
    }

    public List<CompaniesoutletsModel> getAllWithType(Filter<CompaniesoutletsFilter> filter, long userId) throws SQLException, QueryBuilderException {
        int isNotDisabled = 0;
        final SelectBuilder builder = QueryBuilder.select("c",CompaniesoutletsModel.class)
                .includeAllJoin()
                .join("t", TypeofbusinessModel.class).on("c","typeOfBusinessID").isEqual("t","id")
                .where("c",CompaniesoutletsModel._IS_DISABLED).isEqual(isNotDisabled)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter);

        try (final ResultBuilder result = builder.execute(DatabasePool.getConnection())) {
            List<CompaniesoutletsModel> list = new ArrayList<>();
            CompanyoutletpicsFacade cpics = new CompanyoutletpicsFacade();
            while (result.moveNext()){
                CompaniesoutletsModel cp = result.getItem("c",CompaniesoutletsModel.class);
                TypeofbusinessModel tp = result.getItem("t",TypeofbusinessModel.class);
                cp.setTypeofbusinessModel(tp);
                if(cp.getOwner()==userId){
                    list.add(cp);
                }else{
                    //cek if as pic
                    CompanyoutletpicsModel cps = cpics.findByCU(cp.getId(), userId);
                    if(cps!=null && cps.getStatus()==ConstantHelper.STATUS_ACTIVE){
                        list.add(cp);
                    }
                }
            }
            result.close();
            return list;
        }
    }

    public List<MyCompany> getMyCompany(long userLoginId) throws SQLException, QueryBuilderException {
        int isNotDisabled = 0;
        List<MyCompany> myCompanies = new ArrayList<>();
        QueryBuilder.select(MyCompany.class)
                .where(CompaniesoutletsModel._IS_DISABLED).isEqual(isNotDisabled)
                .where(CompaniesoutletsModel._OWNER).isEqual(userLoginId)
                .getResult(DatabasePool.getConnection())
                .executeItems(MyCompany.class, (result, myCompany) -> myCompanies.add(myCompany));
        myCompanies.addAll(getMyCompanyAsPIC(userLoginId));
        return myCompanies;
    }

    private List<MyCompany> getMyCompanyAsPIC(long userLoginId) throws SQLException, QueryBuilderException {
        int isNotDisabled = 0;
        return QueryBuilder.select("com",MyCompany.class)
                .join("pic",CompanyoutletpicsModel.class).on("pic",CompanyoutletpicsModel._COMPANY_ID).isEqual("com",CompanyoutletpicsModel._ID)
                .where("pic",CompanyoutletpicsModel._USER_ID).isEqual(userLoginId)
                .where("com",CompaniesoutletsModel._IS_DISABLED).isEqual(isNotDisabled)
                .getResult(DatabasePool.getConnection())
                .executeItems(MyCompany.class);
    }

    public CompaniesoutletsModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select("c",CompaniesoutletsModel.class)
                .includeAllJoin()
                .join("t", TypeofbusinessModel.class).on("c","typeOfBusinessID").isEqual("t","id")
                .where("c",CompaniesoutletsModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem("c",CompaniesoutletsModel.class, (result,value) -> {
                    TypeofbusinessModel tp = result.getItem("t",TypeofbusinessModel.class);
                    value.setTypeofbusinessModel(tp);
                });
    }

    public boolean siupIsExists(String siup) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(CompaniesoutletsModel.class)
                .where(CompaniesoutletsModel._SIUP_TDPNUMBER).isEqual(siup)
                .getResult(DatabasePool.getConnection())
                .executeItem(CompaniesoutletsModel.class) != null;
    }

    public List<UsersModel> findOwnerAndPIC(long companyId) throws SQLException, QueryBuilderException {
        List<UsersModel> usersModels = new ArrayList<>();
        CompaniesoutletsModel cp = QueryBuilder.select(CompaniesoutletsModel.class)
                .where(CompaniesoutletsModel._ID).isEqual(companyId)
                .getResult(DatabasePool.getConnection())
                .executeItem(CompaniesoutletsModel.class);
        if(cp!=null){
            long ownerId = cp.getOwner();
            if(ownerId>0){
                UsersModel owner = new UsersFacade().findById(ownerId);
                if(owner!=null){
                    usersModels.add(owner);
                }
                //find all pic on this company
                List<UsersModel> pic = new CompanyoutletpicsFacade().findByCompany(companyId);
                if(pic.size()>0){
                    usersModels.addAll(pic);
                }
            }
        }
        return usersModels;
    }

    public DetailCompany getDetailCompany(long companyID) throws SQLException, QueryBuilderException {
        DetailCompany detailCompany = QueryBuilder.select(DetailCompany.class)
                .where(CompaniesoutletsModel._ID).isEqual(companyID)
                .getResult(DatabasePool.getConnection())
                .executeItem(DetailCompany.class);
        if(detailCompany!=null){
            List<DetailCompanyUsers> users = QueryBuilder.select(DetailCompanyUsers.class)
                    .where(UserworkatcompaniesModel._COMPANY_ID).isEqual(companyID)
                    .where(UserworkatcompaniesModel._STATUS)
                            .isEqualOr(ConstantHelper.STATUS_APPROVED, UserworkatcompaniesModel._STATUS)
                            .isEqual(ConstantHelper.STATUS_MARK_AS_PIC)
                    .getResult(DatabasePool.getConnection())
                    .executeItems(DetailCompanyUsers.class);
            detailCompany.setUsers(users);
        }
        return detailCompany;
    }

    public List<FeaturedCompany> getFeaturedCompany(Filter<FeaturedCompany> filter) throws SQLException, QueryBuilderException {
        int isFeatured = 1;
        int isNotDisabled = 0;
        return QueryBuilder.select(FeaturedCompany.class)
                .where(CompaniesoutletsModel._IS_FEATURED).isEqual(isFeatured)
                .where(CompaniesoutletsModel._IS_DISABLED).isEqual(isNotDisabled)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(FeaturedCompany.class);
    }

    public List<FeaturedCompany> getFeaturedCompany() throws SQLException, QueryBuilderException {
        int isFeatured = 1;
        int isNotDisabled = 0;
        return QueryBuilder.select(FeaturedCompany.class)
                .where(CompaniesoutletsModel._IS_FEATURED).isEqual(isFeatured)
                .where(CompaniesoutletsModel._IS_DISABLED).isEqual(isNotDisabled)
                .orderBy(CompaniesoutletsModel._ORDER_LIST).desc()
                .limit(9)
                .getResult(DatabasePool.getConnection())
                .executeItems(FeaturedCompany.class);
    }

}