package id.code.nikku.facade;

import id.code.component.DatabasePool;
import id.code.component.utility.StringUtility;
import id.code.database.builder.*;
import id.code.database.filter.Filter;
import id.code.nikku.filter.ReviewsFilter;
import id.code.nikku.model.InboxesModel;
import id.code.nikku.model.LikesModel;
import id.code.nikku.model.ReviewsModel;
import id.code.nikku.model.UsersModel;
import id.code.nikku.model.modify.CompanyPersonReviewModel;
import id.code.nikku.model.modify.MyProfilePopularReviews;
import id.code.nikku.model.modify.MyReview;
import id.code.nikku.model.modify.UserCanView;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class ReviewsFacade {
    public boolean insert(ReviewsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        if (result.isModified()) {
            long userId = value.getUserId();
            UsersModel usersModel = new UsersFacade().findById(userId);
            return usersModel != null && updateRating(usersModel);
        } else {
            return false;
        }
    }

    public long insertLastId(ReviewsModel value) throws SQLException, QueryBuilderException, IOException {
        if (insert(value)) {
            String query = "SELECT LAST_INSERT_ID() from " + ReviewsModel.TABLE_NAME;
            ResultBuilder rs = QueryBuilder.selectRaw(DatabasePool.getConnection(), query, null);
            long lastInsertedId = 0;
            while (rs.moveNext()) {
                lastInsertedId = rs.getLongScalar();
            }
            return lastInsertedId;
        } else {
            return 0;
        }
    }

    public boolean update(ReviewsModel value) throws SQLException, QueryBuilderException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException {
        final ReviewsModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<ReviewsModel> getAll(Filter<ReviewsFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ReviewsModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(ReviewsModel.class);
    }

    public List<ReviewsModel> getAllWithJoin(Filter<ReviewsFilter> filter, long userLogin) throws SQLException, QueryBuilderException {
        final SelectBuilder builder = QueryBuilder.select("r", ReviewsModel.class)
                .includeAllJoin()
                .join("u", UsersModel.class).on("r", "userId").isEqual("u", "id")
                .join("v", UsersModel.class).on("r", "reviewerId").isEqual("v", "id")
                .limitOffset(filter)
                .orderBy("r", filter)
                .filter("r", filter);

        try (final ResultBuilder result = builder.execute(DatabasePool.getConnection())) {
            ArrayList<ReviewsModel> reviewsModels = new ArrayList<>();
            LikesFacade lf = new LikesFacade();
            while (result.moveNext()) {
                ReviewsModel r = result.getItem("r", ReviewsModel.class);
                UsersModel u = result.getItem("u", UsersModel.class);
                UsersModel v = result.getItem("v", UsersModel.class);
                LikesModel lm = lf.findByReviewUser(userLogin, r.getId());
//                r.setUsersModel(u);
//                r.setUsersReviewers(v);
                r.setUsersModel(v);
                r.setUsersReviewers(u);
                r.setLikes(lm);
                reviewsModels.add(r);
            }
            return reviewsModels;
        }
    }

    public ReviewsModel getAllWithJoin(InboxesModel inboxesModel, long userLogin) throws SQLException, QueryBuilderException {
        SelectBuilder builder = QueryBuilder.select("r", ReviewsModel.class)
                .includeAllJoin()
                .join("u", UsersModel.class).on("r", "userId").isEqual("u", "id")
                .join("v", UsersModel.class).on("r", "reviewerId").isEqual("v", "id")
                .where("r", ReviewsModel._USER_ID).isEqual(userLogin)
                .where("r", ReviewsModel._REVIEW_TYPE).isEqual(inboxesModel.getInboxType())
                .where("r", ReviewsModel._DATE).isEqual(inboxesModel.getDate());


        try (final ResultBuilder result = builder.execute(DatabasePool.getConnection())) {
            ReviewsModel rm = null;
            LikesFacade lf = new LikesFacade();
            if (result.moveNext()) {
                rm = result.getItem("r", ReviewsModel.class);
                UsersModel u = result.getItem("u", UsersModel.class);
                UsersModel v = result.getItem("v", UsersModel.class);
                LikesModel lm = lf.findByReviewUser(userLogin, rm.getId());
                rm.setUsersModel(u);
                rm.setUsersReviewers(v);
                rm.setLikes(lm);
            }
            result.close();
            return rm;
        }
    }

    public ReviewsModel findCompanyIdAndDate(long companyId, long date) throws SQLException, QueryBuilderException {
        return QueryBuilder.select("r", ReviewsModel.class)
                .includeAllJoin()
                .join("u", UsersModel.class).on("r", "userId").isEqual("u", "id")
                .join("v", UsersModel.class).on("r", "reviewerId").isEqual("v", "id")
                .where("r", ReviewsModel._AS_COMPANY_ID).isEqual(companyId)
                .where("r", ReviewsModel._DATE).isEqual(date)
                .getResult(DatabasePool.getConnection())
                .executeItem(ReviewsModel.class, (result, reviewsModel) -> {
                    UsersModel u = result.getItem("u", UsersModel.class);
                    UsersModel v = result.getItem("v", UsersModel.class);
                    reviewsModel.setUsersModel(u);
                    reviewsModel.setUsersReviewers(v);
                });
    }


    public ReviewsModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select("r", ReviewsModel.class)
                .includeAllJoin()
                .join("u", UsersModel.class).on("r", "userId").isEqual("u", "id")
                .join("v", UsersModel.class).on("r", "reviewerId").isEqual("v", "id")
                .where("r", ReviewsModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(ReviewsModel.class, (result, reviewsModel) -> {
                    UsersModel u = result.getItem("u", UsersModel.class);
                    UsersModel v = result.getItem("v", UsersModel.class);
//                    reviewsModel.setUsersModel(u);
//                    reviewsModel.setUsersReviewers(v);
                    reviewsModel.setUsersModel(v);
                    reviewsModel.setUsersReviewers(u);
                });
    }

    public List<ReviewsModel> findByUserId(long userId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(ReviewsModel.class)
                .where(ReviewsModel._USER_ID).isEqual(userId)
                .getResult(DatabasePool.getConnection())
                .executeItems(ReviewsModel.class);
    }

    public float getRatingScoreByUserAndCompany(long companyId, long userId) throws SQLException, QueryBuilderException {
        String query = "select avg(ratingScore) as ratingScore from " + ReviewsModel.TABLE_NAME + " where ";
        query += ReviewsModel._AS_COMPANY_ID + "=" + companyId + " AND ";
        query += ReviewsModel._USER_ID + "=" + userId;
        List<Object> reviewsModels = new ArrayList<>();
        try (final ResultBuilder result = QueryBuilder.selectRaw(DatabasePool.getConnection(), query, reviewsModels)) {
            if (!result.moveNext()) return 0;
            else return (float) result.getItem(ReviewsModel.class).getRatingScore();
        }
    }

    public List<MyReview> getMyReviews(long userLoginId) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MyReview.class)
                .where(ReviewsModel._REVIEWER_ID).isEqual(userLoginId)
                .getResult(DatabasePool.getConnection())
                .executeItems(MyReview.class);
    }

    public List<MyProfilePopularReviews> getMyPopularReviewes(long loginId, long userId, Filter<MyProfilePopularReviews> filter) throws SQLException, QueryBuilderException {
        SelectBuilder selectBuilder = QueryBuilder.select(MyProfilePopularReviews.class)
                .where(ReviewsModel._USER_ID).isEqual(userId);
        if (filter != null) {
            if (!StringUtility.isNullOrWhiteSpace(filter.getParam().getReviewerName())) {
                selectBuilder.filter(filter);
            }
        }
        SelectResult selectResult = selectBuilder.getResult(DatabasePool.getConnection());
        List<MyProfilePopularReviews> popularReviews = selectResult.executeItems(MyProfilePopularReviews.class);
        final LikesFacade likesFacade = new LikesFacade();
        final CompaniesoutletsFacade companiesoutletsModel = new CompaniesoutletsFacade();
        for (MyProfilePopularReviews popularReview : popularReviews) {
            LikesModel lk = likesFacade.findByReviewUser(loginId, popularReview.getId());
            if (lk != null) {
                popularReview.setLike(true);
            } else {
                popularReview.setLike(false);
            }
            List<UserCanView> usersCanView = new ArrayList<>();
            List<UsersModel> userOwnerPic = companiesoutletsModel.findOwnerAndPIC(popularReview.getAsCompanyID());
            for (UsersModel anUserOwnerPic : userOwnerPic) {
                UserCanView ucv = new UserCanView();
                ucv.setId(anUserOwnerPic.getId());
                usersCanView.add(ucv);
            }
            popularReview.setUsersCanView(usersCanView);
        }
        return popularReviews;
    }

    public List<CompanyPersonReviewModel> getMyCompanyPersonReviews(Filter<CompanyPersonReviewModel> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(CompanyPersonReviewModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(CompanyPersonReviewModel.class);
    }

    private boolean updateRating(UsersModel um) throws QueryBuilderException, SQLException, IOException {
        float rat = calculateRating(um);
        um.setAvgRating(rat);
        return new UsersFacade().update(um);
    }

    private float calculateRating(UsersModel usersModel) throws SQLException, QueryBuilderException {
        String query = "select avg(ratingScore) as ratingScore from  reviews where userId=" + usersModel.getId();
        List<Object> reviewsModels = new ArrayList<>();
        try (final ResultBuilder result = QueryBuilder.selectRaw(DatabasePool.getConnection(), query, reviewsModels)) {
            if (!result.moveNext()) return 0;
            else return (float) result.getItem(ReviewsModel.class).getRatingScore();
        }
    }
}