package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.MenusModel;
import id.code.nikku.filter.MenusFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class MenusFacade {
    public boolean insert(MenusModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(MenusModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final MenusModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<MenusModel> getAll(Filter<MenusFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MenusModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(MenusModel.class);
    }

    public MenusModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MenusModel.class)
                .where(MenusModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(MenusModel.class);
    }
}