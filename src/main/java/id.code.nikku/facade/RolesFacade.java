package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.RolesModel;
import id.code.nikku.filter.RolesFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class RolesFacade {
    public boolean insert(RolesModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(RolesModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final RolesModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<RolesModel> getAll(Filter<RolesFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RolesModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(RolesModel.class);
    }

    public RolesModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(RolesModel.class)
                .where(RolesModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(RolesModel.class);
    }
}