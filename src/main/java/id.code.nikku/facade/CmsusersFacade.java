package id.code.nikku.facade;

import id.code.database.filter.Filter;
import id.code.component.DatabasePool;
import id.code.database.builder.*;

import id.code.nikku.model.CmsusersModel;
import id.code.nikku.filter.CmsusersFilter;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class CmsusersFacade {
    public boolean insert(CmsusersModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(CmsusersModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final CmsusersModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<CmsusersModel> getAll(Filter<CmsusersFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(CmsusersModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(CmsusersModel.class);
    }

    public CmsusersModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(CmsusersModel.class)
                .where(CmsusersModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(CmsusersModel.class);
    }
}