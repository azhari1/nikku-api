package id.code.nikku.facade;

import id.code.component.DatabasePool;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.database.builder.UpdateResult;
import id.code.database.filter.Filter;
import id.code.nikku.filter.SkillsFilter;
import id.code.nikku.model.SkillsModel;
import id.code.nikku.model.modify.MyProfileSkills;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Web Api Generator 17/12/2017.
 */

public class SkillsFacade {
    public boolean insert(SkillsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.insert(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean update(SkillsModel value) throws SQLException, QueryBuilderException, IOException {
        final UpdateResult result = QueryBuilder.update(value).execute(DatabasePool.getConnection());
        return result.isModified();
    }

    public boolean delete(long id) throws SQLException, QueryBuilderException, IOException {
        final SkillsModel oldData = this.findById(id);
        final UpdateResult result = oldData == null ? null : QueryBuilder.delete(oldData).execute(DatabasePool.getConnection());
        return result != null && result.isModified();
    }

    public List<SkillsModel> getAll(Filter<SkillsFilter> filter) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(SkillsModel.class)
                .limitOffset(filter)
                .orderBy(filter)
                .filter(filter)
                .getResult(DatabasePool.getConnection())
                .executeItems(SkillsModel.class);
    }

    public SkillsModel findById(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(SkillsModel.class)
                .where(SkillsModel._ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItem(SkillsModel.class);
    }

    public List<MyProfileSkills> getMySkills(long id) throws SQLException, QueryBuilderException {
        return QueryBuilder.select(MyProfileSkills.class)
                .where(SkillsModel._USER_ID).isEqual(id)
                .getResult(DatabasePool.getConnection())
                .executeItems(MyProfileSkills.class);
    }
}