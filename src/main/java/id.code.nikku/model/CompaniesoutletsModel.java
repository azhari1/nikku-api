package id.code.nikku.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = CompaniesoutletsModel.TABLE_NAME)
public class CompaniesoutletsModel extends BaseModel {
    public static final String TABLE_NAME = "companiesoutlets";
	public static final String _ID = "id";
	public static final String _COMPANY_NAME = "companyName";
	public static final String _OUTLET_NAME = "outletName";
	public static final String _SIUP_TDPNUMBER = "SIUP_TDPNumber";
	public static final String _TYPE_OF_BUSINESS_ID = "typeOfBusinessID";
	public static final String _ADDRESS = "address";
	public static final String _PHONE = "phone";
	public static final String _EMAIL = "email";
	public static final String _WEBSITE = "website";
	public static final String _OWNER = "owner";
	public static final String _LOGO = "logo";
	public static final String _DATE_CREATED = "dateCreated";
	public static final String _CREATED_BY = "createdBy";
	public static final String _IS_FEATURED = "isFeatured";
	public static final String _ORDER_LIST = "orderList";
	public static final String _IS_DISABLED = "isDisabled";
	public static final String _TYPE_OF_BUSINESS = "typeOfBussiness";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_COMPANY_NAME)
	@JsonProperty(_COMPANY_NAME)
	@ValidateColumn(_COMPANY_NAME)
	private String companyName;

	@TableColumn(_OUTLET_NAME)
	@JsonProperty(_OUTLET_NAME)
	private String outletName;

	@TableColumn(_SIUP_TDPNUMBER)
	@JsonProperty(_SIUP_TDPNUMBER)
	@ValidateColumn(_SIUP_TDPNUMBER)
	private String siupTdpnumber;

	@TableColumn(_TYPE_OF_BUSINESS_ID)
	@JsonProperty(_TYPE_OF_BUSINESS_ID)
	private Long typeOfBusinessId;

	@TableColumn(_ADDRESS)
	@JsonProperty(_ADDRESS)
	@ValidateColumn(_ADDRESS)
	private String address;

	@TableColumn(_PHONE)
	@JsonProperty(_PHONE)
	@ValidateColumn(_PHONE)
	private String phone;

	@TableColumn(_EMAIL)
	@JsonProperty(_EMAIL)
	@ValidateColumn(_EMAIL)
	private String email;

	@TableColumn(_WEBSITE)
	@JsonProperty(_WEBSITE)
	private String website;

	@TableColumn(_OWNER)
	@JsonProperty(_OWNER)
	@ValidateColumn(_OWNER)
	private long owner;

	@TableColumn(_LOGO)
	@JsonProperty(_LOGO)
	private String logo;

	@TableColumn(_DATE_CREATED)
	@JsonProperty(_DATE_CREATED)
	@ValidateColumn(_DATE_CREATED)
	private long dateCreated;

	@TableColumn(_CREATED_BY)
	@JsonProperty(_CREATED_BY)
	private String createdBy;

	@TableColumn(_IS_FEATURED)
	@JsonProperty(_IS_FEATURED)
	private Integer isFeatured;

	@TableColumn(_ORDER_LIST)
	@JsonProperty(_ORDER_LIST)
	private Integer orderList;

	@TableColumn(_IS_DISABLED)
	@JsonProperty(_IS_DISABLED)
	private Integer isDisabled;

	@JsonProperty(_TYPE_OF_BUSINESS)
	private TypeofbusinessModel typeofbusinessModel;


	public long getId() { return this.id; }
	public String getCompanyName() { return this.companyName; }
	public String getOutletName() { return this.outletName; }
	public String getSiupTdpnumber() { return this.siupTdpnumber; }
	public Long getTypeOfBusinessId() { return this.typeOfBusinessId; }
	public String getAddress() { return this.address; }
	public String getPhone() { return this.phone; }
	public String getEmail() { return this.email; }
	public String getWebsite() { return this.website; }
	public long getOwner() { return this.owner; }
	public String getLogo() { return this.logo; }
	public long getDateCreated() { return this.dateCreated; }
	public String getCreatedBy() { return this.createdBy; }
	public Integer getIsFeatured() { return this.isFeatured; }
	public Integer getOrderList() { return this.orderList; }
	public Integer getIsDisabled() { return this.isDisabled; }
	public TypeofbusinessModel getTypeofbusinessModel() {return this.typeofbusinessModel;}

	public void setId(long id) { this.id = id; }
	public void setCompanyName(String companyName) { this.companyName = companyName; }
	public void setOutletName(String outletName) { this.outletName = outletName; }
	public void setSiupTdpnumber(String siupTdpnumber) { this.siupTdpnumber = siupTdpnumber; }
	public void setTypeOfBusinessId(Long typeOfBusinessId) { this.typeOfBusinessId = typeOfBusinessId; }
	public void setAddress(String address) { this.address = address; }
	public void setPhone(String phone) { this.phone = phone; }
	public void setEmail(String email) { this.email = email; }
	public void setWebsite(String website) { this.website = website; }
	public void setOwner(long owner) { this.owner = owner; }
	public void setLogo(String logo) { this.logo = logo; }
	public void setDateCreated(long dateCreated) { this.dateCreated = dateCreated; }
	public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
	public void setIsFeatured(Integer isFeatured) { this.isFeatured = isFeatured; }
	public void setOrderList(Integer orderList) { this.orderList = orderList; }
	public void setIsDisabled(Integer isDisabled) { this.isDisabled = isDisabled; }
	public void setTypeofbusinessModel(TypeofbusinessModel typeofbusinessModel) {this.typeofbusinessModel=typeofbusinessModel;}

	public JSONObject toJsonObject() throws JSONException {
		JSONObject json = new JSONObject();
		json.put(_ID, this.id);
		json.put(_OWNER, this.owner);
		json.put(_WEBSITE, this.website);
		json.put(_ADDRESS, this.address);
		json.put(_COMPANY_NAME, this.companyName);
		json.put(_ORDER_LIST, this.orderList);
		json.put(_OUTLET_NAME, this.outletName);
		json.put(_TYPE_OF_BUSINESS_ID, this.typeOfBusinessId);
		json.put(_DATE_CREATED, this.dateCreated);
		json.put(_SIUP_TDPNUMBER, this.siupTdpnumber);
		json.put(_PHONE, this.phone);
		json.put(_CREATED_BY, this.createdBy);
		json.put(_LOGO, this.logo);
		json.put(_IS_FEATURED, this.isFeatured);
		json.put(_IS_DISABLED, this.isDisabled);
		json.put(_EMAIL, this.email);
		json.put(_TYPE_OF_BUSINESS, this.typeofbusinessModel.toJsonObject());
		return json;
	}

}