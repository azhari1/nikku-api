package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = UserworkatcompaniesModel.TABLE_NAME)
public class UserworkatcompaniesModel extends BaseModel {
    public static final String TABLE_NAME = "userworkatcompanies";
	public static final String _ID = "id";
	public static final String _USER_ID = "userID";
	public static final String _COMPANY_ID = "companyId";
	public static final String _JOB_POSITION = "jobPosition";
	public static final String _START_DATE = "startDate";
	public static final String _END_DATE = "endDate";
	public static final String _STATUS = "status";
	public static final String _IS_HIDE = "isHide";
	public static final String _HIDE_FROM_RATE = "hideFromRate";
	public static final String _COMPANIES = "companies";
	public static final String _USERS = "users";
	public static final String _COMPANIES_PIC = "companiesPIC";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_USER_ID)
	@JsonProperty(_USER_ID)
	@ValidateColumn(_USER_ID)
	private long userId;

	@TableColumn(_COMPANY_ID)
	@JsonProperty(_COMPANY_ID)
	@ValidateColumn(_COMPANY_ID)
	private long companyId;

	@TableColumn(_JOB_POSITION)
	@JsonProperty(_JOB_POSITION)
	@ValidateColumn(_JOB_POSITION)
	private String jobPosition;

	@TableColumn(_START_DATE)
	@JsonProperty(_START_DATE)
	@ValidateColumn(_START_DATE)
	private long startDate;

	@TableColumn(_END_DATE)
	@JsonProperty(_END_DATE)
	private Long endDate;

	@TableColumn(_STATUS)
	@JsonProperty(_STATUS)
	private int status;

	@TableColumn(_IS_HIDE)
	@JsonProperty(_IS_HIDE)
	private int isHide;

	@TableColumn(_HIDE_FROM_RATE)
	@JsonProperty(_HIDE_FROM_RATE)
	private int hideFromRate;

	@JsonProperty(_COMPANIES)
	private CompaniesoutletsModel companiesoutletsModel;

	@JsonProperty(_USERS)
	private UsersModel usersModel;

	@JsonProperty(_COMPANIES_PIC)
	private CompanyoutletpicsModel companyoutletpicsModel;

	public long getId() { return this.id; }
	public long getUserId() { return this.userId; }
	public long getCompanyId() { return this.companyId; }
	public String getJobPosition() { return this.jobPosition; }
	public long getStartDate() { return this.startDate; }
	public Long getEndDate() { return this.endDate; }
	public int getStatus() { return this.status; }
	public int getIsHide() { return this.isHide; }
	public int getHideFromRate() { return this.hideFromRate; }
	public CompaniesoutletsModel getCompaniesoutletsModel() { return this.companiesoutletsModel; }
	public UsersModel getUsersModel() {return this.usersModel;}
	public CompanyoutletpicsModel getCompanyoutletpicsModel() { return this.companyoutletpicsModel; }

	public void setId(long id) { this.id = id; }
	public void setUserId(long userId) { this.userId = userId; }
	public void setCompanyId(long companyId) { this.companyId = companyId; }
	public void setJobPosition(String jobPosition) { this.jobPosition = jobPosition; }
	public void setStartDate(long startDate) { this.startDate = startDate; }
	public void setEndDate(Long endDate) { this.endDate = endDate; }
	public void setStatus(int status) { this.status = status; }
	public void setIsHide(int isHide) { this.isHide = isHide; }
	public void setHideFromRate(int hideFromRate) { this.hideFromRate = hideFromRate; }
	public void setCompaniesoutletsModel(CompaniesoutletsModel companiesoutletsModel) { this.companiesoutletsModel = companiesoutletsModel;}
	public void setUsersModel(UsersModel usersModel) {this.usersModel = usersModel; }
	public void setCompanyoutletpicsModel(CompanyoutletpicsModel companyoutletpicsModel) {this.companyoutletpicsModel = companyoutletpicsModel; }
}