package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = PrivacysettingsModel.TABLE_NAME)
public class PrivacysettingsModel extends BaseModel {
    public static final String TABLE_NAME = "privacysettings";
	public static final String _USER_ID = "userId";
	public static final String _EMAIL_VISIBLE = "emailVisible";
	public static final String _PHONE_VISIBLE = "phoneVisible";
	public static final String _BIRTH_DATE_VISIBLE = "birthDateVisible";
	public static final String _COMPANY_VISIBLE = "companyVisible";

	@TableColumn(name = _USER_ID, primaryKey = true)
	@JsonProperty(_USER_ID)
	private long userId;

	@TableColumn(_EMAIL_VISIBLE)
	@JsonProperty(_EMAIL_VISIBLE)
	private int emailVisible;

	@TableColumn(_PHONE_VISIBLE)
	@JsonProperty(_PHONE_VISIBLE)
	private int phoneVisible;

	@TableColumn(_BIRTH_DATE_VISIBLE)
	@JsonProperty(_BIRTH_DATE_VISIBLE)
	private int birthDateVisible;

	@TableColumn(_COMPANY_VISIBLE)
	@JsonProperty(_COMPANY_VISIBLE)
	private int companyVisible;


	public long getUserId() { return this.userId; }
	public int getEmailVisible() { return this.emailVisible; }
	public int getPhoneVisible() { return this.phoneVisible; }
	public int getBirthDateVisible() { return this.birthDateVisible; }
	public int getCompanyVisible() { return this.companyVisible; }

	public void setUserId(long userId) { this.userId = userId; }
	public void setEmailVisible(int emailVisible) { this.emailVisible = emailVisible; }
	public void setPhoneVisible(int phoneVisible) { this.phoneVisible = phoneVisible; }
	public void setBirthDateVisible(int birthDateVisible) { this.birthDateVisible = birthDateVisible; }
	public void setCompanyVisible(int companyVisible) { this.companyVisible = companyVisible; }

}