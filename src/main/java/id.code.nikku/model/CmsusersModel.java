package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = CmsusersModel.TABLE_NAME)
public class CmsusersModel extends BaseModel {
    public static final String TABLE_NAME = "cmsusers";
	public static final String _ID = "id";
	public static final String _ROLE_ID = "roleId";
	public static final String _USER_NAME = "userName";
	public static final String _PASSWORD = "password";
	public static final String _PASSWORD_SALT = "passwordSalt";
	public static final String _NAME = "name";
	public static final String _IS_ACTIVE = "isActive";
	public static final String _CREATED_BY = "createdBy";
	public static final String _CREATED_DATE = "createdDate";
	public static final String _MODIFIED_BY = "modifiedBy";
	public static final String _MODIFIED_DATE = "modifiedDate";
	public static final String _LAS_LOGIN = "lasLogin";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_ROLE_ID)
	@JsonProperty(_ROLE_ID)
	@ValidateColumn(_ROLE_ID)
	private long roleId;

	@TableColumn(_USER_NAME)
	@JsonProperty(_USER_NAME)
	private String userName;

	@TableColumn(_PASSWORD)
	@JsonProperty(_PASSWORD)
	@ValidateColumn(_PASSWORD)
	private String password;

	@TableColumn(_PASSWORD_SALT)
	@JsonProperty(_PASSWORD_SALT)
	@ValidateColumn(_PASSWORD_SALT)
	private String passwordSalt;

	@TableColumn(_NAME)
	@JsonProperty(_NAME)
	@ValidateColumn(_NAME)
	private String name;

	@TableColumn(_IS_ACTIVE)
	@JsonProperty(_IS_ACTIVE)
	@ValidateColumn(_IS_ACTIVE)
	private int isActive;

	@TableColumn(_CREATED_BY)
	@JsonProperty(_CREATED_BY)
	private String createdBy;

	@TableColumn(_CREATED_DATE)
	@JsonProperty(_CREATED_DATE)
	private Long createdDate;

	@TableColumn(_MODIFIED_BY)
	@JsonProperty(_MODIFIED_BY)
	private String modifiedBy;

	@TableColumn(_MODIFIED_DATE)
	@JsonProperty(_MODIFIED_DATE)
	private Long modifiedDate;

	@TableColumn(_LAS_LOGIN)
	@JsonProperty(_LAS_LOGIN)
	private Long lasLogin;


	public long getId() { return this.id; }
	public long getRoleId() { return this.roleId; }
	public String getUserName() { return this.userName; }
	public String getPassword() { return this.password; }
	public String getPasswordSalt() { return this.passwordSalt; }
	public String getName() { return this.name; }
	public int getIsActive() { return this.isActive; }
	public String getCreatedBy() { return this.createdBy; }
	public Long getCreatedDate() { return this.createdDate; }
	public String getModifiedBy() { return this.modifiedBy; }
	public Long getModifiedDate() { return this.modifiedDate; }
	public Long getLasLogin() { return this.lasLogin; }

	public void setId(long id) { this.id = id; }
	public void setRoleId(long roleId) { this.roleId = roleId; }
	public void setUserName(String userName) { this.userName = userName; }
	public void setPassword(String password) { this.password = password; }
	public void setPasswordSalt(String passwordSalt) { this.passwordSalt = passwordSalt; }
	public void setName(String name) { this.name = name; }
	public void setIsActive(int isActive) { this.isActive = isActive; }
	public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
	public void setCreatedDate(Long createdDate) { this.createdDate = createdDate; }
	public void setModifiedBy(String modifiedBy) { this.modifiedBy = modifiedBy; }
	public void setModifiedDate(Long modifiedDate) { this.modifiedDate = modifiedDate; }
	public void setLasLogin(Long lasLogin) { this.lasLogin = lasLogin; }

}