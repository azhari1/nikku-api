package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = CompanyoutletpicsModel.TABLE_NAME)
public class CompanyoutletpicsModel extends BaseModel {
    public static final String TABLE_NAME = "companyoutletpics";
	public static final String _ID = "id";
	public static final String _COMPANY_ID = "companyId";
	public static final String _USER_ID = "userId";
	public static final String _CREATED_DATE = "createdDate";
	public static final String _STATUS = "status";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_COMPANY_ID)
	@JsonProperty(_COMPANY_ID)
	@ValidateColumn(_COMPANY_ID)
	private long companyId;

	@TableColumn(_USER_ID)
	@JsonProperty(_USER_ID)
	@ValidateColumn(_USER_ID)
	private long userId;

	@TableColumn(_CREATED_DATE)
	@JsonProperty(_CREATED_DATE)
	@ValidateColumn(_CREATED_DATE)
	private long createdDate;

	@TableColumn(_STATUS)
	@JsonProperty(_STATUS)
	@ValidateColumn(_STATUS)
	private int status;


	public long getId() { return this.id; }
	public long getCompanyId() { return this.companyId; }
	public long getUserId() { return this.userId; }
	public long getCreatedDate() { return this.createdDate; }
	public int getStatus() { return this.status; }

	public void setId(long id) { this.id = id; }
	public void setCompanyId(long companyId) { this.companyId = companyId; }
	public void setUserId(long userId) { this.userId = userId; }
	public void setCreatedDate(long createdDate) { this.createdDate = createdDate; }
	public void setStatus(int status) { this.status = status; }

}