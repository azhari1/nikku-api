package id.code.nikku.model;

import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public abstract class BaseModel {
    public static final String _REQUEST_ID = "request_id";

    @JsonProperty(value = _REQUEST_ID)
    @ValidateColumn(name = _REQUEST_ID)
    public long requestId;
    public long getRequestId() { return this.requestId; }
    public void setRequestId(long requestId) { this.requestId = requestId; }
}