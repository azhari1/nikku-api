package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = RolesModel.TABLE_NAME)
public class RolesModel extends BaseModel {
    public static final String TABLE_NAME = "roles";
	public static final String _ID = "id";
	public static final String _ROLE_NAME = "roleName";
	public static final String _IS_SUPER_ADMIN = "isSuperAdmin";
	public static final String _CREATED_DATE = "createdDate";
	public static final String _CREATED_BY = "createdBy";
	public static final String _MODIFIED_BY = "modifiedBy";
	public static final String _MODIFIED_DATE = "modifiedDate";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_ROLE_NAME)
	@JsonProperty(_ROLE_NAME)
	@ValidateColumn(_ROLE_NAME)
	private String roleName;

	@TableColumn(_IS_SUPER_ADMIN)
	@JsonProperty(_IS_SUPER_ADMIN)
	@ValidateColumn(_IS_SUPER_ADMIN)
	private int isSuperAdmin;

	@TableColumn(_CREATED_DATE)
	@JsonProperty(_CREATED_DATE)
	private Long createdDate;

	@TableColumn(_CREATED_BY)
	@JsonProperty(_CREATED_BY)
	private String createdBy;

	@TableColumn(_MODIFIED_BY)
	@JsonProperty(_MODIFIED_BY)
	private String modifiedBy;

	@TableColumn(_MODIFIED_DATE)
	@JsonProperty(_MODIFIED_DATE)
	private Long modifiedDate;


	public long getId() { return this.id; }
	public String getRoleName() { return this.roleName; }
	public int getIsSuperAdmin() { return this.isSuperAdmin; }
	public Long getCreatedDate() { return this.createdDate; }
	public String getCreatedBy() { return this.createdBy; }
	public String getModifiedBy() { return this.modifiedBy; }
	public Long getModifiedDate() { return this.modifiedDate; }

	public void setId(long id) { this.id = id; }
	public void setRoleName(String roleName) { this.roleName = roleName; }
	public void setIsSuperAdmin(int isSuperAdmin) { this.isSuperAdmin = isSuperAdmin; }
	public void setCreatedDate(Long createdDate) { this.createdDate = createdDate; }
	public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
	public void setModifiedBy(String modifiedBy) { this.modifiedBy = modifiedBy; }
	public void setModifiedDate(Long modifiedDate) { this.modifiedDate = modifiedDate; }

}