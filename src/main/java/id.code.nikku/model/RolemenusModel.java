package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = RolemenusModel.TABLE_NAME)
public class RolemenusModel extends BaseModel {
    public static final String TABLE_NAME = "rolemenus";
	public static final String _ID = "id";
	public static final String _ROLES_ID = "rolesId";
	public static final String _MENUS_ID = "menusId";
	public static final String _VIEW = "view";
	public static final String _ADD = "add";
	public static final String _UPDATE = "update";
	public static final String _DELETE = "delete";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_ROLES_ID)
	@JsonProperty(_ROLES_ID)
	@ValidateColumn(_ROLES_ID)
	private long rolesId;

	@TableColumn(_MENUS_ID)
	@JsonProperty(_MENUS_ID)
	@ValidateColumn(_MENUS_ID)
	private long menusId;

	@TableColumn(_VIEW)
	@JsonProperty(_VIEW)
	@ValidateColumn(_VIEW)
	private int view;

	@TableColumn(_ADD)
	@JsonProperty(_ADD)
	@ValidateColumn(_ADD)
	private int add;

	@TableColumn(_UPDATE)
	@JsonProperty(_UPDATE)
	@ValidateColumn(_UPDATE)
	private int update;

	@TableColumn(_DELETE)
	@JsonProperty(_DELETE)
	@ValidateColumn(_DELETE)
	private int delete;


	public long getId() { return this.id; }
	public long getRolesId() { return this.rolesId; }
	public long getMenusId() { return this.menusId; }
	public int getView() { return this.view; }
	public int getAdd() { return this.add; }
	public int getUpdate() { return this.update; }
	public int getDelete() { return this.delete; }

	public void setId(long id) { this.id = id; }
	public void setRolesId(long rolesId) { this.rolesId = rolesId; }
	public void setMenusId(long menusId) { this.menusId = menusId; }
	public void setView(int view) { this.view = view; }
	public void setAdd(int add) { this.add = add; }
	public void setUpdate(int update) { this.update = update; }
	public void setDelete(int delete) { this.delete = delete; }

}