package id.code.nikku.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

@Table(name = FaqModel.TABLE_NAME)
public class FaqModel {

    public static final String TABLE_NAME = "faq";
    public static final String _ID = "id";
    public static final String _TITLE = "title";
    public static final String _DESCRIPTION = "description";
    public static final String _IMAGE = "image";

    @TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(_TITLE)
    @JsonProperty(_TITLE)
    @ValidateColumn(_TITLE)
    private String title;

    @TableColumn(_DESCRIPTION)
    @JsonProperty(_DESCRIPTION)
    @ValidateColumn(_DESCRIPTION)
    private String description;

    @TableColumn(_IMAGE)
    @JsonProperty(_IMAGE)
    private String image;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
