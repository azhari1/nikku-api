package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = MenusModel.TABLE_NAME)
public class MenusModel extends BaseModel {
    public static final String TABLE_NAME = "menus";
	public static final String _ID = "id";
	public static final String _MENU_PARENT = "menuParent";
	public static final String _MENU_NAME = "menuName";
	public static final String _URL = "url";
	public static final String _POSITION = "position";
	public static final String _IS_ACTIVE = "isActive";
	public static final String _ICON_PATH = "iconPath";
	public static final String _CREATED_BY = "createdBy";
	public static final String _CREATED_DATE = "createdDate";
	public static final String _MODIFIED_BY = "modifiedBy";
	public static final String _MODIFIED_DATE = "modifiedDate";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_MENU_PARENT)
	@JsonProperty(_MENU_PARENT)
	private Long menuParent;

	@TableColumn(_MENU_NAME)
	@JsonProperty(_MENU_NAME)
	@ValidateColumn(_MENU_NAME)
	private String menuName;

	@TableColumn(_URL)
	@JsonProperty(_URL)
	@ValidateColumn(_URL)
	private String url;

	@TableColumn(_POSITION)
	@JsonProperty(_POSITION)
	private Integer position;

	@TableColumn(_IS_ACTIVE)
	@JsonProperty(_IS_ACTIVE)
	@ValidateColumn(_IS_ACTIVE)
	private int isActive;

	@TableColumn(_ICON_PATH)
	@JsonProperty(_ICON_PATH)
	@ValidateColumn(_ICON_PATH)
	private String iconPath;

	@TableColumn(_CREATED_BY)
	@JsonProperty(_CREATED_BY)
	private String createdBy;

	@TableColumn(_CREATED_DATE)
	@JsonProperty(_CREATED_DATE)
	private Long createdDate;

	@TableColumn(_MODIFIED_BY)
	@JsonProperty(_MODIFIED_BY)
	private String modifiedBy;

	@TableColumn(_MODIFIED_DATE)
	@JsonProperty(_MODIFIED_DATE)
	private Long modifiedDate;


	public long getId() { return this.id; }
	public Long getMenuParent() { return this.menuParent; }
	public String getMenuName() { return this.menuName; }
	public String getUrl() { return this.url; }
	public Integer getPosition() { return this.position; }
	public int getIsActive() { return this.isActive; }
	public String getIconPath() { return this.iconPath; }
	public String getCreatedBy() { return this.createdBy; }
	public Long getCreatedDate() { return this.createdDate; }
	public String getModifiedBy() { return this.modifiedBy; }
	public Long getModifiedDate() { return this.modifiedDate; }

	public void setId(long id) { this.id = id; }
	public void setMenuParent(Long menuParent) { this.menuParent = menuParent; }
	public void setMenuName(String menuName) { this.menuName = menuName; }
	public void setUrl(String url) { this.url = url; }
	public void setPosition(Integer position) { this.position = position; }
	public void setIsActive(int isActive) { this.isActive = isActive; }
	public void setIconPath(String iconPath) { this.iconPath = iconPath; }
	public void setCreatedBy(String createdBy) { this.createdBy = createdBy; }
	public void setCreatedDate(Long createdDate) { this.createdDate = createdDate; }
	public void setModifiedBy(String modifiedBy) { this.modifiedBy = modifiedBy; }
	public void setModifiedDate(Long modifiedDate) { this.modifiedDate = modifiedDate; }

}