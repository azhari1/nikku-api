package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = ReviewsModel.TABLE_NAME)
public class ReviewsModel extends BaseModel {
    public static final String TABLE_NAME = "reviews";
	public static final String _ID = "id";
	public static final String _USER_ID = "userId";
	public static final String _REVIEWER_ID = "reviewerId";
	public static final String _REVIEW_TYPE = "reviewType";
	public static final String _AS_COMPANY_ID = "asCompanyId";
	public static final String _DATE = "date";
	public static final String _TOTAL_LIKES = "totalLikes";
	public static final String _TOTAL_VIEWS = "totalViews";
	public static final String _RATING_SCORE = "ratingScore";
	public static final String _QUESTIONNAIRE1 = "questionnaire1";
	public static final String _QUESTIONNAIRE2 = "questionnaire2";
	public static final String _QUESTIONNAIRE3 = "questionnaire3";
	public static final String _QUESTIONNAIRE4 = "questionnaire4";
	public static final String _QUESTIONNAIRE5 = "questionnaire5";
	public static final String _COMMENT = "comment";
	public static final String _PHOTO = "photo";
	public static final String _URL = "url";
	public static final String _IS_HIDDEN = "isHidden";
	public static final String _USERS = "users";
	public static final String _REVIEWERS = "reviewers";
	public static final String _LIKES = "likes";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_USER_ID)
	@JsonProperty(_USER_ID)
	@ValidateColumn(_USER_ID)
	private long userId;

	@TableColumn(_REVIEWER_ID)
	@JsonProperty(_REVIEWER_ID)
	@ValidateColumn(_REVIEWER_ID)
	private long reviewerId;

	@TableColumn(_REVIEW_TYPE)
	@JsonProperty(_REVIEW_TYPE)
	@ValidateColumn(_REVIEW_TYPE)
	private int reviewType;

	@TableColumn(_AS_COMPANY_ID)
	@JsonProperty(_AS_COMPANY_ID)
	private Long asCompanyId;

	@TableColumn(_DATE)
	@JsonProperty(_DATE)
	@ValidateColumn(_DATE)
	private long date;

	@TableColumn(_TOTAL_LIKES)
	@JsonProperty(_TOTAL_LIKES)
	private Long totalLikes;

	@TableColumn(_TOTAL_VIEWS)
	@JsonProperty(_TOTAL_VIEWS)
	private Long totalViews;

	@TableColumn(_RATING_SCORE)
	@JsonProperty(_RATING_SCORE)
	@ValidateColumn(_RATING_SCORE)
	private double ratingScore;

	@TableColumn(_QUESTIONNAIRE1)
	@JsonProperty(_QUESTIONNAIRE1)
	private Integer questionnaire1;

	@TableColumn(_QUESTIONNAIRE2)
	@JsonProperty(_QUESTIONNAIRE2)
	private Integer questionnaire2;

	@TableColumn(_QUESTIONNAIRE3)
	@JsonProperty(_QUESTIONNAIRE3)
	private Integer questionnaire3;

	@TableColumn(_QUESTIONNAIRE4)
	@JsonProperty(_QUESTIONNAIRE4)
	private Integer questionnaire4;

	@TableColumn(_QUESTIONNAIRE5)
	@JsonProperty(_QUESTIONNAIRE5)
	private Integer questionnaire5;

	@TableColumn(_COMMENT)
	@JsonProperty(_COMMENT)
	private String comment;

	@TableColumn(_PHOTO)
	@JsonProperty(_PHOTO)
	private String photo;

	@TableColumn(_URL)
	@JsonProperty(_URL)
	private String url;

	@TableColumn(_IS_HIDDEN)
	@JsonProperty(_IS_HIDDEN)
	@ValidateColumn(_IS_HIDDEN)
	private int isHidden;

	@JsonProperty(_USERS)
	private UsersModel usersModel;

	@JsonProperty(_REVIEWERS)
	private UsersModel usersReviewers;

	@JsonProperty(_LIKES)
	private LikesModel likes;

	public long getId() { return this.id; }
	public long getUserId() { return this.userId; }
	public long getReviewerId() { return this.reviewerId; }
	public int getReviewType() { return this.reviewType; }
	public Long getAsCompanyId() { return this.asCompanyId; }
	public long getDate() { return this.date; }
	public Long getTotalLikes() { return this.totalLikes; }
	public Long getTotalViews() { return this.totalViews; }
	public double getRatingScore() { return this.ratingScore; }
	public Integer getQuestionnaire1() { return this.questionnaire1; }
	public Integer getQuestionnaire2() { return this.questionnaire2; }
	public Integer getQuestionnaire3() { return this.questionnaire3; }
	public Integer getQuestionnaire4() { return this.questionnaire4; }
	public Integer getQuestionnaire5() { return this.questionnaire5; }
	public String getComment() { return this.comment; }
	public String getPhoto() { return this.photo; }
	public String getUrl() { return this.url; }
	public int getIsHidden() { return this.isHidden; }
	public UsersModel getUsersModel() {
		return this.usersModel;
	}
	public UsersModel getUsersReviewers() {
		return this.usersReviewers;
	}
	public LikesModel getLikes() { return this.likes; }

	public void setId(long id) { this.id = id; }
	public void setUserId(long userId) { this.userId = userId; }
	public void setReviewerId(long reviewerId) { this.reviewerId = reviewerId; }
	public void setReviewType(int reviewType) { this.reviewType = reviewType; }
	public void setAsCompanyId(Long asCompanyId) { this.asCompanyId = asCompanyId; }
	public void setDate(long date) { this.date = date; }
	public void setTotalLikes(Long totalLikes) { this.totalLikes = totalLikes; }
	public void setTotalViews(Long totalViews) { this.totalViews = totalViews; }
	public void setRatingScore(double ratingScore) { this.ratingScore = ratingScore; }
	public void setQuestionnaire1(Integer questionnaire1) { this.questionnaire1 = questionnaire1; }
	public void setQuestionnaire2(Integer questionnaire2) { this.questionnaire2 = questionnaire2; }
	public void setQuestionnaire3(Integer questionnaire3) { this.questionnaire3 = questionnaire3; }
	public void setQuestionnaire4(Integer questionnaire4) { this.questionnaire4 = questionnaire4; }
	public void setQuestionnaire5(Integer questionnaire5) { this.questionnaire5 = questionnaire5; }
	public void setComment(String comment) { this.comment = comment; }
	public void setPhoto(String photo) { this.photo = photo; }
	public void setUrl(String url) { this.url = url; }
	public void setIsHidden(int isHidden) { this.isHidden = isHidden; }
	public void setUsersModel(UsersModel usersModel) {
		this.usersModel = usersModel;
	}
	public void setUsersReviewers(UsersModel usersReviewers) {
		this.usersReviewers = usersReviewers;
	}
	public void setLikes(LikesModel likes) { this.likes = likes; }
}