package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = ProvinceModel.TABLE_NAME)
public class ProvinceModel extends BaseModel {
    public static final String TABLE_NAME = "province";
	public static final String _ID = "id";
	public static final String _PROVINCE_NAME = "provinceName";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_PROVINCE_NAME)
	@JsonProperty(_PROVINCE_NAME)
	@ValidateColumn(_PROVINCE_NAME)
	private String provinceName;


	public long getId() { return this.id; }
	public String getProvinceName() { return this.provinceName; }

	public void setId(long id) { this.id = id; }
	public void setProvinceName(String provinceName) { this.provinceName = provinceName; }

}