package id.code.nikku.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class HomeCompanyDetails extends BaseModel{

    public static final String _JOINED = "joined";
    public static final String _COMPANY = "company";
    public static final String _USERS = "users";

    @JsonProperty(_JOINED)
    private boolean isJoined;

    @JsonProperty(_COMPANY)
    private CompaniesoutletsModel companiesoutletsModel;

    @JsonProperty(_USERS)
    private List<UsersModel> usersModels;

    public boolean isJoined() {
        return isJoined;
    }

    public void setJoined(boolean joined) {
        isJoined = joined;
    }

    public CompaniesoutletsModel getCompaniesoutletsModel() {
        return companiesoutletsModel;
    }

    public void setCompaniesoutletsModel(CompaniesoutletsModel companiesoutletsModel) {
        this.companiesoutletsModel = companiesoutletsModel;
    }

    public List<UsersModel> getUsersModels() {
        return usersModels;
    }

    public void setUsersModels(List<UsersModel> usersModels) {
        this.usersModels = usersModels;
    }
}
