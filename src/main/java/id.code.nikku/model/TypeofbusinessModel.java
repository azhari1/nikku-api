package id.code.nikku.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = TypeofbusinessModel.TABLE_NAME)
public class TypeofbusinessModel extends BaseModel {
    public static final String TABLE_NAME = "typeofbusiness";
    public static final String _ID = "id";
    public static final String _TYPE_OF_BUSINESS_NAME = "typeOfBusinessName";

    @TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(_TYPE_OF_BUSINESS_NAME)
    @JsonProperty(_TYPE_OF_BUSINESS_NAME)
    @ValidateColumn(_TYPE_OF_BUSINESS_NAME)
    private String typeOfBusinessName;


    public long getId() {
        return this.id;
    }

    public String getTypeOfBusinessName() {
        return this.typeOfBusinessName;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTypeOfBusinessName(String typeOfBusinessName) {
        this.typeOfBusinessName = typeOfBusinessName;
    }

    public JSONObject toJsonObject() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(_ID, this.id);
        json.put(_TYPE_OF_BUSINESS_NAME, this.typeOfBusinessName);
        return json;
    }
}