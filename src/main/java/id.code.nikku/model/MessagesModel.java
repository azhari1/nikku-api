package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = MessagesModel.TABLE_NAME)
public class MessagesModel extends BaseModel {
    public static final String TABLE_NAME = "messages";
	public static final String _ID = "id";
	public static final String _SENDER_ID = "senderId";
	public static final String _RECEIVER_ID = "receiverId";
	public static final String _DATE = "date";
	public static final String _MESSAGE = "message";
	public static final String _IS_READ = "isRead";
	public static final String _SENDER = "sender";
	public static final String _RECEIVER = "receiver";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_SENDER_ID)
	@JsonProperty(_SENDER_ID)
	@ValidateColumn(_SENDER_ID)
	private long senderId;

	@TableColumn(_RECEIVER_ID)
	@JsonProperty(_RECEIVER_ID)
	@ValidateColumn(_RECEIVER_ID)
	private long receiverId;

	@TableColumn(_DATE)
	@JsonProperty(_DATE)
	@ValidateColumn(_DATE)
	private long date;

	@TableColumn(_MESSAGE)
	@JsonProperty(_MESSAGE)
	@ValidateColumn(_MESSAGE)
	private String message;

	@TableColumn(_IS_READ)
	@JsonProperty(_IS_READ)
	private long isRead;

	@JsonProperty(_SENDER)
	private UsersModel sender;

	@JsonProperty(_RECEIVER)
	private UsersModel receiver;

	public long getId() { return this.id; }
	public long getSenderId() { return this.senderId; }
	public long getReceiverId() { return this.receiverId; }
	public long getDate() { return this.date; }
	public String getMessage() { return this.message; }
	public long getIsRead() { return this.isRead; }
	public UsersModel getSender() { return this.sender; }
	public UsersModel getReceiver() { return this.receiver; }

	public void setId(long id) { this.id = id; }
	public void setSenderId(long senderId) { this.senderId = senderId; }
	public void setReceiverId(long receiverId) { this.receiverId = receiverId; }
	public void setDate(long date) { this.date = date; }
	public void setMessage(String message) { this.message = message; }
	public void setIsRead(long isRead) { this.isRead = isRead;}
	public void setSender(UsersModel sender) {this.sender = sender; }
	public void setReceiver(UsersModel receiver) { this.receiver = receiver; }
}