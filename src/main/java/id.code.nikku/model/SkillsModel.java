package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = SkillsModel.TABLE_NAME)
public class SkillsModel extends BaseModel {
    public static final String TABLE_NAME = "skills";
	public static final String _ID = "id";
	public static final String _USER_ID = "userId";
	public static final String _SKILL_NAME = "skillName";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_USER_ID)
	@JsonProperty(_USER_ID)
	@ValidateColumn(_USER_ID)
	private long userId;

	@TableColumn(_SKILL_NAME)
	@JsonProperty(_SKILL_NAME)
	@ValidateColumn(_SKILL_NAME)
	private String skillName;


	public long getId() { return this.id; }
	public long getUserId() { return this.userId; }
	public String getSkillName() { return this.skillName; }

	public void setId(long id) { this.id = id; }
	public void setUserId(long userId) { this.userId = userId; }
	public void setSkillName(String skillName) { this.skillName = skillName; }

}