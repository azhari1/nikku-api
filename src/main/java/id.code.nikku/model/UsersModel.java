package id.code.nikku.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = UsersModel.TABLE_NAME)
public class UsersModel extends BaseModel {
    public static final String TABLE_NAME = "users";
	public static final String _ACCESS_TOKEN = "access_token";
	public static final String _ID = "id";
	public static final String _NIK = "NIK";
	public static final String _FULL_NAME = "fullName";
	public static final String _EMAIL = "email";
	public static final String _SEX = "sex";
	public static final String _DATE_OF_BIRTH = "dateOfBirth";
	public static final String _PHONE = "phone";
	public static final String _CITY_ID = "cityId";
	public static final String _PROFILE_PICTURE = "profilePicture";
	public static final String _KTP_PHOTO = "ktpPhoto";
	public static final String _JOB_TITLE = "jobTitle";
	public static final String _BIO = "bio";
	public static final String _AVG_RATING = "avgRating";
	public static final String _TOTAL_REVIEWS = "totalReviews";
	public static final String _PASSWORD = "password";
	public static final String _GOOGLE_ID = "googleId";
	public static final String _FCM_TOKEN = "fcmToken";
	public static final String _IS_ACTIVE = "isActive";
	public static final String _DATE_CREATED = "dateCreated";
	public static final String _IS_VERIFIED = "isVerified";
	public static final String _UID = "uid";
	public static final String _CITIES = "cities";
	public static final String _PRIVACY_SETTINGS = "privacy_settings";
	public static final String _SESSION_ID = "session_id";
	public static final String _AVG_RATING_COMPANY = "avgRatingCompany";

	@JsonProperty(_ACCESS_TOKEN)
	private String accessToken;

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_NIK)
	@JsonProperty(_NIK)
	private String nik;

	@TableColumn(_FULL_NAME)
	@JsonProperty(_FULL_NAME)
	@ValidateColumn(_FULL_NAME)
	private String fullName;

	@TableColumn(_EMAIL)
	@JsonProperty(_EMAIL)
	@ValidateColumn(_EMAIL)
	private String email;

	@TableColumn(_SEX)
	@JsonProperty(_SEX)
	@ValidateColumn(_SEX)
	private int sex;

	@TableColumn(_DATE_OF_BIRTH)
	@JsonProperty(_DATE_OF_BIRTH)
	@ValidateColumn(_DATE_OF_BIRTH)
	private long dateOfBirth;

	@TableColumn(_PHONE)
	@JsonProperty(_PHONE)
	private String phone;

	@TableColumn(_CITY_ID)
	@JsonProperty(_CITY_ID)
	@ValidateColumn(_CITY_ID)
	private long cityId;

	@TableColumn(_PROFILE_PICTURE)
	@JsonProperty(_PROFILE_PICTURE)
	@ValidateColumn(_PROFILE_PICTURE)
	private String profilePicture;

	@TableColumn(_KTP_PHOTO)
	@JsonProperty(_KTP_PHOTO)
	@ValidateColumn(_KTP_PHOTO)
	private String ktpPhoto;

	@TableColumn(_JOB_TITLE)
	@JsonProperty(_JOB_TITLE)
	@ValidateColumn(_JOB_TITLE)
	private String jobTitle;

	@TableColumn(_BIO)
	@JsonProperty(_BIO)
	private String bio;

	@TableColumn(_AVG_RATING)
	@JsonProperty(_AVG_RATING)
	@ValidateColumn(_AVG_RATING)
	private double avgRating;

	@TableColumn(_TOTAL_REVIEWS)
	@JsonProperty(_TOTAL_REVIEWS)
	@ValidateColumn(_TOTAL_REVIEWS)
	private int totalReviews;

	@TableColumn(_PASSWORD)
	@JsonProperty(value = _PASSWORD, access = JsonProperty.Access.WRITE_ONLY /* don't show password to client! */)
	private String password;

	@TableColumn(_GOOGLE_ID)
	@JsonProperty(_GOOGLE_ID)
	private String googleId;

	@TableColumn(_FCM_TOKEN)
	@JsonProperty(_FCM_TOKEN)
	private String fcmToken;

	@TableColumn(_IS_ACTIVE)
	@JsonProperty(_IS_ACTIVE)
	@ValidateColumn(_IS_ACTIVE)
	private int isActive;

	@TableColumn(_DATE_CREATED)
	@JsonProperty(_DATE_CREATED)
	@ValidateColumn(_DATE_CREATED)
	private long dateCreated;

	@TableColumn(_IS_VERIFIED)
	@JsonProperty(_IS_VERIFIED)
	@ValidateColumn(_IS_VERIFIED)
	private int isVerified;

	@TableColumn(_UID)
	@JsonProperty(_UID)
	private String uid;

	@TableColumn(_SESSION_ID)
	@JsonProperty(_SESSION_ID)
	private String sessionId;

	@JsonProperty(_AVG_RATING_COMPANY)
	private double avgRatingCompany;

	@JsonProperty(_CITIES)
	private CityModel cityModel;

	@JsonProperty(_PRIVACY_SETTINGS)
	private PrivacysettingsModel privacysettingsModel;

	public String getAccessToken() { return this.accessToken; }
	public long getId() { return this.id; }
	public String getNik() { return this.nik; }
	public String getFullName() { return this.fullName; }
	public String getEmail() { return this.email; }
	public int getSex() { return this.sex; }
	public long getDateOfBirth() { return this.dateOfBirth; }
	public String getPhone() { return this.phone; }
	public long getCityId() { return this.cityId; }
	public String getProfilePicture() { return this.profilePicture; }
	public String getKtpPhoto() { return this.ktpPhoto; }
	public String getJobTitle() { return this.jobTitle; }
	public String getBio() { return this.bio; }
	public double getAvgRating() { return this.avgRating; }
	public int getTotalReviews() { return this.totalReviews; }
	public String getPassword() { return this.password; }
	public String getGoogleId() { return this.googleId; }
	public String getFcmToken() { return this.fcmToken; }
	public int getIsActive() { return this.isActive; }
	public long getDateCreated() { return this.dateCreated; }
	public int getIsVerified() { return this.isVerified; }
	public String getUid() { return this.uid; }
	public CityModel getCityModel() { return this.cityModel; }
	public PrivacysettingsModel getPrivacysettingsModel() {return this.privacysettingsModel; }

	public String getSessionId() {
		return sessionId;
	}

	public void setAccessToken(String accessToken) { this.accessToken = accessToken; }
	public void setId(long id) { this.id = id; }
	public void setNik(String nik) { this.nik = nik; }
	public void setFullName(String fullName) { this.fullName = fullName; }
	public void setEmail(String email) { this.email = email; }
	public void setSex(int sex) { this.sex = sex; }
	public void setDateOfBirth(long dateOfBirth) { this.dateOfBirth = dateOfBirth; }
	public void setPhone(String phone) { this.phone = phone; }
	public void setCityId(long cityId) { this.cityId = cityId; }
	public void setProfilePicture(String profilePicture) { this.profilePicture = profilePicture; }
	public void setKtpPhoto(String ktpPhoto) { this.ktpPhoto = ktpPhoto; }
	public void setJobTitle(String jobTitle) { this.jobTitle = jobTitle; }
	public void setBio(String bio) { this.bio = bio; }
	public void setAvgRating(double avgRating) { this.avgRating = avgRating; }
	public void setTotalReviews(int totalReviews) { this.totalReviews = totalReviews; }
	public void setPassword(String password) { this.password = password; }
	public void setGoogleId(String googleId) { this.googleId = googleId; }
	public void setFcmToken(String fcmToken) { this.fcmToken = fcmToken; }
	public void setIsActive(int isActive) { this.isActive = isActive; }
	public void setDateCreated(long dateCreated) { this.dateCreated = dateCreated; }
	public void setIsVerified(int isVerified) { this.isVerified = isVerified; }
	public void setUid(String uid) { this.uid = uid; }
	public void setCityModel(CityModel cityModel) { this.cityModel=cityModel;}
	public void setPrivacysettingsModel(PrivacysettingsModel privacysettingsModel) {this.privacysettingsModel = privacysettingsModel;}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public JSONObject toJsonObject() throws JSONException {
		JSONObject json = new JSONObject();
		json.put(_ID, this.id);
		json.put(_GOOGLE_ID, this.googleId);
		json.put(_SEX, this.sex);
		json.put(_JOB_TITLE, this.jobTitle);
		json.put(_BIO, this.bio);
		json.put(_FULL_NAME, this.fullName);
		json.put(_DATE_OF_BIRTH, this.dateOfBirth);
		json.put(_CITY_ID, this.cityId);
		json.put(_KTP_PHOTO, this.ktpPhoto);
		json.put(_IS_ACTIVE, this.isActive);
		json.put(_IS_VERIFIED, this.isVerified);
		json.put(_NIK, this.nik);
		json.put(_PROFILE_PICTURE, this.profilePicture);
		json.put(_TOTAL_REVIEWS, this.totalReviews);
		json.put(_DATE_CREATED, this.dateCreated);
		json.put(_PHONE, this.phone);
		json.put(_AVG_RATING, this.avgRating);
		json.put(_PASSWORD, this.password);
		json.put(_FCM_TOKEN, this.fcmToken);
		json.put(_EMAIL, this.email);
		json.put(_UID, this.uid);
		json.put(_CITIES, this.cityModel);
		json.put(_PRIVACY_SETTINGS, this.privacysettingsModel);
		return json;
	}

	public double getAvgRatingCompany() {
		return avgRatingCompany;
	}

	public void setAvgRatingCompany(double avgRatingCompany) {
		this.avgRatingCompany = avgRatingCompany;
	}
}