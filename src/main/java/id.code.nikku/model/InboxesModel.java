package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = InboxesModel.TABLE_NAME)
public class InboxesModel extends BaseModel {
    public static final String TABLE_NAME = "inboxes";
    public static final String _ID = "id";
    public static final String _USER_ID = "userId";
    public static final String _INBOX_TYPE = "inboxType";
    public static final String _REFERENCE_ID = "referenceId";
    public static final String _MESSAGE = "message";
    public static final String _DATE = "date";
    public static final String _IS_READ = "isRead";
    public static final String _USERS = "users";
    public static final String _COMPANIES = "companies";

    @TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(_USER_ID)
    @JsonProperty(_USER_ID)
    @ValidateColumn(_USER_ID)
    private long userId;

    @TableColumn(_INBOX_TYPE)
    @JsonProperty(_INBOX_TYPE)
    @ValidateColumn(_INBOX_TYPE)
    private int inboxType;

    @TableColumn(_REFERENCE_ID)
    @JsonProperty(_REFERENCE_ID)
    @ValidateColumn(_REFERENCE_ID)
    private long referenceId;

    @TableColumn(_MESSAGE)
    @JsonProperty(_MESSAGE)
    private String message;

    @TableColumn(_DATE)
    @JsonProperty(_DATE)
    @ValidateColumn(_DATE)
    private long date;

    @TableColumn(_IS_READ)
    @JsonProperty(_IS_READ)
    private long isRead;

    @JsonProperty(_USERS)
    private UsersModel usersModel;

    @JsonProperty(_COMPANIES)
    private CompaniesoutletsModel companiesoutletsModel;

    public long getId() {
        return this.id;
    }

    public long getUserId() {
        return this.userId;
    }

    public int getInboxType() {
        return this.inboxType;
    }

    public long getReferenceId() {
        return this.referenceId;
    }

    public String getMessage() {
        return this.message;
    }

    public long getDate() {
        return this.date;
    }

    public long getIsRead() {return this.isRead;}

    public UsersModel getUsersModel() {
        return this.usersModel;
    }

    public CompaniesoutletsModel getCompaniesoutletsModel() {
        return companiesoutletsModel;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setInboxType(int inboxType) {
        this.inboxType = inboxType;
    }

    public void setReferenceId(long referenceId) {
        this.referenceId = referenceId;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public void setIsRead(long isRead) {this.isRead = isRead;}
    public void setUsersModel(UsersModel usersModel) {this.usersModel=usersModel;}

    public void setCompaniesoutletsModel(CompaniesoutletsModel companiesoutletsModel) {
        this.companiesoutletsModel = companiesoutletsModel;
    }

    public JSONObject toJsonObject() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(_ID, this.id);
        json.put(_USER_ID, this.userId);
        json.put(_INBOX_TYPE, this.inboxType);
        json.put(_MESSAGE, this.message);
        json.put(_REFERENCE_ID, this.referenceId);
        json.put(_DATE, this.date);
        json.put(_IS_READ, this.isRead);
        return json;
    }
}