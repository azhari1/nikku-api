package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.nikku.model.CompaniesoutletsModel;
import id.code.nikku.model.UserworkatcompaniesModel;

@Table(name = MyProfileWorkingExperiences.TABLE_NAME)
public class MyProfileWorkingExperiences {

    public static final String TABLE_NAME = "myprofileworkingexperiences";

    @TableColumn(name = UserworkatcompaniesModel._ID, primaryKey = true)
    @JsonProperty(UserworkatcompaniesModel._ID)
    private long id;

    @TableColumn(name = UserworkatcompaniesModel._USER_ID)
    @JsonProperty(UserworkatcompaniesModel._USER_ID)
    private long userId;

    @TableColumn(name = UserworkatcompaniesModel._COMPANY_ID)
    @JsonProperty(UserworkatcompaniesModel._COMPANY_ID)
    private long companyId;

    @TableColumn(name = CompaniesoutletsModel._LOGO)
    @JsonProperty(CompaniesoutletsModel._LOGO)
    private String logo;

    @TableColumn(name = CompaniesoutletsModel._COMPANY_NAME)
    @JsonProperty(CompaniesoutletsModel._COMPANY_NAME)
    private String companyName;

    @TableColumn(name = CompaniesoutletsModel._OUTLET_NAME)
    @JsonProperty(CompaniesoutletsModel._OUTLET_NAME)
    private String outletName;

    @TableColumn(name = UserworkatcompaniesModel._JOB_POSITION)
    @JsonProperty(UserworkatcompaniesModel._JOB_POSITION)
    private String jobPosition;

    @TableColumn(name = UserworkatcompaniesModel._START_DATE)
    @JsonProperty(UserworkatcompaniesModel._START_DATE)
    private long startDate;

    @TableColumn(name = UserworkatcompaniesModel._END_DATE)
    @JsonProperty(UserworkatcompaniesModel._END_DATE)
    private long endDate;

    @TableColumn(name = UserworkatcompaniesModel._STATUS)
    @JsonProperty(UserworkatcompaniesModel._STATUS)
    private long status;

    @TableColumn(name = UserworkatcompaniesModel._IS_HIDE)
    @JsonProperty(UserworkatcompaniesModel._IS_HIDE)
    private int isHide;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public int getIsHide() {
        return isHide;
    }

    public void setIsHide(int isHide) {
        this.isHide = isHide;
    }
}
