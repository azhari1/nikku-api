package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;
import id.code.nikku.model.MessagesModel;
import id.code.nikku.model.UsersModel;
import org.json.JSONException;
import org.json.JSONObject;

public class MyMessages {
    public static final String _USER_ID = "userId";

    @TableColumn(name = _USER_ID, primaryKey = true)
    @JsonProperty(_USER_ID)
    private long userId;

    @TableColumn(name = UsersModel._PROFILE_PICTURE)
    @JsonProperty(UsersModel._PROFILE_PICTURE)
    private String profilePicture;

    @TableColumn(name = UsersModel._FULL_NAME)
    @JsonProperty(UsersModel._FULL_NAME)
    private String fullName;

    @TableColumn(name = MessagesModel._MESSAGE)
    @JsonProperty(MessagesModel._MESSAGE)
    private String message;

    @TableColumn(name = MessagesModel._DATE)
    @JsonProperty(MessagesModel._DATE)
    private long date;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public JSONObject toJsonObject() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(_USER_ID, this.userId);
        json.put(UsersModel._PROFILE_PICTURE, this.profilePicture);
        json.put(UsersModel._FULL_NAME, this.fullName);
        json.put(MessagesModel._MESSAGE, this.message);
        json.put(MessagesModel._DATE, this.date);
        return json;
    }
}
