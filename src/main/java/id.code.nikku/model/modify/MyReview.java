package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.CompaniesoutletsModel;
import id.code.nikku.model.ReviewsModel;
import id.code.nikku.model.UsersModel;

@Table(name = MyReview.TABLE_NAME)
public class MyReview {
    public static final String TABLE_NAME = "myreview";

    @TableColumn(name = ReviewsModel._ID, primaryKey = true)
    @JsonProperty(ReviewsModel._ID)
    private long id;

    @TableColumn(name = ReviewsModel._USER_ID)
    @JsonProperty(ReviewsModel._USER_ID)
    private long userId;

    @TableColumn(name = UsersModel._PROFILE_PICTURE)
    @JsonProperty(UsersModel._PROFILE_PICTURE)
    private String profilePicture;

    @TableColumn(name = UsersModel._FULL_NAME)
    @JsonProperty(UsersModel._FULL_NAME)
    private String fullName;

    @TableColumn(name = ReviewsModel._REVIEWER_ID)
    @JsonProperty(ReviewsModel._REVIEWER_ID)
    private long reviewerId;

    @TableColumn(name = ReviewsModel._AS_COMPANY_ID)
    @JsonProperty(ReviewsModel._AS_COMPANY_ID)
    @FilterColumn(columnName = ReviewsModel._AS_COMPANY_ID)
    private long asCompanyId;

    @TableColumn(name = CompaniesoutletsModel._COMPANY_NAME)
    @JsonProperty(CompaniesoutletsModel._COMPANY_NAME)
    private String companyName;

    @TableColumn(name = CompaniesoutletsModel._OUTLET_NAME)
    @JsonProperty(CompaniesoutletsModel._OUTLET_NAME)
    private String outletName;

    @TableColumn(name = ReviewsModel._REVIEW_TYPE)
    @JsonProperty(ReviewsModel._REVIEW_TYPE)
    private long reviewType;

    @TableColumn(name = ReviewsModel._COMMENT)
    @JsonProperty(ReviewsModel._COMMENT)
    private String comment;

    @TableColumn(name = ReviewsModel._DATE)
    @JsonProperty(ReviewsModel._DATE)
    private long date;

    @TableColumn(name = ReviewsModel._TOTAL_LIKES)
    @JsonProperty(ReviewsModel._TOTAL_LIKES)
    private long totalLikes;

    @TableColumn(name = ReviewsModel._TOTAL_VIEWS)
    @JsonProperty(ReviewsModel._TOTAL_VIEWS)
    private long totalViews;

    @TableColumn(name = ReviewsModel._RATING_SCORE)
    @JsonProperty(ReviewsModel._RATING_SCORE)
    private long ratingScore;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public long getAsCompanyId() {
        return asCompanyId;
    }

    public void setAsCompanyId(long asCompanyId) {
        this.asCompanyId = asCompanyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public long getReviewType() {
        return reviewType;
    }

    public void setReviewType(long reviewType) {
        this.reviewType = reviewType;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(long totalLikes) {
        this.totalLikes = totalLikes;
    }

    public long getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(long totalViews) {
        this.totalViews = totalViews;
    }

    public long getRatingScore() {
        return ratingScore;
    }

    public void setRatingScore(long ratingScore) {
        this.ratingScore = ratingScore;
    }
}
