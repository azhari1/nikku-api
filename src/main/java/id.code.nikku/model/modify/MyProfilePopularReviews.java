package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.WhereComparator;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.filter.annotation.FilterColumn;

import java.util.List;

import static id.code.nikku.model.ReviewsModel.*;

@Table(name = MyProfilePopularReviews.TABLE_NAME)
public class MyProfilePopularReviews {

    public static final String TABLE_NAME = "myprofilepopularreviews";

    private static final String _REVIEWER_NAME = "reviewerName";
    private static final String _REVIEWER_PROFILE_PICTURE = "reviewerProfilePicture";
    private static final String _LIKE = "like";
    private static final String _USERS_CAN_VIEW = "usersCanView";

    @TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(_USER_ID)
    @JsonProperty(_USER_ID)
    private long userId;

    @TableColumn(_REVIEWER_ID)
    @JsonProperty(_REVIEWER_ID)
    private long reviewerId;

    @TableColumn(_REVIEWER_PROFILE_PICTURE)
    @JsonProperty(_REVIEWER_PROFILE_PICTURE)
    private String reviewerProfilePicture;

    @TableColumn(_REVIEWER_NAME)
    @FilterColumn(value = _REVIEWER_NAME, comparator = WhereComparator.CONTAINS)
    @JsonProperty(_REVIEWER_NAME)
    private String reviewerName;

    @TableColumn(_DATE)
    @JsonProperty(_DATE)
    private long date;

    @TableColumn(_COMMENT)
    @JsonProperty(_COMMENT)
    private String comment;

    @TableColumn(_REVIEW_TYPE)
    @JsonProperty(_REVIEW_TYPE)
    private long reviewType;

    @JsonProperty(_LIKE)
    private boolean like;

    @TableColumn(_TOTAL_LIKES)
    @JsonProperty(_TOTAL_LIKES)
    private long totalLikes;

    @TableColumn(_TOTAL_VIEWS)
    @JsonProperty(_TOTAL_VIEWS)
    private long totalViews;

    @TableColumn(_RATING_SCORE)
    @JsonProperty(_RATING_SCORE)
    private long ratingScore;

    @TableColumn(_AS_COMPANY_ID)
    @JsonProperty(_AS_COMPANY_ID)
    private long asCompanyID;

    @JsonProperty(_USERS_CAN_VIEW)
    private List<UserCanView> usersCanView;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public String getReviewerProfilePicture() {
        return reviewerProfilePicture;
    }

    public void setReviewerProfilePicture(String reviewerProfilePicture) {
        this.reviewerProfilePicture = reviewerProfilePicture;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getReviewType() {
        return reviewType;
    }

    public void setReviewType(long reviewType) {
        this.reviewType = reviewType;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public long getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(long totalLikes) {
        this.totalLikes = totalLikes;
    }

    public long getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(long totalViews) {
        this.totalViews = totalViews;
    }

    public long getRatingScore() {
        return ratingScore;
    }

    public void setRatingScore(long ratingScore) {
        this.ratingScore = ratingScore;
    }

    public long getAsCompanyID() {
        return asCompanyID;
    }

    public void setAsCompanyID(long asCompanyID) {
        this.asCompanyID = asCompanyID;
    }

    public List<UserCanView> getUsersCanView() {
        return usersCanView;
    }

    public void setUsersCanView(List<UserCanView> usersCanView) {
        this.usersCanView = usersCanView;
    }
}
