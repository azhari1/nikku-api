package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.nikku.model.SkillsModel;

import static id.code.nikku.model.SkillsModel.*;

@Table(name = SkillsModel.TABLE_NAME)
public class MyProfileSkills {

    @TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(_USER_ID)
    @JsonProperty(_USER_ID)
    private long userId;

    @TableColumn(_SKILL_NAME)
    @JsonProperty(_SKILL_NAME)
    private String skillName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }
}
