package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.nikku.model.CompaniesoutletsModel;
import id.code.nikku.model.TypeofbusinessModel;

@Table(name = MyCompany.TABLE_NAME)
public class MyCompany {
    public static final String TABLE_NAME = "mycompany";

    @TableColumn(name = CompaniesoutletsModel._ID, primaryKey = true)
    @JsonProperty(CompaniesoutletsModel._ID)
    private long id;

    @TableColumn(name = CompaniesoutletsModel._LOGO)
    @JsonProperty(CompaniesoutletsModel._LOGO)
    private String logo;

    @TableColumn(name = CompaniesoutletsModel._COMPANY_NAME)
    @JsonProperty(CompaniesoutletsModel._COMPANY_NAME)
    private String companyName;

    @TableColumn(name = CompaniesoutletsModel._OUTLET_NAME)
    @JsonProperty(CompaniesoutletsModel._OUTLET_NAME)
    private String outletName;

    @TableColumn(name = TypeofbusinessModel._TYPE_OF_BUSINESS_NAME)
    @JsonProperty(TypeofbusinessModel._TYPE_OF_BUSINESS_NAME)
    private String typeOfBusinessName;

    @TableColumn(name = CompaniesoutletsModel._ADDRESS)
    @JsonProperty(CompaniesoutletsModel._ADDRESS)
    private String address;

    @TableColumn(name = CompaniesoutletsModel._OWNER)
    @JsonProperty(CompaniesoutletsModel._OWNER)
    private long owner;

    @TableColumn(name = CompaniesoutletsModel._CREATED_BY)
    @JsonProperty(CompaniesoutletsModel._CREATED_BY)
    private String createdBy;

    @TableColumn(name = CompaniesoutletsModel._IS_DISABLED)
    private Integer isDisabled;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getTypeOfBusinessName() {
        return typeOfBusinessName;
    }

    public void setTypeOfBusinessName(String typeOfBusinessName) {
        this.typeOfBusinessName = typeOfBusinessName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getOwner() {
        return owner;
    }

    public void setOwner(long owner) {
        this.owner = owner;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(Integer isDisabled) {
        this.isDisabled = isDisabled;
    }
}
