package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import id.code.nikku.model.UsersModel;

import static id.code.nikku.model.UsersModel.*;

@Table(name = UsersModel.TABLE_NAME)
public class CreateAccount {

    @TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(_NIK)
    @JsonProperty(_NIK)
    @ValidateColumn(_NIK)
    private String nik;

    @TableColumn(_PROFILE_PICTURE)
    @JsonProperty(_PROFILE_PICTURE)
    private String profilePicture;

    @TableColumn(_FULL_NAME)
    @JsonProperty(_FULL_NAME)
    @ValidateColumn(_FULL_NAME)
    private String fullName;

    @TableColumn(_KTP_PHOTO)
    @JsonProperty(_KTP_PHOTO)
    @ValidateColumn(_KTP_PHOTO)
    private String ktpPhoto;

    @TableColumn(_EMAIL)
    @JsonProperty(_EMAIL)
    private String email;

    @TableColumn(_SEX)
    @JsonProperty(_SEX)
    private int sex;

    @TableColumn(_DATE_OF_BIRTH)
    @JsonProperty(_DATE_OF_BIRTH)
    private long dateOfBirth;

    @TableColumn(_CITY_ID)
    @JsonProperty(_CITY_ID)
    private long cityId;

    @TableColumn(_IS_ACTIVE)
    @JsonProperty(_IS_ACTIVE)
    private int isActive;

    @TableColumn(_UID)
    @JsonProperty(_UID)
    private String uid;

    @TableColumn(_SESSION_ID)
    @JsonProperty(_SESSION_ID)
    private String sessionId;

    @TableColumn(_DATE_CREATED)
    @JsonProperty(_DATE_CREATED)
    private long dateCreated;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getKtpPhoto() {
        return ktpPhoto;
    }

    public void setKtpPhoto(String ktpPhoto) {
        this.ktpPhoto = ktpPhoto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }
}