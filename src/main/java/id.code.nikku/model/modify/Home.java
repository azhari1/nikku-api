package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Home {

    private static final String _TOP_RATED_PERSON = "top_rated_persons";
    private static final String _FEATURED_COMPANY = "featured_companies";

    @JsonProperty(_TOP_RATED_PERSON)
    private List<TopRatedPerson> topRatedPersons;

    @JsonProperty(_FEATURED_COMPANY)
    private List<FeaturedCompany> featuredCompanies;

    public List<TopRatedPerson> getTopRatedPersons() {
        return topRatedPersons;
    }

    public void setTopRatedPersons(List<TopRatedPerson> topRatedPersons) {
        this.topRatedPersons = topRatedPersons;
    }

    public List<FeaturedCompany> getFeaturedCompanies() {
        return featuredCompanies;
    }

    public void setFeaturedCompanies(List<FeaturedCompany> featuredCompanies) {
        this.featuredCompanies = featuredCompanies;
    }
}
