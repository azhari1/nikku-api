package id.code.nikku.model.modify;


import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.nikku.model.CompaniesoutletsModel;

@Table(name = CompaniesoutletsModel.TABLE_NAME)
public class FeaturedCompany {

    @TableColumn(name = CompaniesoutletsModel._ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(CompaniesoutletsModel._ID)
    private long id;

    @TableColumn(name = CompaniesoutletsModel._LOGO)
    @JsonProperty(CompaniesoutletsModel._LOGO)
    private String logo;

    @TableColumn(name = CompaniesoutletsModel._COMPANY_NAME)
    @JsonProperty(CompaniesoutletsModel._COMPANY_NAME)
    private String companyName;

    @TableColumn(name = CompaniesoutletsModel._OUTLET_NAME)
    @JsonProperty(CompaniesoutletsModel._OUTLET_NAME)
    private String outletName;

    @TableColumn(name = CompaniesoutletsModel._ADDRESS)
    @JsonProperty(CompaniesoutletsModel._ADDRESS)
    private String address;


    @TableColumn(name = CompaniesoutletsModel._IS_FEATURED)
    private Integer isFeatured;

    @TableColumn(name = CompaniesoutletsModel._IS_DISABLED)
    private Integer isDisabled;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(Integer isFeatured) {
        this.isFeatured = isFeatured;
    }

    public Integer getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(Integer isDisabled) {
        this.isDisabled = isDisabled;
    }
}
