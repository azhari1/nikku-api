package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.nikku.model.UsersModel;
import id.code.nikku.model.UserworkatcompaniesModel;

@Table(name = DetailCompanyUsers.TABLE_NAME)
public class DetailCompanyUsers {

    public static final String TABLE_NAME = "detailcompanyusers";

    @TableColumn(name = UserworkatcompaniesModel._ID, primaryKey = true)
    @JsonProperty(UserworkatcompaniesModel._ID)
    private long id;

    @TableColumn(name = UserworkatcompaniesModel._COMPANY_ID)
    @JsonProperty(UserworkatcompaniesModel._COMPANY_ID)
    private long companyId;

    @TableColumn(name = UserworkatcompaniesModel._USER_ID)
    @JsonProperty(UserworkatcompaniesModel._USER_ID)
    private long userID;

    @TableColumn(name = UsersModel._PROFILE_PICTURE)
    @JsonProperty(UsersModel._PROFILE_PICTURE)
    private String profilePicture;

    @TableColumn(name = UsersModel._FULL_NAME)
    @JsonProperty(UsersModel._FULL_NAME)
    private String fullName;

    @TableColumn(name = UserworkatcompaniesModel._JOB_POSITION)
    @JsonProperty(UserworkatcompaniesModel._JOB_POSITION)
    private String jobPosition;

    @TableColumn(name = UserworkatcompaniesModel._STATUS)
    @JsonProperty(UserworkatcompaniesModel._STATUS)
    private int status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
