package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.filter.annotation.FilterColumn;
import id.code.nikku.model.PrivacysettingsModel;
import id.code.nikku.model.UsersModel;

import java.util.List;

public class MyProfile {

    private static final String _FROM_COMPANY = "fromCompany";
    private static final String _IS_PIC = "isPIC";
    private static final String _WORKING_EXPERIENCES = "working_experiences";
    private static final String _SKILLS = "skills";
    private static final String _POPULAR_REVIEWS = "popular_reviews";
    private static final String _PRIVACY_SETTINGS = "privacy_settings";
    private static final String _ALREADY_REVIEWED = "already_reviewed";

    @TableColumn(name = UsersModel._ID, primaryKey = true)
    @JsonProperty(UsersModel._ID)
    @FilterColumn(UsersModel._ID)
    private long id;

    @TableColumn(name = UsersModel._NIK)
    @JsonProperty(UsersModel._NIK)
    private String nik;

    @TableColumn(name = UsersModel._FULL_NAME)
    @JsonProperty(UsersModel._FULL_NAME)
    private String fullName;

    @TableColumn(name = UsersModel._PROFILE_PICTURE)
    @JsonProperty(UsersModel._PROFILE_PICTURE)
    private String profilePicture;

    @TableColumn(name = UsersModel._JOB_TITLE)
    @JsonProperty(UsersModel._JOB_TITLE)
    private String jobTitle;

    @TableColumn(name = UsersModel._BIO)
    @JsonProperty(UsersModel._BIO)
    private String bio;

    @TableColumn(name = UsersModel._PHONE)
    @JsonProperty(UsersModel._PHONE)
    private String phone;

    @TableColumn(name = UsersModel._EMAIL)
    @JsonProperty(UsersModel._EMAIL)
    private String email;

    @TableColumn(name = UsersModel._SEX)
    @JsonProperty(UsersModel._SEX)
    private long sex;

    @TableColumn(name = UsersModel._DATE_OF_BIRTH)
    @JsonProperty(UsersModel._DATE_OF_BIRTH)
    private long dateOfBirth;

    @JsonProperty(_IS_PIC)
    private Integer isPIC;

    @JsonProperty(_FROM_COMPANY)
    private Integer fromCompany;

    @JsonProperty(_ALREADY_REVIEWED)
    private Integer already_reviewed;

    @TableColumn(name = _WORKING_EXPERIENCES)
    @JsonProperty(_WORKING_EXPERIENCES)
    private List<MyProfileWorkingExperiences> workingExperiences;

    @TableColumn(name = _SKILLS)
    @JsonProperty(_SKILLS)
    private List<MyProfileSkills> skills;

    @TableColumn(name = _POPULAR_REVIEWS)
    @JsonProperty(_POPULAR_REVIEWS)
    private List<MyProfilePopularReviews> popularReviews;

    @TableColumn(name = _PRIVACY_SETTINGS)
    @JsonProperty(_PRIVACY_SETTINGS)
    private PrivacysettingsModel privacysettings;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getSex() {
        return sex;
    }

    public void setSex(long sex) {
        this.sex = sex;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<MyProfileWorkingExperiences> getWorkingExperiences() {
        return workingExperiences;
    }

    public void setWorkingExperiences(List<MyProfileWorkingExperiences> workingExperiences) {
        this.workingExperiences = workingExperiences;
    }

    public List<MyProfileSkills> getSkills() {
        return skills;
    }

    public void setSkills(List<MyProfileSkills> skills) {
        this.skills = skills;
    }

    public List<MyProfilePopularReviews> getPopularReviews() {
        return popularReviews;
    }

    public void setPopularReviews(List<MyProfilePopularReviews> popularReviews) {
        this.popularReviews = popularReviews;
    }

    public PrivacysettingsModel getPrivacysettings() {
        return privacysettings;
    }

    public void setPrivacysettings(PrivacysettingsModel privacysettings) {
        this.privacysettings = privacysettings;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getIsPIC() {
        return isPIC;
    }

    public void setIsPIC(Integer isPIC) {
        this.isPIC = isPIC;
    }

    public Integer getFromCompany() {
        return fromCompany;
    }

    public void setFromCompany(Integer fromCompany) {
        this.fromCompany = fromCompany;
    }

    public Integer getAlready_reviewed() {
        return already_reviewed;
    }

    public void setAlready_reviewed(Integer already_reviewed) {
        this.already_reviewed = already_reviewed;
    }
}
