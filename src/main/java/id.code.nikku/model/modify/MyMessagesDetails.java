package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.nikku.model.MessagesModel;
import id.code.nikku.model.UsersModel;

import java.util.List;

public class MyMessagesDetails {

    private static final String _MESSAGES = "messages";

    @JsonProperty(UsersModel._ID)
    private long id;

    @JsonProperty(UsersModel._PROFILE_PICTURE)
    private String profilePicture;

    @JsonProperty(UsersModel._FULL_NAME)
    private String fullName;

    @JsonProperty(_MESSAGES)
    private List<MessagesModel> messagesModels;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<MessagesModel> getMessagesModels() {
        return messagesModels;
    }

    public void setMessagesModels(List<MessagesModel> messagesModels) {
        this.messagesModels = messagesModels;
    }
}

