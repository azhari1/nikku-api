package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.nikku.model.UsersModel;

@Table(name = UsersModel.TABLE_NAME)
public class TopRatedPerson{

    @TableColumn(name = UsersModel._ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(UsersModel._ID)
    private long id;

    @TableColumn(name = UsersModel._NIK)
    @JsonProperty(UsersModel._NIK)
    private String nik;

    @TableColumn(name = UsersModel._PROFILE_PICTURE)
    @JsonProperty(UsersModel._PROFILE_PICTURE)
    private String profilePicture;

    @TableColumn(name = UsersModel._AVG_RATING)
    @JsonProperty(UsersModel._AVG_RATING)
    private double avgRating;

    @TableColumn(name = UsersModel._FULL_NAME)
    @JsonProperty(UsersModel._FULL_NAME)
    private String fullName;

    @TableColumn(name = UsersModel._JOB_TITLE)
    @JsonProperty(UsersModel._JOB_TITLE)
    private String jobTitle;

    @TableColumn(name = UsersModel._IS_ACTIVE)
    private Integer isActive;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(double avgRating) {
        this.avgRating = avgRating;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }
}