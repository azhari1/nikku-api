package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.filter.annotation.FilterColumn;

@Table(name = CompanyPersonReviewModel.TABLE_NAME)
public class CompanyPersonReviewModel {

    public static final String TABLE_NAME = "viewcompanypersonreview";
    public static final String _ID = "id";
    public static final String _OWNER = "owner";
    public static final String _REVIEWER_COMMENT = "reviewerComment";
    public static final String _LIKE = "like";
    public static final String _COMPANY_NAME = "companyName";
    public static final String _REVIEWER_PROFILE_PICTURE = "reviewerProfilePicture";
    public static final String _PHOTO = "photo";
    public static final String _USER_ID = "userID";
    public static final String _RATING_SCORE = "ratingScore";
    public static final String _OUTLET_NAME = "outletName";
    public static final String _COMPANY_ID = "companyId";
    public static final String _REVIEWER_NAME = "reviewerName";
    public static final String _REVIEW_DATE = "reviewDate";
    public static final String _REVIEWER_TYPE = "reviewerType";
    public static final String _TOTAL_VIEWS = "totalViews";
    public static final String _TOTAL_LIKES = "totalLikes";
    public static final String _REVIEWER_ID = "reviewerId";

    @TableColumn(name = _ID, primaryKey = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(name = _COMPANY_ID)
    @FilterColumn(columnName = _COMPANY_ID)
    @JsonProperty(_COMPANY_ID)
    private long companyId;

    @TableColumn(name = _COMPANY_NAME)
    @JsonProperty(_COMPANY_NAME)
    private String companyName;

    @TableColumn(name = _OUTLET_NAME)
    @JsonProperty(_OUTLET_NAME)
    private String outletName;

    @TableColumn(name = _OWNER)
    @JsonProperty(_OWNER)
    private long owner;

    @TableColumn(name = _USER_ID)
    @FilterColumn(columnName = _USER_ID)
    @JsonProperty(_USER_ID)
    private long userId;

    @TableColumn(name = _REVIEWER_ID)
    @JsonProperty(_REVIEWER_ID)
    private long reviewerId;

    @TableColumn(name = _REVIEWER_NAME)
    @JsonProperty(_REVIEWER_NAME)
    private String reviewerName;

    @TableColumn(name = _REVIEWER_PROFILE_PICTURE)
    @JsonProperty(_REVIEWER_PROFILE_PICTURE)
    private String reviewerProfilePicture;

    @TableColumn(name = _REVIEWER_TYPE)
    @JsonProperty(_REVIEWER_TYPE)
    private int reviewerType;

    @TableColumn(name = _REVIEWER_COMMENT)
    @JsonProperty(_REVIEWER_COMMENT)
    private String reviewerComment;

    @TableColumn(name = _PHOTO)
    @JsonProperty(_PHOTO)
    private String photo;

    @TableColumn(name = _RATING_SCORE)
    @JsonProperty(_RATING_SCORE)
    private float ratingScore;

    @TableColumn(name = _LIKE)
    @JsonProperty(_LIKE)
    private boolean like;

    @TableColumn(name = _TOTAL_VIEWS)
    @JsonProperty(_TOTAL_VIEWS)
    private int totalViews;

    @TableColumn(name = _TOTAL_LIKES)
    @JsonProperty(_TOTAL_LIKES)
    private int totalLikes;

    @TableColumn(name = _REVIEW_DATE)
    @JsonProperty(_REVIEW_DATE)
    private long reviewDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public long getOwner() {
        return owner;
    }

    public void setOwner(long owner) {
        this.owner = owner;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public String getReviewerProfilePicture() {
        return reviewerProfilePicture;
    }

    public void setReviewerProfilePicture(String reviewerProfilePicture) {
        this.reviewerProfilePicture = reviewerProfilePicture;
    }

    public int getReviewerType() {
        return reviewerType;
    }

    public void setReviewerType(int reviewerType) {
        this.reviewerType = reviewerType;
    }

    public String getReviewerComment() {
        return reviewerComment;
    }

    public void setReviewerComment(String reviewerComment) {
        this.reviewerComment = reviewerComment;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public float getRatingScore() {
        return ratingScore;
    }

    public void setRatingScore(float ratingScore) {
        this.ratingScore = ratingScore;
    }

    public boolean isLike() {
        return like;
    }

    public void setLike(boolean like) {
        this.like = like;
    }

    public int getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(int totalViews) {
        this.totalViews = totalViews;
    }

    public int getTotalLikes() {
        return totalLikes;
    }

    public void setTotalLikes(int totalLikes) {
        this.totalLikes = totalLikes;
    }

    public long getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(long reviewDate) {
        this.reviewDate = reviewDate;
    }
}
