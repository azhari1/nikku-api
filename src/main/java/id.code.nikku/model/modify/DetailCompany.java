package id.code.nikku.model.modify;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.nikku.model.CompaniesoutletsModel;
import id.code.nikku.model.TypeofbusinessModel;

import java.util.List;

@Table(name = DetailCompany.TABLE_NAME)
public class DetailCompany {

    public static final String TABLE_NAME = "detailcompany";
    private static final String _USERS = "users";

    @TableColumn(name = CompaniesoutletsModel._ID, primaryKey = true)
    @JsonProperty(CompaniesoutletsModel._ID)
    private long id;

    @TableColumn(name = CompaniesoutletsModel._LOGO)
    @JsonProperty(CompaniesoutletsModel._LOGO)
    private String logo;

    @TableColumn(name = CompaniesoutletsModel._COMPANY_NAME)
    @JsonProperty(CompaniesoutletsModel._COMPANY_NAME)
    private String companyName;

    @TableColumn(name = CompaniesoutletsModel._OUTLET_NAME)
    @JsonProperty(CompaniesoutletsModel._OUTLET_NAME)
    private String outletName;

    @TableColumn(name = CompaniesoutletsModel._TYPE_OF_BUSINESS_ID)
    @JsonProperty(CompaniesoutletsModel._TYPE_OF_BUSINESS_ID)
    private long typeOfBusinessID;

    @TableColumn(name = TypeofbusinessModel._TYPE_OF_BUSINESS_NAME)
    @JsonProperty(TypeofbusinessModel._TYPE_OF_BUSINESS_NAME)
    private String typeOfBusinessName;

    @TableColumn(name = CompaniesoutletsModel._PHONE)
    @JsonProperty(CompaniesoutletsModel._PHONE)
    private String phone;

    @TableColumn(name = CompaniesoutletsModel._EMAIL)
    @JsonProperty(CompaniesoutletsModel._EMAIL)
    private String email;

    @TableColumn(name = CompaniesoutletsModel._ADDRESS)
    @JsonProperty(CompaniesoutletsModel._ADDRESS)
    private String address;

    @TableColumn(name = CompaniesoutletsModel._WEBSITE)
    @JsonProperty(CompaniesoutletsModel._WEBSITE)
    private String website;

    @JsonProperty(_USERS)
    private List<DetailCompanyUsers> users;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public long getTypeOfBusinessID() {
        return typeOfBusinessID;
    }

    public void setTypeOfBusinessID(long typeOfBusinessID) {
        this.typeOfBusinessID = typeOfBusinessID;
    }

    public String getTypeOfBusinessName() {
        return typeOfBusinessName;
    }

    public void setTypeOfBusinessName(String typeOfBusinessName) {
        this.typeOfBusinessName = typeOfBusinessName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public List<DetailCompanyUsers> getUsers() {
        return users;
    }

    public void setUsers(List<DetailCompanyUsers> users) {
        this.users = users;
    }
}
