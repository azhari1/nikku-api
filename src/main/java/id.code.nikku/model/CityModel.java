package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = CityModel.TABLE_NAME)
public class CityModel extends BaseModel {

	public static final String TABLE_NAME = "city";
	public static final String _ID = "id";
	public static final String _PROVINCE_ID = "provinceId";
	public static final String _CITY_NAME = "cityName";
	public static final String _IS_PRIORITY = "isPriority";
	public static final String _PROVINCES = "provinces";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_PROVINCE_ID)
	@JsonProperty(_PROVINCE_ID)
	@ValidateColumn(_PROVINCE_ID)
	private long provinceId;

	@TableColumn(_CITY_NAME)
	@JsonProperty(_CITY_NAME)
	@ValidateColumn(_CITY_NAME)
	private String cityName;

	@TableColumn(_IS_PRIORITY)
	@JsonProperty(_IS_PRIORITY)
	private int isPriority;

	@JsonProperty(_PROVINCES)
	@ValidateColumn(_PROVINCES)
	private ProvinceModel province;


	public long getId() { return this.id; }
	public long getProvinceId() { return this.provinceId; }
	public String getCityName() { return this.cityName; }
	public int getIsPriority() { return this.isPriority; }
	public ProvinceModel getProvince(){ return this.province; }

	public void setId(long id) { this.id = id; }
	public void setProvinceId(long provinceId) { this.provinceId = provinceId; }
	public void setCityName(String cityName) { this.cityName = cityName; }
	public void setIsPriority(int isPriority) { this.isPriority = isPriority;}
	public void setProvince(ProvinceModel province) {this.province = province;}
}