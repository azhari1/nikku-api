package id.code.nikku.model;

import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Web Api Generator 17/12/2017.
 */

@Table(name = LikesModel.TABLE_NAME)
public class LikesModel extends BaseModel {
    public static final String TABLE_NAME = "likes";
	public static final String _ID = "id";
	public static final String _REVIEW_ID = "reviewId";
	public static final String _USER_ID = "userId";
	public static final String _DATE = "date";

	@TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
	@JsonProperty(_ID)
	private long id;

	@TableColumn(_REVIEW_ID)
	@JsonProperty(_REVIEW_ID)
	@ValidateColumn(_REVIEW_ID)
	private long reviewId;

	@TableColumn(_USER_ID)
	@JsonProperty(_USER_ID)
	@ValidateColumn(_USER_ID)
	private long userId;

	@TableColumn(_DATE)
	@JsonProperty(_DATE)
	@ValidateColumn(_DATE)
	private long date;


	public long getId() { return this.id; }
	public long getReviewId() { return this.reviewId; }
	public long getUserId() { return this.userId; }
	public long getDate() { return this.date; }

	public void setId(long id) { this.id = id; }
	public void setReviewId(long reviewId) { this.reviewId = reviewId; }
	public void setUserId(long userId) { this.userId = userId; }
	public void setDate(long date) { this.date = date; }

}