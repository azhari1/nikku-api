package id.code.nikku.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.Table;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

@Table(name = RequestReviewModel.TABLE_NAME)
public class RequestReviewModel extends BaseModel{

    public static final String TABLE_NAME = "requestReview";

    public static final String _ID = "id";
    public static final String _REQUESTER = "requester";
    public static final String _APPROVER = "approver";
    public static final String _APPROVED_DATE = "approvedDate";
    public static final String _REQUEST_DATE = "requestDate";
    public static final String _REVIEW_ID = "reviewId";
    public static final String _STATUS = "status";
    public static final String _USERS = "users";

    @TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(_REQUESTER)
    @JsonProperty(_REQUESTER)
    @ValidateColumn(_REQUESTER)
    private long requester;

    @TableColumn(_APPROVER)
    @JsonProperty(_APPROVER)
    @ValidateColumn(_APPROVER)
    private long approver;

    @TableColumn(_STATUS)
    @JsonProperty(_STATUS)
    private int status;

    @TableColumn(_REQUEST_DATE)
    @JsonProperty(_REQUEST_DATE)
    private long requestDate;

    @TableColumn(_APPROVED_DATE)
    @JsonProperty(_APPROVED_DATE)
    private long approvedDate;

    @TableColumn(_REVIEW_ID)
    @JsonProperty(_REVIEW_ID)
    private long reviewId;

    @JsonProperty(_USERS)
    private UsersModel users;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getRequester() {
        return requester;
    }

    public void setRequester(long requester) {
        this.requester = requester;
    }

    public long getApprover() {
        return approver;
    }

    public void setApprover(long approver) {
        this.approver = approver;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(long requestDate) {
        this.requestDate = requestDate;
    }

    public long getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(long approvedDate) {
        this.approvedDate = approvedDate;
    }

    public long getReviewId() {
        return reviewId;
    }

    public void setReviewId(long reviewId) {
        this.reviewId = reviewId;
    }

    public UsersModel getUsers() {
        return users;
    }

    public void setUsers(UsersModel users) {
        this.users = users;
    }
}
