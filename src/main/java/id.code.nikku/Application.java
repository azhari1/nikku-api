package id.code.nikku;

import id.code.nikku.api.Route;
import id.code.nikku.api.middleware.AccessTokenMiddleware;
import id.code.nikku.helper.Configure;
import id.code.server.ApiServerApplication;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class Application {

    private static final boolean isLocal = false;
    public static final String _PROPERTIES = "/var/www/html/nikku-api/Nikku.properties";

    public static void main(String[] args) {
        createApplication(_PROPERTIES).standaloneStart();
    }

    static ApiServerApplication createApplication(String configPath) {
        if (isLocal) {
            Configure.HOST = "http://localhost:8090/";
            return new ApiServerApplication()
                    .setPropertiesPath("Nikku.properties")
                    .setRoutes(Route::createRoutes)
                    .setAccessTokenMiddleware(AccessTokenMiddleware::new);
        } else {
            Configure.HOST = "http://159.65.141.12/";
            return new MyApiServerApplication()
                    .setPropertiesPath(configPath)
                    .setRoutes(Route::createRoutes)
                    .setAccessTokenMiddleware(AccessTokenMiddleware::new);
        }
    }
}