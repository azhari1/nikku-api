package id.code.nikku;

import id.code.component.AppLogger;
import id.code.component.DefaultComponent;
import id.code.component.lambda.ThrowingFunction;
import id.code.component.lambda.ThrowingRunnable;
import id.code.component.utility.StreamUtility;
import id.code.database.builder.QueryBuilder;
import id.code.database.builder.QueryBuilderException;
import id.code.server.AccessTokenValidator;
import id.code.server.ApiHandler;
import id.code.server.ApiServer;
import id.code.server.ApiServerApplication;
import id.code.server.servlet.ServletIntegration;

import java.util.Map;
import java.util.Properties;

public class MyApiServerApplication extends ApiServerApplication implements AutoCloseable {
    private ThrowingFunction<Properties, AccessTokenValidator> accessTokenMiddleware;
    private ThrowingFunction<Properties, Map<String, ApiHandler>> routes;
    private String propertiesPath;
    private ApiServer apiServer;
    private Properties properties;

    public MyApiServerApplication() {
    }

    public void close() throws Exception {
        StreamUtility.close(this.apiServer);
    }

    public id.code.server.ApiServerApplication setPropertiesPath(String propertiesPath) {
        this.propertiesPath = propertiesPath;
        return this;
    }

    public id.code.server.ApiServerApplication setRoutes(ThrowingFunction<Properties, Map<String, ApiHandler>> routes) {
        this.routes = routes;
        return this;
    }

    public id.code.server.ApiServerApplication setAccessTokenMiddleware(ThrowingFunction<Properties, AccessTokenValidator> accessTokenMiddleware) {
        this.accessTokenMiddleware = accessTokenMiddleware;
        return this;
    }

    public ApiServer getApiServer() {
        return this.apiServer;
    }

    public Properties getProperties() {
        return this.properties;
    }

    public id.code.server.ApiServerApplication standaloneStart() {
        return this.standaloneStart((ThrowingRunnable)null);
    }

    public id.code.server.ApiServerApplication standaloneStart(ThrowingRunnable listener) {
        try {
            this.createApiServer();
            this.apiServer.start();
            if (listener != null) {
                listener.run();
            }

            while(true){

            }

//            AppLogger.writeDebug("Stopping server");
//            this.apiServer.stop();
//            AppLogger.writeDebug("Server stopped");
        } catch (QueryBuilderException var14) {
            onStartupError(var14, false);
        } catch (Exception var15) {
            onStartupError(var15, true);
        } finally {
            if (this.apiServer != null) {
                try {
                    this.apiServer.stop();
                } catch (Exception var13) {
                    AppLogger.writeError(var13.getMessage(), var13);
                }
            }

        }

        return this;
    }

    public id.code.server.ApiServerApplication servletStart() {
        return this.servletStart((ThrowingRunnable)null);
    }

    public id.code.server.ApiServerApplication servletStart(ThrowingRunnable listener) {
        try {
            ServletIntegration.enableServletIntegration(this.createApiServer());
            if (listener != null) {
                listener.run();
            }

            return this;
        } catch (Exception var3) {
            AppLogger.writeError("Unable to enable servlet integration: " + var3.getMessage(), var3);
            throw new RuntimeException(var3);
        }
    }

    private static void onStartupError(Exception e, boolean trace) {
        try {
            Thread.sleep(1000L);
            if (trace) {
                e.printStackTrace();
            }

            AppLogger.writeError(e.getMessage(), trace ? e : null);
            Thread.sleep(1000L);
            System.out.println("\n");
            System.out.println(e.getMessage());
            System.out.println("Server cannot be started!");
            System.exit(1);
        } catch (InterruptedException var3) {
            ;
        }

    }

    protected ApiServer createApiServer() throws Exception {
        this.properties = this.properties != null ? this.properties : (this.properties = StreamUtility.getProperties(this.propertiesPath, true));
        DefaultComponent.initialize(this.properties);
        QueryBuilder.initialize(this.properties);
        AccessTokenValidator accessTokenValidator = this.accessTokenMiddleware == null ? AccessTokenValidator.createDefault() : (AccessTokenValidator)this.accessTokenMiddleware.apply(this.properties);
        Map<String, ApiHandler> routes = (Map)this.routes.apply(this.properties);
        this.apiServer = new ApiServer(this.properties, routes, accessTokenValidator);
        return this.apiServer;
    }
}