package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import static id.code.nikku.model.UserworkatcompaniesModel.*;

public class UserworkatcompaniesCreateValidation {

    @TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(_USER_ID)
    @JsonProperty(_USER_ID)
    @ValidateColumn(_USER_ID)
    private long userId;

    @TableColumn(_COMPANY_ID)
    @JsonProperty(_COMPANY_ID)
    @ValidateColumn(_COMPANY_ID)
    private long companyId;

    @TableColumn(_JOB_POSITION)
    @JsonProperty(_JOB_POSITION)
    @ValidateColumn(_JOB_POSITION)
    private String jobPosition;

    @TableColumn(_START_DATE)
    @JsonProperty(_START_DATE)
    @ValidateColumn(_START_DATE)
    private long startDate;

    @TableColumn(_END_DATE)
    @JsonProperty(_END_DATE)
    private Long endDate;

    @TableColumn(_STATUS)
    @JsonProperty(_STATUS)
    private Integer status;

    public long getId() { return this.id; }
    public long getUserId() { return this.userId; }
    public long getCompanyId() { return this.companyId; }
    public String getJobPosition() { return this.jobPosition; }
    public long getStartDate() { return this.startDate; }
    public Long getEndDate() { return this.endDate; }
    public Integer getStatus() { return this.status; }

    public void setId(long id) { this.id = id; }
    public void setUserId(long userId) { this.userId = userId; }
    public void setCompanyId(long companyId) { this.companyId = companyId; }
    public void setJobPosition(String jobPosition) { this.jobPosition = jobPosition; }
    public void setStartDate(long startDate) { this.startDate = startDate; }
    public void setEndDate(Long endDate) { this.endDate = endDate; }
    public void setStatus(Integer status) { this.status = status; }

}
