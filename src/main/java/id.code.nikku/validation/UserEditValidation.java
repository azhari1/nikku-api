package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import static id.code.nikku.model.UsersModel.*;

public class UserEditValidation {

    @TableColumn(_ID)
    @JsonProperty(_ID)
    @ValidateColumn(_ID)
    private long id;

    @TableColumn(_FULL_NAME)
    @JsonProperty(_FULL_NAME)
    @ValidateColumn(_FULL_NAME)
    private String fullName;

    @TableColumn(_EMAIL)
    @JsonProperty(_EMAIL)
    @ValidateColumn(_EMAIL)
    private String email;

    @TableColumn(_SEX)
    @JsonProperty(_SEX)
    private int sex;

    @TableColumn(_DATE_OF_BIRTH)
    @JsonProperty(_DATE_OF_BIRTH)
    @ValidateColumn(_DATE_OF_BIRTH)
    private long dateOfBirth;

    @TableColumn(_CITY_ID)
    @JsonProperty(_CITY_ID)
    @ValidateColumn(_CITY_ID)
    private long cityId;

    @TableColumn(_PROFILE_PICTURE)
    @JsonProperty(_PROFILE_PICTURE)
    private String profilePicture;

    @TableColumn(_PHONE)
    @JsonProperty(_PHONE)
    private String phone;

    @TableColumn(_NIK)
    @JsonProperty(_NIK)
    @ValidateColumn(_NIK)
    private String nik;

    @TableColumn(_JOB_TITLE)
    @JsonProperty(_JOB_TITLE)
    @ValidateColumn(_JOB_TITLE)
    private String jobTitle;

    @TableColumn(_BIO)
    @JsonProperty(_BIO)
    private String bio;

    @JsonProperty(UsersNikUpdateValidation._IS_MERGE)
    private int isMerge;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getIsMerge() {
        return isMerge;
    }

    public void setIsMerge(int isMerge) {
        this.isMerge = isMerge;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
