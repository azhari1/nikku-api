package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;

import static id.code.nikku.model.UserworkatcompaniesModel._ID;
import static id.code.nikku.model.UserworkatcompaniesModel._IS_HIDE;

public class HideOrShowCompanyValidation {

    @TableColumn(name = _ID, primaryKey = true, autoIncrement = true)
    @JsonProperty(_ID)
    private long id;

    @TableColumn(_IS_HIDE)
    @JsonProperty(_IS_HIDE)
    private int isHide;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIsHide() {
        return isHide;
    }

    public void setIsHide(int isHide) {
        this.isHide = isHide;
    }
}
