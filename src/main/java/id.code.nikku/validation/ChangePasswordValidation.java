package id.code.nikku.validation;

import id.code.nikku.model.UsersModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class ChangePasswordValidation {

    @ValidateColumn("old_password")
    @JsonProperty("old_password")
    private String oldPassword;

    @ValidateColumn("new_password")
    @JsonProperty("new_password")
    private String newPassword;

    public String getNewPassword() { return this.newPassword; }
    public String getOldPassword() { return this.oldPassword; }

    public void setNewPassword(String password) { this.newPassword = password; }
    public void setOldPassword(String password) { this.oldPassword = password; }
}