package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;

import static id.code.nikku.model.MessagesModel._RECEIVER_ID;
import static id.code.nikku.model.MessagesModel._SENDER_ID;

public class MessageDeleteValidation {

    @JsonProperty(_SENDER_ID)
    private long senderId;

    @JsonProperty(_RECEIVER_ID)
    private long receiverId;

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }
}
