package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import static id.code.nikku.model.UsersModel._FCM_TOKEN;
import static id.code.nikku.model.UsersModel._ID;

public class UsersFirebaseValidation {

    @JsonProperty(_ID)
    @ValidateColumn(_ID)
    private long id;

    @JsonProperty(_FCM_TOKEN)
    @ValidateColumn(_FCM_TOKEN)
    private String fcmToken;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
