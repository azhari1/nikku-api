package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import static id.code.nikku.model.UsersModel.*;

public class UserRegisterValidation {

    @TableColumn(_FULL_NAME)
    @JsonProperty(_FULL_NAME)
    @ValidateColumn(_FULL_NAME)
    private String fullName;

    @TableColumn(_EMAIL)
    @JsonProperty(_EMAIL)
    @ValidateColumn(_EMAIL)
    private String email;

    @TableColumn(_SEX)
    @JsonProperty(_SEX)
    private int sex;

    @TableColumn(_DATE_OF_BIRTH)
    @JsonProperty(_DATE_OF_BIRTH)
    @ValidateColumn(_DATE_OF_BIRTH)
    private long dateOfBirth;

    @TableColumn(_CITY_ID)
    @JsonProperty(_CITY_ID)
    @ValidateColumn(_CITY_ID)
    private long cityId;

    @TableColumn(_PROFILE_PICTURE)
    @JsonProperty(_PROFILE_PICTURE)
    @ValidateColumn(_PROFILE_PICTURE)
    private String profilePicture;

    @TableColumn(_GOOGLE_ID)
    @JsonProperty(_GOOGLE_ID)
    private String googleId;

    @TableColumn(_PASSWORD)
    @JsonProperty(value = _PASSWORD, access = JsonProperty.Access.WRITE_ONLY /* don't show password to client! */)
    private String password;

    @JsonProperty(_FCM_TOKEN)
    private String fcmToken;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFcmToken() { return this.fcmToken; }

    public void setFcmToken(String fcmToken) { this.fcmToken=fcmToken; }

    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }
}
