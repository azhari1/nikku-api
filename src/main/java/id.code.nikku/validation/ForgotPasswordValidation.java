package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ForgotPasswordValidation {

    @JsonProperty("email")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
