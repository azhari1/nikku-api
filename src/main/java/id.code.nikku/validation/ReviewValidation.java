package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;
import static id.code.nikku.model.ReviewsModel.*;

public class ReviewValidation {

    public static final String _REQUEST_REVIEW_ID = "request_review_id";

    @TableColumn(_USER_ID)
    @JsonProperty(_USER_ID)
    @ValidateColumn(_USER_ID)
    private long userId;

    @TableColumn(_REVIEW_TYPE)
    @JsonProperty(_REVIEW_TYPE)
    private int reviewType;

    @TableColumn(_AS_COMPANY_ID)
    @JsonProperty(_AS_COMPANY_ID)
    private Long asCompanyId;

    @TableColumn(_DATE)
    @JsonProperty(_DATE)
    @ValidateColumn(_DATE)
    private long date;

    @TableColumn(_QUESTIONNAIRE1)
    @JsonProperty(_QUESTIONNAIRE1)
    private Integer questionnaire1;

    @TableColumn(_QUESTIONNAIRE2)
    @JsonProperty(_QUESTIONNAIRE2)
    private Integer questionnaire2;

    @TableColumn(_QUESTIONNAIRE3)
    @JsonProperty(_QUESTIONNAIRE3)
    private Integer questionnaire3;

    @TableColumn(_QUESTIONNAIRE4)
    @JsonProperty(_QUESTIONNAIRE4)
    private Integer questionnaire4;

    @TableColumn(_QUESTIONNAIRE5)
    @JsonProperty(_QUESTIONNAIRE5)
    private Integer questionnaire5;

    @TableColumn(_COMMENT)
    @JsonProperty(_COMMENT)
    private String comment;

    @TableColumn(_PHOTO)
    @JsonProperty(_PHOTO)
    private String photo;

    @JsonProperty(_REQUEST_REVIEW_ID)
    private Long requestReviewId;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getReviewType() {
        return reviewType;
    }

    public void setReviewType(int reviewType) {
        this.reviewType = reviewType;
    }

    public Long getAsCompanyId() {
        return asCompanyId;
    }

    public void setAsCompanyId(Long asCompanyId) {
        this.asCompanyId = asCompanyId;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public Integer getQuestionnaire1() {
        return questionnaire1;
    }

    public void setQuestionnaire1(Integer questionnaire1) {
        this.questionnaire1 = questionnaire1;
    }

    public Integer getQuestionnaire2() {
        return questionnaire2;
    }

    public void setQuestionnaire2(Integer questionnaire2) {
        this.questionnaire2 = questionnaire2;
    }

    public Integer getQuestionnaire3() {
        return questionnaire3;
    }

    public void setQuestionnaire3(Integer questionnaire3) {
        this.questionnaire3 = questionnaire3;
    }

    public Integer getQuestionnaire4() {
        return questionnaire4;
    }

    public void setQuestionnaire4(Integer questionnaire4) {
        this.questionnaire4 = questionnaire4;
    }

    public Integer getQuestionnaire5() {
        return questionnaire5;
    }

    public void setQuestionnaire5(Integer questionnaire5) {
        this.questionnaire5 = questionnaire5;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Long getRequestReviewId() { return this.requestReviewId; }

    public void setRequestReviewId(Long requestReviewId) { this.requestReviewId = requestReviewId; }
}
