package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

public class UpdateForgotPasswordValidation {

    @ValidateColumn("email")
    @JsonProperty("email")
    private String email;

    @ValidateColumn("password")
    @JsonProperty("password")
    private String password;

    @ValidateColumn("password_confirmation")
    @JsonProperty("password_confirmation")
    private String passwordConfirmation;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
