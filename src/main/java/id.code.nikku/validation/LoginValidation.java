package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;
import id.code.nikku.model.UsersModel;

import static id.code.nikku.model.UsersModel._SESSION_ID;

/**
 * Created by Web Api Generator 17/12/2017.
 */

public class LoginValidation {
    @ValidateColumn(UsersModel._EMAIL)
    @JsonProperty(UsersModel._EMAIL)
    private String email;

    @ValidateColumn(UsersModel._PASSWORD)
    @JsonProperty(UsersModel._PASSWORD)
    private String password;

    @JsonProperty(UsersModel._FCM_TOKEN)
    private String fcmToken;

    @JsonProperty(_SESSION_ID)
    @ValidateColumn(_SESSION_ID)
    private String sessionId;

    public String getEmail() { return this.email; }
    public String getPassword() { return this.password; }
    public String getFcmToken() { return this.fcmToken; }

    public String getSessionId() {
        return sessionId;
    }

    public void setEmail(String email) { this.email = email; }
    public void setPassword(String password) { this.password = password; }
    public void setFcmToken(String fcmToken) { this.fcmToken = fcmToken; }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}