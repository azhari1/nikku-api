package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import static id.code.nikku.model.UsersModel.*;

public class UsersNikUpdateValidation {

    public static final String _IS_MERGE = "isMerge";

    @JsonProperty(_ID)
    @ValidateColumn(_ID)
    private long id;

    @JsonProperty(_NIK)
    @ValidateColumn(_NIK)
    private String nik;

    @JsonProperty(_KTP_PHOTO)
    @ValidateColumn(_KTP_PHOTO)
    private String ktpPhoto;

    @JsonProperty(_IS_MERGE)
    private int isMerge;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getKtpPhoto() {
        return ktpPhoto;
    }

    public void setKtpPhoto(String ktpPhoto) {
        this.ktpPhoto = ktpPhoto;
    }

    public int getIsMerge() {
        return isMerge;
    }

    public void setIsMerge(int isMerge) {
        this.isMerge = isMerge;
    }
}
