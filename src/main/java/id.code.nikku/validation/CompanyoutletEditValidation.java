package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.builder.annotation.TableColumn;
import id.code.database.validation.ValidateColumn;

import static id.code.nikku.model.CompaniesoutletsModel.*;
import static id.code.nikku.model.CompaniesoutletsModel._CREATED_BY;
import static id.code.nikku.model.CompaniesoutletsModel._LOGO;

public class CompanyoutletEditValidation {

    @TableColumn(_COMPANY_NAME)
    @JsonProperty(_COMPANY_NAME)
    @ValidateColumn(_COMPANY_NAME)
    private String companyName;

    @TableColumn(_OUTLET_NAME)
    @JsonProperty(_OUTLET_NAME)
    private String outletName;

    @TableColumn(_TYPE_OF_BUSINESS_ID)
    @JsonProperty(_TYPE_OF_BUSINESS_ID)
    private Long typeOfBusinessId;

    @TableColumn(_ADDRESS)
    @JsonProperty(_ADDRESS)
    @ValidateColumn(_ADDRESS)
    private String address;

    @TableColumn(_PHONE)
    @JsonProperty(_PHONE)
    @ValidateColumn(_PHONE)
    private String phone;

    @TableColumn(_EMAIL)
    @JsonProperty(_EMAIL)
    @ValidateColumn(_EMAIL)
    private String email;

    @TableColumn(_WEBSITE)
    @JsonProperty(_WEBSITE)
    private String website;

    @TableColumn(_LOGO)
    @JsonProperty(_LOGO)
    private String logo;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public Long getTypeOfBusinessId() {
        return typeOfBusinessId;
    }

    public void setTypeOfBusinessId(Long typeOfBusinessId) {
        this.typeOfBusinessId = typeOfBusinessId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
