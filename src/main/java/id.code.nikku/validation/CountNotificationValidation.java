package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CountNotificationValidation {

    final String _COUNTER = "counter";

    @JsonProperty(_COUNTER)
    private int counter;

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
