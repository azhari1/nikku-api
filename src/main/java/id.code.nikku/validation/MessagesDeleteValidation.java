package id.code.nikku.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import id.code.database.validation.ValidateColumn;

import java.util.List;

public class MessagesDeleteValidation {

    @JsonProperty("deleted")
    @ValidateColumn("deleted")
    private List<MessageDeleteValidation> deleted;

    public List<MessageDeleteValidation> getDeleted() {
        return deleted;
    }

    public void setDeleted(List<MessageDeleteValidation> deleted) {
        this.deleted = deleted;
    }
}
